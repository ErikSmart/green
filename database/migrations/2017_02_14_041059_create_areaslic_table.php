<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAreaslicTable extends Migration
{

    public function up()
    {
         Schema::create('areaslic', function (Blueprint $table) {
            $table->increments('id');
            $table->text('nombreArea');
            $table->text('acronimo');
            $table->text('color');
            $table->timestamps();
        });
    }

  
    public function down()
    {
       Schema::drop('areaslic');
    }
}
