<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVinculacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vinculacions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombreCompleto',255);
            $table->integer('id_perfil');
            $table->integer('id_status_formacion');
            $table->string('formAcademicLIC',255);
            $table->integer('id_area_LIC');
            $table->string('formdeMaestria',255)->nullable();
            $table->string('formEspecialidad',255)->nullable();
            $table->string('formDoctorado',255)->nullable();
            $table->text('formPostGrado')->nullable();
            $table->integer('id_status_gestion_laboral');
            $table->integer('id_status_form_docencia');
            $table->integer('id_status_invest_bio');
            $table->text('investigacion_temas')->nullable();
            $table->string('telefono');
            $table->string('telefono2')->nullable();
            $table->string('correo')->unique();
            $table->string('correo2')->nullable();
            $table->string('palabras_claves'); 
            /*llaves foraneas*/
            $table->integer('id_respuestas_gestion_laboral')->unsigned()->nullable();
            $table->foreign('id_respuestas_gestion_laboral')->references('id')->on('respuestas')->onDelete('cascade');
            $table->integer('id_respuestas_form_docencia')->unsigned()->nullable();
            $table->foreign('id_respuestas_form_docencia')->references('id')->on('respuestas')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();//borrado analogico en base de datos
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('vinculacions');
    }
}
