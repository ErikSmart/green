<?php

use Illuminate\Database\Seeder;

class referentialsTableSeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('referentials')->insert([ 
        	'descripcion'    => 'SuperAdmin',
            'identificador'  => 'perfil',
        ]);
         DB::table('referentials')->insert([ 
        	'descripcion'    => 'Alumno',
            'identificador'  => 'perfil',
        ]);
        DB::table('referentials')->insert([ 
        	'descripcion'    => 'Profesor',
            'identificador'  => 'perfil',
        ]);
        DB::table('referentials')->insert([ 
        	'descripcion'    => 'Egresado',
            'identificador'  => 'perfil',
        ]);
		/*status formacion*/
         DB::table('referentials')->insert([ 
        	'descripcion'    => 'Incompleta',
            'identificador'  => 'statusF',
        ]); 
         DB::table('referentials')->insert([ 
        	'descripcion'    => 'Completa',
            'identificador'  => 'statusF',
        ]); 
         DB::table('referentials')->insert([ 
        	'descripcion'    => 'Titulado',
            'identificador'  => 'statusF',
        ]);
         /*status laboral*/
         DB::table('referentials')->insert([ 
            'descripcion'    => 'Activo',
            'identificador'  => 'statusL',
        ]); 
         DB::table('referentials')->insert([ 
            'descripcion'    => 'Inactivo',
            'identificador'  => 'statusL',
        ]); 
         DB::table('referentials')->insert([ 
            'descripcion'    => 'Inexistente',
            'identificador'  => 'statusL',
        ]);
         /*status ambito*/
         DB::table('referentials')->insert([ 
            'descripcion'    => 'Gestión',
            'identificador'  => 'ambito',
        ]); 
         DB::table('referentials')->insert([ 
            'descripcion'    => 'Docencia',
            'identificador'  => 'ambito',
        ]); 
         DB::table('referentials')->insert([ 
            'descripcion'    => 'Investigación',
            'identificador'  => 'ambito',
        ]);
    }
}
