<?php

use Illuminate\Database\Seeder;

class degreesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('degrees')->insert([ 
        [   'title' => 'Médico Cirujano',   
            'areasLic'     =>'1',
        ],[ 
            'title' => 'Psicología',   
            'areasLic'     =>'1',
        ],[ 
            'title' => 'Nutrición',   
            'areasLic'     =>'1',
        ],[ 
            'title' => 'Terapia Física y Rehabilitación',   
            'areasLic'     =>'1',
        ],[ 
            'title' => 'Cirujano Dentista',   
            'areasLic'     =>'1',
        ],[ 
            'title' => 'Psicología',   
            'areasLic'     =>'1',
        ],[ 
            'title' => 'Dirección y Administración de Instituciones de Salud',   
            'areasLic'     =>'1',
        ],[ 
            'title' => 'Otra',   
            'areasLic'     =>'1',
        ],[
        		'title' => 'Lenguas Modernas y Gestión Cultural',
            'areasLic'     =>'2',
        ],[
        		'title' => 'Derecho',
            'areasLic'     =>'2',
        ],[
        		'title' => 'Comunicación',
            'areasLic'     =>'2',
        ],[
        		'title' => 'Diseño Gráfico',
            'areasLic'     =>'2',
        ],[
        		'title' => 'Administración Pública y Gobierno',
            'areasLic'     =>'2',
        ],[
        		'title' => 'Pedagogía Organizacional y Educativa',
            'areasLic'     =>'2',
        ],[
        		'title' => 'Historia',
            'areasLic'     =>'2',
        ],[
        		'title' => 'Moda, Innovación y Tendencia',
            'areasLic'     =>'2',
        ],[
        		'title' => 'Administración Turística',
            'areasLic'     =>'2',
        ],[
        		'title' => 'Dirección de Restaurantes',
            'areasLic'     =>'2',
        ],[
        		'title' => 'Dirección y Administración del Deporte',
            'areasLic'     =>'2',
        ],[
        		'title' => 'Dirección Internacional de Hoteles',
            'areasLic'     =>'2',
        ],[
        		'title' => 'Gastronomía',
            'areasLic'     =>'2',
        ],[
        		'title' => 'Dirección de Empresas de Entretenimiento',
            'areasLic'     =>'2',
        ],[
        		'title' => 'Inteligencia Estratégica',
            'areasLic'     =>'2',
        ],[
        		'title' => 'D. en Responsabilidad Social y Desarrollo Sustentable',
            'areasLic'     =>'2',
        ],[
        		'title' => 'Relaciones Internacionales',
            'areasLic'     =>'2',
        ],[
        		'title' => 'Turismo Cultural y Gastronómico',
            'areasLic'     =>'2',
        ],[
        		'title' => 'Otra',
            'areasLic'     =>'2',
        ],[
        		'title' => 'Administración y Dirección de Empresas',
            'areasLic'     =>'3',
        ],[
        		'title' => 'Finanzas y Contaduría Pública',
            'areasLic'     =>'3',
        ],[
        		'title' => 'Economía',
            'areasLic'     =>'3',
        ],[
        		'title' => 'Actuaría',
            'areasLic'     =>'3',
        ],[
        		'title' => 'Dirección Financiera',
            'areasLic'     =>'3',
        ],[
        		'title' => 'Ingeniería Financiera',
            'areasLic'     =>'3',
        ],[
        		'title' => 'Mercadotecnia Estratégica',
            'areasLic'     =>'3',
        ],[
        		'title' => 'Negocios Internacionales',
            'areasLic'     =>'3',
        ],[
        		'title' => 'Administración de Negocios',
            'areasLic'     =>'3',
        ],[
        		'title' => 'D. Comunicación Mercadológica y Corporativa',
            'areasLic'     =>'3',
        ],[
        		'title' => 'Ingeniería de Negocios',
            'areasLic'     =>'3',
        ],[
        		'title' => 'Otra',
            'areasLic'     =>'3',
        ],[
        		'title' => 'Ingeniería Ambiental',
            'areasLic'     =>'4',
        ],[
        		'title' => 'Ingeniería Biomédica',
            'areasLic'     =>'4',
        ],[
        		'title' => 'Ingeniería Química',
            'areasLic'     =>'4',
        ],[
        		'title' => 'Geografía',
            'areasLic'     =>'4',
        ],[
        		'title' => 'Física',
            'areasLic'     =>'4',
        ],[
        		'title' => 'Otra',
            'areasLic'     =>'4',
        ],[
        		'title' => 'Teología',
            'areasLic'     =>'5',
        ],[
        		'title' => 'Filosofía',
            'areasLic'     =>'5',
        ],[
        		'title' => 'Ciencias Religiosas',
            'areasLic'     =>'5',
        ],[
        		'title' => 'Otra',
            'areasLic'     =>'5',
        ],[
        		'title' => 'Ing. Industrial para la Dirección',
            'areasLic'     =>'6',
        ],[
        		'title' => 'Ingeniería Mecatrónica',
            'areasLic'     =>'6',
        ],[
        		'title' => 'Ingeniería Civil',
            'areasLic'     =>'6',
        ],[
        		'title' => 'Ing. en Sistemas y Tecnologías de Información',
            'areasLic'     =>'6',
        ],[
        		'title' => 'Diseño Industrial',
            'areasLic'     =>'6',
        ],[
        		'title' => 'Ingeniería de Alimentos',
            'areasLic'     =>'6',
        ],[
        		'title' => 'Ingeniería Biomédica',
            'areasLic'     =>'6',
        ],[
        		'title' => 'Diseño Multimedia',
            'areasLic'     =>'6',
        ],[
        		'title' => 'Arquitectura',
            'areasLic'     =>'6',
        ],[
        		'title' => 'Otra',
            'areasLic'     =>'6',
        ],[
        		'title' => 'Artes Visuales',
            'areasLic'     =>'7',
        ],[
        		'title' => 'Música Contemporánea',
            'areasLic'     =>'7',
        ],[
        		'title' => 'Teatro y Actuación',
            'areasLic'     =>'7',
        ],[
        		'title' => 'Fotografía',
            'areasLic'     =>'7',
        ],[
        		'title' => 'Otra',
            'areasLic'     =>'7',
        ]]);
    }
}
