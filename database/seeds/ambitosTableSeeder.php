<?php

use Illuminate\Database\Seeder;

class ambitosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       
        DB::table('ambitos')->insert([ 
            'nombre' => 'Fundamentos de la Ética y Bioética',       	
        ]);
        DB::table('ambitos')->insert([ 
            'nombre' => 'Bioética al Inicio de la Vida',       	
        ]);
        DB::table('ambitos')->insert([ 
            'nombre' => 'Bioética al Final de la Vida',       	
        ]);
        DB::table('ambitos')->insert([ 
            'nombre' => 'Bioética, Reproducción Humana y Sexualidad',       	
        ]);
        DB::table('ambitos')->insert([ 
            'nombre' => 'Bioética Clínica',       	
        ]);
        DB::table('ambitos')->insert([ 
            'nombre' => 'Bioética y la Industria Farmacéutica',       	
        ]);
        DB::table('ambitos')->insert([ 
            'nombre' => 'Bioética y Tecnlogía',       	
        ]);
        DB::table('ambitos')->insert([ 
            'nombre' => 'Bioética en la Investigación Clínica y Básica',       	
        ]);
        DB::table('ambitos')->insert([ 
            'nombre' => 'Bioética Equidad y Justicia',       	
        ]);
        DB::table('ambitos')->insert([ 
            'nombre' => 'Bioética y Educación',       	
        ]);
        DB::table('ambitos')->insert([ 
            'nombre' => 'Bioética y Medio Ambiente',       	
        ]);
        DB::table('ambitos')->insert([ 
            'nombre' => 'Arte, Ética y Bioética',       	
        ]);
        

    }
}
