<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $this->call(referentialsTableSeder::class);
        $this->call(areasLicTableSeeder::class);
        $this->call(ambitosTableSeeder::class);
        $this->call(degreesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
    }
}
