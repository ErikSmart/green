<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class areasLic extends Model
{
    protected $table = 'areaslic';
    protected $primarykey = 'id';
    protected $fillable = [
        'nombreArea', 'acronimo', 'color',
    ];

    public static function areaslic(){
    	return DB::select("select * from areaslic");
    }
    public function degrees(){
         return $this->hasMany(Degree::class, 'areaslic');
    }
}
