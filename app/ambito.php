<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class ambito extends Model
{
    protected $table = 'areasLic';
    protected $fillable = [
        'nombre',
    ];
    public static function ambitos(){
    	return DB::select("select * from ambitos");
    }
    
}
