<?php
namespace App;

/*archivos necesarios para creacion de imagen*/
require_once __DIR__.'/captcha/ArchivoBase64.php';
require_once __DIR__.'/captcha/Random.php';
class Captcha {

    public $base64, $tmp, $folder;

    public function __construct() {
        set_time_limit(0);
        $this->base64 = new ArchivoBase64();
        $this->tmp = new Random();
        $this->folder = __DIR__."/captcha/";
    }


    public function generarCaptcha($code) {
        $code = trim($code);
        if ($code == '*') {
            $code = $this->tmp->alfaNumerico(7);
        }else{
            $code = $this->tmp->alfaNumerico($code);
        }
        $width = 300;
        $heigth = 65;
        // Genero la imagen 
        $img = imagecreatetruecolor($width, $heigth);

        // Colores 
        $bgColor = imagecolorallocate($img, 230, 230, 230);
        $stringColor = imagecolorallocate($img, 90, 90, 90);
        $lineColor = imagecolorallocate($img, 245, 245, 245);

        // Fondo 
        imagefill($img, 0, 0, $bgColor);

        imageline($img, 0, 5, $width, 5, $lineColor);
        imageline($img, 0, 10, $width, 10, $lineColor);
        imageline($img, 0, 15, $width, 15, $lineColor);
        imageline($img, 0, 20, $width, 20, $lineColor);
        imageline($img, 0, 25, $width, 10, $lineColor);
        imageline($img, 0, 30, $width, 30, $lineColor);
        imageline($img, 0, 35, $width, 35, $lineColor);
        imageline($img, 0, 40, $width, 40, $lineColor);
        imageline($img, 0, 45, $width, 45, $lineColor);
        imageline($img, 0, 50, $width, 50, $lineColor);
        imageline($img, 0, 55, $width, 55, $lineColor);
        imageline($img, 0, 60, $width, 60, $lineColor);

        imageline($img, 12, 0, 24, $heigth, $lineColor);
        imageline($img, 24, 0, 24, $heigth, $lineColor);
        imageline($img, 36, 0, 36, $heigth, $lineColor);
        imageline($img, 48, 0, 48, $heigth, $lineColor);
        imageline($img, 60, 0, 60, $heigth, $lineColor);
        imageline($img, 72, 0, 24, $heigth, $lineColor);
        imageline($img, 60, 0, 84, $heigth, $lineColor);
        imageline($img, 96, 0, 96, $heigth, $lineColor);
        imageline($img, 108, 0, 108, $heigth, $lineColor);
        imageline($img, 120, 0, 120, $heigth, $lineColor);
        imageline($img, 132, 0, 132, $heigth, $lineColor);
        imageline($img, 144, 0, 144, $heigth, $lineColor);
        imageline($img, 156, 0, 120, $heigth, $lineColor);
        imageline($img, 168, 0, 168, $heigth, $lineColor);
        imageline($img, 180, 0, 180, $heigth, $lineColor);
        imageline($img, 144, 0, 192, $heigth, $lineColor);
        imageline($img, 204, 0, 204, $heigth, $lineColor);
        imageline($img, 216, 0, 216, $heigth, $lineColor);
        imageline($img, 228, 0, 228, $heigth, $lineColor);
        imageline($img, 240, 0, 240, $heigth, $lineColor);
        imageline($img, 252, 0, 252, $heigth, $lineColor);
        imageline($img, 264, 0, 264, $heigth, $lineColor);
        imageline($img, 276, 0, 276, $heigth, $lineColor);
        imageline($img, 252, 0, 288, $heigth, $lineColor);
        imageline($img, 288, 0, 240, $heigth, $lineColor);
        imageline($img, 288, 0, 168, $heigth, $lineColor);

        //Cargar fuente
        $font = imageloadfont(__DIR__ . '/captcha/fuente/bubblebath.gdf');

        // Escribo el codigo 
        imageString($img, $font, 35, 15, $code, $stringColor);

        // Image output.

         if(file_exists($this->folder)){ 
            $ruta = $this->folder . $code . ".png";
            imagepng($img, $ruta);
         }else{
            die('Debe crear el siguiente directorio <b>'. $this->folder.'</b> para continuar con su funcionamiento.');
         }
        // Extraer el tipo de mime del archivo
        $mime = $this->base64->obtenerMime($ruta);
        //
        $img = $this->base64->generarBase64($ruta);

        // Eliminar la imagen fisica del servidor
        $this->base64->eliminarArchivoFisico($ruta);
        // Ruta para el usuario
        $uri = 'data:' . $mime . ';base64,' . $img;
        return array("code"=>base64_encode($code),"message" => "Generar imagen base64", "uri" => $uri);
    }

}

?>