<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class vinculacions_ambitos extends Model
{
    protected $table = 'vinculacions_ambitos';
    protected $fillable = [
    	'id_vinculacions','id_ambitos','checked','id_tipo'
    ];
 
   public static function selectA1($id){
   	return DB::select("select tp.id, tp.id_ambitos,ambitos.nombre,tp.id_tipo,cheked
		FROM vinculacions_ambitos as tp
		RIGHT JOIN ambitos on ambitos.id = tp.id_ambitos where tp.id_tipo = 11 and tp.id_vinculacions = ".$id." ");
   }
    public static function selectA2($id){
   	return DB::select("select tp.id, tp.id_ambitos,ambitos.nombre,tp.id_tipo,cheked
		FROM vinculacions_ambitos as tp
		RIGHT JOIN ambitos on ambitos.id = tp.id_ambitos where tp.id_tipo = 12 and tp.id_vinculacions = ".$id."  ");
   }
    public static function selectA3($id){
   	return DB::select("select tp.id, tp.id_ambitos,ambitos.nombre,tp.id_tipo,cheked
		FROM vinculacions_ambitos as tp
		RIGHT JOIN ambitos on ambitos.id = tp.id_ambitos where tp.id_tipo = 13 and tp.id_vinculacions = ".$id."  ");
   }

   public static function changeCheck($id){
    return  DB::table('vinculacions_ambitos')
        ->select('*')
        ->where('id','=',$id)
        ->get();
   }
}
