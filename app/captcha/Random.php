<?php

namespace App;
class Random {

    /**
     * string de valores posibles que seran usados para generar un alfanumerico aleatorio
     * @var string str
     * @access private
     */
    private $alfa = 'ABCDEFGHJKMNOPQRSTUVWXYZabcdefghjkmnopqrstuvwxyz1234567890';

    /**
     * Contenedor posibles que seran usados cuando se genere el numero
     * @var string str
     * @access private
     */
    private $cont;

    public function __construct() {
        $this->alfa;
        $this->cont = '';
    }

    /**
     * @descripcion(Permite generar valores alfanum&eacute;ricos aleatorios)
     * 
     * @example(titulo="Generar alfanum&eacute;ricos aleatorios",codigo="$respuesta=file_get_contents(&#34;http://esbutil.mppi.gob.ve/alfaNumerico/&#34;&#46;http_build_query(array(&#34;prm&#34;=>array(&#34;7&#34;))))")
     * @param($cant="Cantidad de string que deseo obtener.")
     * @return(Devuelve un string con la cantidad solicitada)
     */
    public function alfaNumerico($cant) {
        for ($i = 0; $i < $cant; $i++) {
            $this->cont .= substr($this->alfa, rand(0, 62), 1);
        }
        $this->result = $this->cont;
        return $this->result;
    }

    /**
     * @descripcion(Permite generar valores num&eacute;ricos aleatorios)
     * 
     * @example(titulo="Generar num&eacute;ricos aleatorios",codigo="$respuesta=file_get_contents(&#34;http://esbutil.mppi.gob.ve/numerico/&#34;&#46;http_build_query(array(&#34;prm&#34;=>array(&#34;7&#34;))))")
     * @param($cant="Cantidad de num&eacute;ricos que deseo obtener.")
     * @return(Devuelve un string con la cantidad solicitada)
     */
    public function numerico($cant) {
        for ($i = 0; $i < $cant; $i++) {
            $this->cont .= chr(rand(48, 57));
        }
        $this->result = $this->cont;
        return $this->result;
    }

    /**
     * @descripcion(Permite generar valores aleatorios solo letras May&uacute;sculas.)
     * 
     * @example(titulo="Generar letas de forma aleatorias",codigo="$respuesta=file_get_contents(&#34;http://esbutil.mppi.gob.ve/letrasMayuscula/&#34;&#46;http_build_query(array(&#34;prm&#34;=>array(&#34;7&#34;))))")
     * @param($cant="Cantidad de letras May&uacute;sculas que deseo obtener.")
     * @return(Devuelve un string con la cantidad solicitada)
     */
    public function letrasMayuscula($cant) {
        for ($i = 0; $i < $cant; $i++) {
            $this->cont .= chr(rand(65, 90));
        }
        $this->result = $this->cont;
        return $this->result;
    }

    /**
     * @descripcion(Permite generar valores aleatorios solo letras Min&uacute;scula.)
     * 
     * @example(titulo="Generar letas de forma aleatorias",codigo="$respuesta=file_get_contents(&#34;http://esbutil.mppi.gob.ve/letrasMinuscula/&#34;&#46;http_build_query(array(&#34;prm&#34;=>array(&#34;7&#34;))))")
     * @param($cant="Cantidad de letras Min&uacute;scula que deseo obtener.")
     * @return(Devuelve un string con la cantidad solicitada)
     */
    public function letrasMinuscula($cant) {
        for ($i = 0; $i < $cant; $i++) {
            $this->cont .= chr(rand(97, 122));
        }
        $this->result = $this->cont;
        return $this->result;
    }

    public function __destruct() {
        
    }

}

?>
