<?php

namespace App;

class ArchivoBase64 {
     /**
     * @access public
     * @var string Almacenar los datos del base 64 del archivo
     */
    public $data;

    /**
     * Constructor de la clase
     */
    public function __construct() {
        $this->data;
    }

    /**
     * Generador de base64 de archivos 
     * @author Gregorio Bolivar B <elalconxvii@gmail.com>
     * @version 1.0
     * @param string $ruta Ruta del archivo donde se guarda los documentos 
     * @return string Base64 del archivo que se codifico
     * 
     */
    public function generarBase64($ruta) {
        $this->data = base64_encode(file_get_contents($ruta));
        return $this->data;
    }

    /**
     * Eliminar archivo fisico del servidor luego que se genera el string del base 64
     * @author Gregorio Bolivar B <elalconxvii@gmail.com>
     * @version 1.0     
     * @param string $ruta ruta donde esta creada el archivo en el servidor 
     */
    public function eliminarArchivoFisico($ruta) {
        unlink($ruta);
    }
    /**
     * Permite obtener el mime del archivo
     * @param string $ruta Ruta donde se encuentra la imagen
     */
    public function obtenerMime($ruta){
        //return mime_content_type($ruta);
    }
}

?>
