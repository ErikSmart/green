<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
class vinculacion extends Model
{
    protected $table = 'vinculacions';
    protected $fillable = [
       'nombreCompleto', 'id_perfil', 'id_status_formacion', 'formAcademicLIC', 'id_area_LIC', 'formdeMaestria', 'formEspecialidad', 'formDoctorado', 'formPostGrado', 'id_status_gestion_laboral', 'id_respuestas_gestion_laboral', 'id_status_form_docencia', 'id_respuestas_form_docencia', 'id_status_invest_bio', 'investigacion_temas', 'telefono', 'telefono2', 'correo', 'correo2', 'palabras_claves','created_at','updated_at'
    ];
    
    use SoftDeletes;
    protected $dates = ['deleted_at'];//para hacer un borrado logico de datos

    /*busqueda de correo*/
    public  static function email($correo){
    	return DB::table('vinculacions')
    	->select('correo')
    	->where('correo','=',$correo)
        ->get();
     }

       public function Registry(){
         return $this->hasMany('App\Registry','id');
    }

    public static function busqueda($sql_where){
    
    	return DB::select("select * FROM usuariosactivos ".$sql_where." ");
    }

 
   public static function listado($where){
   	return DB::select("
   				select 
				vinculacions.id,
				vinculacions.nombreCompleto,
				vinculacions.palabras_claves,
	        	st.descripcion as status,
				users.deleted_at,
				registris.created_at
				from vinculacions	
       			join users  on users.id_vinculacion = vinculacions.id
	        	join referentials as st on st.id = users.status
	        	join registris on vinculacions.id = registris.addressee
                ".$where."
                ");
   }
   public static function seleccionado($id){

   	 return DB::select("select * from usuariosactivos where idV = ".$id."   ");
   }
   public static function verConsulta($where_sql){

   	 return DB::select("select * from usuariosactivos ".$where_sql." ORDER BY nombreCompleto");
   }

   public static function notificar(){
   	return DB::select("
			SELECT DISTINCT
			vinculacions.id as idV,
			nombreCompleto,			
			p.descripcion as perfil ,	
			telefono,
			correo, 
		    users.id as idUser,
			vinculacions.created_at,
			vinculacions.updated_at,
			st.descripcion as status,
			users.deleted_at
			FROM vinculacions
			left join referentials as p on p.id = vinculacions.id_perfil
		    left join users on users.id_vinculacion = vinculacions.id
		    left join referentials as st on st.id = users.status
		    where users.id is null
			order by vinculacions.id desc	
		");
   }

   
    public static function listadoU($id = false){
	if ($id == false)
	 {
		return DB::select("
			SELECT DISTINCT
			vinculacions.id as idV,
			nombreCompleto,			
			p.descripcion as perfil ,	
			telefono,
			correo, 
		    users.id as idUser,
			vinculacions.created_at,
			vinculacions.updated_at,
			st.descripcion as status,
			users.deleted_at
			FROM vinculacions
			join referentials as p on p.id = vinculacions.id_perfil
		    left join users on users.id_vinculacion = vinculacions.id
		    left join referentials as st on st.id = users.status
			order by vinculacions.id desc	
		");
	}else{
		return DB::select("
				SELECT DISTINCT
				vn.id,
				vn.nombreCompleto,
				vn.id_perfil,
				vn.id_status_formacion,
				vn.id_area_LIC,
				vn.id_status_gestion_laboral,
				rl.respuesta1 as rl1,
				rl.respuesta2 as rl2,
				rl.respuesta3 as rl3,
				rd.respuesta1 as rd1,
				rd.respuesta2 as rd2,
				rd.respuesta3 as rd3,
				vn.id_status_form_docencia,
				vn.id_respuestas_form_docencia,
				vn.id_respuestas_gestion_laboral,
				vn.id_status_invest_bio,
				vn.formAcademicLIC,
				vn.formdeMaestria,
				vn.formEspecialidad,
				vn.formDoctorado,
				vn.formPostGrado,
				vn.telefono,
				vn.telefono2,
				vn.correo,
				vn.correo2,
				vn.investigacion_temas,
				vn.palabras_claves,
				vn.gestion_laboral,
				vn.formacion_docencia,
				vn.relacion,
				vn.temas_area,
				st.descripcion as status
				FROM ambitos as am
				join vinculacions_ambitos as tp on tp.id_ambitos = am.id
				join vinculacions as vn on vn.id = tp.id_vinculacions
				left join respuestas as rl on rl.id = vn.id_respuestas_gestion_laboral
				left join respuestas as rd on rd.id = vn.id_respuestas_form_docencia
       			left join users  on users.id_vinculacion = vn.id
	        	left join referentials as st on st.id = users.status
				WHERE tp.id_vinculacions = ".$id." 
				
			");
				
    	}
    }
    /*zona de mapa*/
    
}
