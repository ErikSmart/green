<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Degree extends Model{
  protected $table = 'degrees';
  protected $primarykey = 'id';
  protected $fillable = [
    'title', 'areasLic','color','mapView','id_tooltip_carrera','otros'
  ];

  public function areaLic(){
    return $this->belongsTo(areasLic::class, 'areaslic');
  }

  public static function listado(){
  	return DB::select('select
  	 dg.id , areaslic.nombreArea as areasLic,title,dg.color,mapView
  		from degrees as dg 
  		join areaslic on dg.areasLic = areaslic.id
  		order by areaslic.nombreArea asc');
  }
  public static function otros(){
      return DB::select("select * from degrees where areasLic = 8 ");
    }
  public static function notificar(){
    return DB::select("select * from degrees where created_at is not null and visto = false order by created_at desc");
  }
}
