<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class areaslic extends Model
{
    protected $table = 'areaslic';
    protected $primarykey = 'id';
    protected $fillable = [
        'nombreArea', 'acronimo', 'color',
    ];

    public function degrees(){
         return $this->hasMany(Degree::class, 'areaslic');
    }
}
