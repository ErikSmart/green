<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class referential extends Model
{
    protected $table = 'referentials';
    protected $fillable = [
        'descripcion','identificador',
    ];

    public static function perfiles(){
    	return DB::select("select id, descripcion,identificador from referentials where id != '1' and identificador = 'perfil'");
    }
    public static function statusF(){
        return DB::select("select id, descripcion,identificador from referentials where id != '1' and identificador = 'statusF'");
    }
    public static function statusL(){
        return DB::select("select id, descripcion,identificador from referentials where id != '1' and identificador = 'statusL'");
    }
     public static function statusA(){
        return DB::select("select id, descripcion,identificador from referentials where id != '1' and identificador = 'ambito'");
    }
}
