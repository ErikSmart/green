<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::group(['middleware' => 'web'], function () {
  Route::get('/','UsuarioController@index');

  Route::get('unete','UsuarioController@formulario');
  /*envio de formulario usuario*/
  Route::post('newUsuario','UsuarioController@store');
  Route::post('newFormacion','UsuarioController@saveformacion');
  Route::post('registrar','UsuarioController@store');
  Route::get('perfil','UsuarioController@perfil');
  Route::get('captcha','UsuarioController@captcha');

  Route::get('listdegrees','UsuarioController@areasLicSelect');
  Route::get('norepeatformacion/{title}','UsuarioController@norepeat');
  
  Route::get('mail/{correo}/{perfil?}','UsuarioController@mail');

  Route::get('registrar','HomeController@register');
  Route::get('logeo','HomeController@logeo');
  Route::get('quienes-somos','webController@somos');
  Route::get('areas-de-bioetica','webController@areasBioetica');
  Route::get('contactanos','webController@contacto');
  Route::post('enviocorreo','webController@contactanosMail');

  Route::post('passwordReset','webController@passwordReset');

  /*============ Controller mapa  ================*/
  Route::get('mapa','buscarController@busqueda');
  Route::get('resultados','buscarController@resultados');
  Route::get('buscando','mapaController@result');
  Route::get('dibujarMapa','mapaController@dibujarMapa');
  Route::get('ver/{nombre?}/{id?}/{where_sql?}','buscarController@verDetalles');
  Route::get('LoadCookies','buscarController@verDetalles');
  Route::get('searchClave/{clave?}','buscarController@searchClave');
  /*============ end Controller mapa  ================*/
  
 //===================== recuperar contraseña con codigo  ==========================
  Route::get('passReset','webController@recuperacionpass');
  Route::get('verifcarCod/{codigo}','webController@verficarcod');
  Route::post('password/restar','UsuarioController@contrasenaChange');
 

});

Route::group(['middleware' => ['auth']], function () {

  /*============ Controller Administrador  ================*/
  Route::get('hora','AdminController@hora');
  Route::get('configTimes','AdminController@timesConfig');
  Route::post('dataTimes','AdminController@editarTimes');



  Route::get('fichaUsuario/{id}','AdminController@listado');
  Route::get('sesionA/{id}','AdminController@sesionA');
  Route::get('validateUser/{id}','AdminController@validarUser');
  Route::get('desactivarsesion/{id}','AdminController@destroy');
  Route::get('activarsesion/{id}','AdminController@activar');
  Route::get('eliminarU/{id}','AdminController@deletedU');
  Route::get('perfil_usuario/{id}','AdminController@fichaAdimin');
  Route::get('newRegistros','AdminController@notiUsers');
  Route::get('newLice','AdminController@notiLice');

  Route::post('edit_perfil','AdminController@adminUpdate');
  Route::get('historials','AdminController@historialsView');
  Route::get('licenciaturas','AdminController@gestionarLic');//vista inicial
  Route::get('dataLic','AdminController@dataLic');//cargar via ajax datos en tabla
  Route::get('viewEditLic/{id}','AdminController@viewEditLic');//llamado de vista
  Route::post('updateLic','AdminController@updateLic');
  Route::get('trashLic/{id}','AdminController@destroyLic');
  Route::get('licvista/{id}','AdminController@licvista');


  /*============ end Controller Administrador  ================*/
 

  /*============ Controller Usuario  ================*/
  Route::post('cambiar_password','UsuarioController@contrasenaChange');
  Route::get('misdatos','UsuarioController@perfil');
  Route::get('userDeAlta/{id}','UsuarioController@deAlta');
  Route::get('userActivo/{id}','UsuarioController@Activo');
  Route::post('actualizardatos','UsuarioController@ActualizarDatos');
  Route::get('ambito/{id}','UsuarioController@checked');
  /*============ end Controller Usuario  ================*/

});
