<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\vinculacion;
use App\areasLic;
use App\mapaModel;
use App\ambito;
use App\Degree;

use DB;
class mapaController extends Controller
{
    /*dibujar mapa con el sql creado de la funcion anterior*/
    
    public function dibujarMapa(Request $request){

    $claves = $request['key'];
    $perfil = $request['perfil'];
    $ambito = $request['ambito'];
    $areas  = $request['areas'];


    /*contador de los checkbox seleccionados*/
    $cantP = count($perfil);
    $cantT = count($ambito);
    $cantA = count($areas);


    /*se inicializan las variables que contendran el sql en vacio*/
    $sql_perfil  = '';
    $sql_ambitos = '';
    $sql_areas   = '';
    $host        = [];
    /*condicional en caso de que la persona ingrese un valor en el input de reduccion asistida*/
  
      for ($i = 0; $i < $cantP; $i++) {
        if ($i == 0) {
          $condicion = "AND";
        }
        else {
          $condicion = "OR";
        }
          $sql_perfil.= "$condicion id_perfil = ".$request['perfil'][$i]." ";
      }
      for ($i = 0; $i < $cantT; $i++) {
         $sql_ambitos.= "AND ".$request['ambito'][$i]."= 'Activo' OR relacion like '%completa%' OR relacion like '%titulado%' OR relacion like '%incompleta%' OR relacion is null ";
      }
      for ($i = 0; $i < $cantA; $i++) {
        if ($i == 0) {
          $condicion = "AND";
        }
        else {
          $condicion = " OR";
        }
        $sql_areas.=  $condicion." id_ambitos =".$request['areas'][$i]."";
      }

      $where_sql = 'WHERE status = "Activo"  '.$sql_perfil.''.$sql_ambitos.' '.$sql_areas.'';

      
      
     if (empty($sql_perfil) && empty($sql_ambitos) && empty($sql_areas)) {
        $array = self::MapaDefault();
     }
     else{

        $salidaIDS = vinculacion::busqueda($where_sql);

        $ids =array();
        foreach ($salidaIDS as $value) {
          $quien =$value->idV;
          array_push($ids, $quien);
        }

        $array = self::mapFilter($ids);

     }
      return response()->json($array);
      
     
  }
  #consulta disparada por defecto al iniciar la vista
  public function MapaDefault(){
      $areaslic =areasLic::all();
      $count=mapaModel::all();
      $grupo=vinculacion::all();
      $array  =[];
      $info  =[];
      foreach ($areaslic as $key) 
      {
        foreach ($key->degrees as $g => $value) 
        {
          if ($value->mapView==true) 
          {
            if ($value->areasLic != 8) 
            {
              $cant=$count->where('iDtooltip',$value->id_tooltip_carrera)->where('status','Activo')->groupBy('id_vinculacions')->count();
                $ids=DB::table('usuariosactivos')->distinct()->where('iDtooltip',$value->id_tooltip_carrera)->pluck('id_vinculacions');

                 $subcat = self::subcat($value->id_tooltip_carrera);

                $datas=['subcat'=>$subcat,'nombre'=>$value->title,'cantidad'=>$cant,'url'=>$ids,'color'=>$value->color];
                
                array_push($info, $datas);
            }else{

               
               if ($value->title == 'Otras C. DE LA SALUD') {
                  $cant=$count->where('status','Activo')->whereIn('otros',[1,0])->whereIn('iDtooltip',[7])->whereIn('id_area_LIC',[1])->groupBy('id_vinculacions')->count();
                  
                  $ids=DB::table('usuariosactivos')->distinct()->whereIn('otros',[1,0])->whereIn('iDtooltip',[7])->whereIn('id_area_LIC',[1])->pluck('id_vinculacions');

                  $datas=['subcat'=>1,'nombre'=>'Otras C. DE LA SALUD','cantidad'=>$cant,'url'=>$ids,'color'=>$value->color];

               }elseif ($value->title  == 'C. SOCIALES Y HUMANIDADES, C. RELIGIOSAS y ARTE Y...') {
                  $cant=$count->where('status','Activo')->whereIn('otros',[1,0])->whereIn('iDtooltip',[500,80])->whereIn('id_area_LIC',[2,3,8])->groupBy('id_vinculacions')->count();

               $ids=DB::table('usuariosactivos')->distinct()->whereIn('otros',[1,0])->whereIn('iDtooltip',[500,80])->whereIn('id_area_LIC',[2,3,8])->pluck('id_vinculacions');
                $datas=['subcat'=>1,'nombre'=>'Otras C. ECONÓMICAS, SOCIALES Y HUMANIDADES','cantidad'=>$cant,'url'=>$ids,'color'=>$value->color];

               }elseif ($value->title  == 'Otras C. ECONÓMICAS') {

                $cant=$count->where('status','Activo')->whereIn('otros',[1,0])->whereIn('iDtooltip',[600000,6000])->whereIn('id_area_LIC',[6,4])->groupBy('id_vinculacions')->count();

                $ids=DB::table('usuariosactivos')->distinct()->whereIn('otros',[1,0])->whereIn('iDtooltip',[600000,6000])->whereIn('id_area_LIC',[6,4])->pluck('id_vinculacions');
                $datas=['subcat'=>1,'nombre'=>'Otras C.NATURALES Y TECNOLÓGICAS','cantidad'=>$cant,'url'=>$ids,'color'=>$value->color];

               }elseif ($value->title  == 'Otras C. TECNOLÓGICAS') {
                
                  $cant=$count->where('status','Activo')->whereIn('otros',[1,0])->whereIn('iDtooltip',[40000,5000000])->whereIn('id_area_LIC',[5,7])->groupBy('id_vinculacions')->count();

               $ids=DB::table('usuariosactivos')->distinct()->whereIn('otros',[1,0])->whereIn('iDtooltip',[40000,5000000])->whereIn('id_area_LIC',[5,7])->pluck('id_vinculacions');
                $datas=['subcat'=>1,'nombre'=>'Otras C. RELIGIOSAS, ARTE Y CULTURA','cantidad'=>$cant,'url'=>$ids,'color'=>$value->color];
               }
                
                array_push($info, $datas);
                
              
            }   
          }
        }  

         $data = ['num_cat' => $key->id,"categoria"=>$key->acronimo,'data'=>$info];
          array_push($array, $data); 
          $info  =[];
      }
      return $array;
  }
  #retornar valores a listado de colores
    public function result(Request $request)
    {

    /*optenemos el arreglo de id para consultar*/
      if (is_array($request['where'])) {
        $wher = array_unique($request['where']);
        $idss = implode($wher, ',');
  
     // mapaModel::whereIn('idV',$idss)->groupBy('id_vinculacions')->get();
        $resultado=DB::select('select * from usuariosactivos where idV in ('.$idss.') group by idV');
      }else{
        $wher = $request['where'];
        $resultado=mapaModel::where('acronimo',$wher )->groupBy('idV')->get();
      }


       $id =array();
        foreach ($resultado as $value) {
          $quien =$value->idV;
          array_push($id, $quien);
        }
 
    
      return response()->json(["data"=>$resultado,"where"=>$id]);
      
    }
     public function mapFilter($ids)
     {
        $areaslic =areaslic::all();
        $count=mapaModel::whereIn('idV', $ids)->get();
        $array  =[];
        $info  =[];
        foreach ($areaslic as $key) {
          foreach ($key->degrees as $value) {
            if ($value->mapView==true) {

               if ($value->areasLic != 8) 
            {
                $cant=$count->where('iDtooltip',$value->id_tooltip_carrera)->where('status','Activo')->groupBy('id_vinculacions')->count();
                $ids=$count->where('iDtooltip',$value->id_tooltip_carrera)->pluck('id_vinculacions');

                 $subcat = self::subcat($value->id_tooltip_carrera);

                $datas=['subcat'=>$subcat,'nombre'=>$value->title,'cantidad'=>$cant,'url'=>$ids,'color'=>$value->color];
                
                array_push($info, $datas);
            }else{

             if ($value->title == 'Otras C. DE LA SALUD') {
                  $cant=$count->where('status','Activo')->whereIn('otros',[1,0])->whereIn('iDtooltip',[7])->whereIn('id_area_LIC',[1])->groupBy('id_vinculacions')->count();
                  
                  $ids=DB::table('usuariosactivos')->distinct()->whereIn('otros',[1,0])->whereIn('iDtooltip',[7])->whereIn('id_area_LIC',[1])->pluck('id_vinculacions');

                  $datas=['subcat'=>1,'nombre'=>'Otras C. DE LA SALUD','cantidad'=>$cant,'url'=>$ids,'color'=>$value->color];

               }elseif ($value->title  == 'C. SOCIALES Y HUMANIDADES, C. RELIGIOSAS y ARTE Y...') {
                  $cant=$count->where('status','Activo')->whereIn('otros',[1,0])->whereIn('iDtooltip',[500,80])->whereIn('id_area_LIC',[2,3,8])->groupBy('id_vinculacions')->count();

               $ids=DB::table('usuariosactivos')->distinct()->whereIn('otros',[1,0])->whereIn('iDtooltip',[500,80])->whereIn('id_area_LIC',[2,3,8])->pluck('id_vinculacions');
                $datas=['subcat'=>1,'nombre'=>'Otras C. ECONÓMICAS, SOCIALES Y HUMANIDADES','cantidad'=>$cant,'url'=>$ids,'color'=>$value->color];

               }elseif ($value->title  == 'Otras C. ECONÓMICAS') {

                $cant=$count->where('status','Activo')->whereIn('otros',[1,0])->whereIn('iDtooltip',[600000,6000])->whereIn('id_area_LIC',[6,4])->groupBy('id_vinculacions')->count();

                $ids=DB::table('usuariosactivos')->distinct()->whereIn('otros',[1,0])->whereIn('iDtooltip',[600000,6000])->whereIn('id_area_LIC',[6,4])->pluck('id_vinculacions');
                $datas=['subcat'=>1,'nombre'=>'Otras C.NATURALES Y TECNOLÓGICAS','cantidad'=>$cant,'url'=>$ids,'color'=>$value->color];

               }elseif ($value->title  == 'Otras C. TECNOLÓGICAS') {
                
                  $cant=$count->where('status','Activo')->whereIn('otros',[1,0])->whereIn('iDtooltip',[40000,5000000])->whereIn('id_area_LIC',[5,7])->groupBy('id_vinculacions')->count();

               $ids=DB::table('usuariosactivos')->distinct()->whereIn('otros',[1,0])->whereIn('iDtooltip',[40000,5000000])->whereIn('id_area_LIC',[5,7])->pluck('id_vinculacions');
                $datas=['subcat'=>1,'nombre'=>'Otras C. RELIGIOSAS, ARTE Y CULTURA','cantidad'=>$cant,'url'=>$ids,'color'=>$value->color];
               }
                
                array_push($info, $datas);
                
                
              
            }   

            }
          }
            $data = ['num_cat' => $key->id,"categoria"=>$key->acronimo,'data'=>$info];
            array_push($array, $data); 
            $info  =[];
        }
        return $array;
    }
  

    #devolver la subcategoria
    public function subcat($sub)
    {

      if ($sub < 10) {
        $r = substr(($sub / 1),0);//1
          return  $r;
      }
       if ($sub <= 90 && 10 >= $sub) {
      
          $r = substr(($sub / 10),0);//2

          return  $r;
      }
       if ($sub <=500  &&  100>= $sub) {
          $r = substr(($sub / 100),-1);//3

          return  $r;
      }
       if ($sub <= 6000  &&  1000 >= $sub) {

          $r = substr(($sub / 1000),-1);//4

          return  $r;
      }

      if ($sub <= 40000  && 10000 >= $sub) {

          $r = substr(($sub / 10000),-1);//5

          return  $r;
      }

       if ($sub <= 600000 &&  100000>= $sub) {

          $r = substr(($sub / 100000),-1);//6

          return  $r;
      }
       if ($sub <= 5000000 && 1000000 >= $sub ) {

          $r = substr(($sub / 1000000),-1);//7

          return  $r;
      }
      if ($sub <= 40000000 &&  99999999 >=$sub )
      {
          $r = substr(($sub / 10000000),-1);//8

          return  $r;
      }

    }

}
