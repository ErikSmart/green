<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\vinculacion;
use App\User;
use Mail;
use Auth;
/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

       public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {
        if (Auth::user()->perfil_id == 1) {
            $listado = vinculacion::listadoU();
            $Hactive ="active";
            return view('home',compact('listado','Hactive'));
        }else{
             return view('errors.404');
        }
    }

   
}