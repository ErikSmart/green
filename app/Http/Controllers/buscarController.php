<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\vinculacion;
use App\referential;
use App\areasLic;
use App\ambito;
use App\User;
use App\respuesta;
use App\vinculacions_ambitos;
use DB;
use Symfony\Component\HttpFoundation\Cookie;
use Illuminate\Support\Facades\Redirect;

class buscarController extends Controller {
  public function resultados() {
    return view('search');

  }
  public function busqueda() {
    $val= vinculacion::all();
      //dd($val->pluck('formAcademicLIC'));
    $perfil  = referential::perfiles();
    $ambitos = referential::statusA();
    $areas   = ambito::ambitos();
    $areaslic =areaslic::areaslic();

    $listCookies = array_keys($_COOKIE);
    $cookies= str_replace('_',' ',$listCookies); 


    $numCookie=count($listCookies)-2;
    $title="MAPA";
    $Mactive="background:#000";
    return view('cuerpo.search',compact('perfil','ambitos','areas','areaslic','cookies','listCookies','numCookie','title','Mactive'));
  }


  
  public function verDetalles($nombre=false, $id=false, $where_sql=false) {
    $nombreId = $nombre.'/'.$id;
    $namecookie= str_replace(' ','%20',$nombre);
    $datos = array();
    if (!empty($where_sql)){

      $ids = "WHERE idV IN (".$where_sql.") group by id_vinculacions";
      $datos = vinculacion::verConsulta($ids); 
      
      if(!isset($_COOKIE[$namecookie])) {
        // setcookie("$namecookie", "$id/$where_sql" , time() + (518400 * 30), "/");  /* expira en 6 meses */
        self::LoadCookies($namecookie,$where_sql,$nombreId);
      }
    }
    else {
      if (!isset($_COOKIE[$namecookie])) {
        $where_sql = "";
        self::LoadCookies($namecookie,$where_sql,$nombreId);
      }
    }

    //print_r($resultado);

   /* echo "<br>";*/
    $actual    = vinculacion::seleccionado($id);

    /*print_r($datos);*/
    return view('cuerpo.show',compact('datos','actual'));
  }

  /*reproduccion asistida*/
  public function searchClave($clave = false) {
    $data =[];
    if (!empty($clave)) {
      $key= str_replace(' ','_',$clave);
      $where = "WHERE nombreCompleto like '%".$key."%' or palabras_claves like '%".$key."%' group by vinculacions.id order by registris.created_at desc ";
      $data = vinculacion::listado($where);
    }
    //print_r($data);
    return response()->json($data);
  }

  public function LoadCookies($namecookie=false,$where_sql=false,$nombreId=false) {
    $fecha = date('Y-m-j');
    $fechaDestroy = strtotime ( '+6 month' , strtotime ( $fecha ) ) ;
    $fechaDestroy = date ( 'Y-m-j' , $fechaDestroy );

    $response = array('name'=>$namecookie,'ruta'=>$nombreId.'/'.$where_sql);
    return response()->json($response);
  }

}
