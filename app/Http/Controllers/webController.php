<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\vinculacion;
use App\User;
use Mail;class webController extends Controller
{
    public function generarCodigo(){
          $cadena = "ABCDEFGHJKLMNOPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz1234567890";
          $longitudCadena=strlen($cadena);
          $pass = "";
          $longitudPass=8;
          for($i=1 ; $i<=$longitudPass ; $i++){
            $pos=rand(0,$longitudCadena-1);
            $pass .= substr($cadena,$pos,1);
          }
          return $pass;
    }
    public function passwordReset(Request $request){

        $codigo  = self::generarCodigo();
        $cabecera= 'Información de Bióetica';
        $accion  = 'Codigo de reinicio';
        $contenido = 'Ingresa el siguiente C&oacute;digo para restablecer t&uacute; contrase&ntilde;a <br><br><b style="font-size:16px">'.$codigo.'</b>';

        $dataEmail = $request['email'];
        $idU=User::sesion($dataEmail);

     //========= insertar codigo que se envia al correo

        foreach ($idU as $U) {
           User::where('id', $U->id)->update(['codigo' => $codigo ]);
        }
      $pagina = 'emails.resetpass';

        self::correo($pagina,$dataEmail,$accion,$cabecera,$contenido );
        
        $response=array('data'=>$idU,'msj'=>"Mensaje Enviado");

        return response()->json($response);
    }
     /*funcion encargada de  correos y tomar plantilla para ser enviada*/
      public function correo($pagina,$dataEmail,$accion,$cabecera,$contenido ){
          Mail::send($pagina,compact('contenido','cabecera'), function ($m) use ($dataEmail,$accion) {
              /*correo predeterminado del cual se enviaran correos*/
              $m->from('mundoweb321@gmail.com', 'Bióetica');
              $m->to($dataEmail,'Reinicio de Contrse&ntilde;a')->subject($accion);
          });
      }
    //===============retornar vista para ingresar codigo==================
    public function recuperacionpass(){
      return view('auth.passwords.reset');
    }

    public function verficarcod(Request $request ,$codigo){
        if ($request->ajax() && $codigo !=false )
        {
            $cod = User::codigo($codigo);
            return response()->json($cod);
        }
    }
    public function somos(){
      $Qactive="background:#000";
      return view('cuerpo.quienesomos',compact('Qactive'));
    }
    public function areasBioetica(){
      $Bactive="background:#000";
      return view('cuerpo.areasdebioetica',compact('Bactive'));
    }
     public function contacto(){
      return view('cuerpo.contactanos');
    }
    public function contactanosMail(Request $request){

        $cabecera= 'Usuario de Bióetica';
        $accion  = 'CONTACTO ÁREAS DE LA BIOÉTICA';
        $contenido = '<b>Nombre</b> '.$request['nombre'].'<br> <b>correo</b> '.$request['correo'].'<br> <b>Asunto:</b><br><br>'.$request['mensaje'];

        $dataEmail = "mundoweb321@gmail.com";
        $pagina = 'emails.resetpass';

        self::correo($pagina,$dataEmail,$accion,$cabecera,$contenido );
    }
}
