<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\vinculacion;
use App\referential;
use App\areasLic;
use App\ambito;
use App\Degree;
use App\User;
use App\respuesta;
use App\vinculacions_ambitos;
use App\Registry;
use Auth;
use DB;
use Symfony\Component\HttpFoundation\Cookie;
use Illuminate\Support\Facades\Redirect;
use App\Captcha;
use Mail;
class UsuarioController extends Controller {

    public $horactual;

    public function __construct()
    {
          $ubication = DB::select("select * from times");
        foreach ($ubication as  $v) {
         date_default_timezone_set($v->zona);

          $fecha = date('Y-m-d H:i:s'); //inicializo la fecha con la hora
          $this->horactual = strtotime ( '+'.$v->hour.' hour' , strtotime ( $fecha )) ;
          $this->horactual = strtotime ( '+'.$v->minute.' minute' , $this->horactual ) ; // utilizo "horactual"
          $this->horactual = strtotime ( '+'.$v->second.' second' , $this->horactual ) ; // utilizo "horactual"
          $this->horactual = date ( 'Y-m-d  H:i:s' , $this->horactual );
        }
          $this->horactual;
    }

  public function index() {
    return view('cuerpo.index');
  }
  public function captcha() {
    /*clase encargada de mostrar img para recaptcha*/
    $capchat = new Captcha;
    $valor = $capchat->generarCaptcha('*');

    $vali = base64_decode($valor['code']);

    $response = array(
      'codigo' => $vali,
      'data'   => $valor
    );

    return response()->json($response);
  }

  public function formulario(){

  	$perfil   = referential::perfiles();
  	$statusF  = referential::statusF();
  	$statusL  = referential::statusL();
    $areasLic = areasLic::all();
  	$degree   = Degree::all();
    $ambitos  = ambito::ambitos();
  	$otros    = Degree::all();
    $title    = "FORMULARIO DE REGISTRO";
    $Uactive  = "background:#000";
    return view('cuerpo.unete',compact('otros','perfil','statusF','areasLic','ambitos','statusL','title','Uactive','degree'));
  }
  /*cambiar contrase«Ða*/
  public function contrasenaChange(Request $request){

    if (Auth::guest()) {
      $id = $request['user'];
    }
    else {
      $id = Auth::user()->id;
    }
    $newpass = bcrypt($request['password']);
    $resul=DB::insert("update users SET password = '".$newpass."' WHERE users.id = ".$id." ");

    if($resul) {
      $respuesta=1;
      return response()->json($respuesta);
    }

  }
  public function store(Request $request) {
  

    if ($request['recaptcha'] == 'true') 
    {
      $listAmbitos = ambito::ambitos();

      $ct=count($listAmbitos);
      $last_resp_uno = DB::table('respuestas')->insertGetId([
        'respuesta1' => $request['respuesta1-1'],
        'respuesta2' => $request['respuesta2-1'],
        'respuesta3' => $request['respuesta3-1']
      ]);//obtener ultimo id insertado

      $last_resp_dos = DB::table('respuestas')->insertGetId([
        'respuesta1' => $request['respuesta1-2'],
        'respuesta2' => $request['respuesta2-2'],
        'respuesta3' => $request['respuesta3-2']
      ]);//obtener ultimo id insertado

      $areaslic = DB::select("select areasLic from degrees where id = '".$request['formAcademicLIC']."' ");

      foreach ($areaslic as $area) {
        $areaslic = $area->areasLic;

      }

      $ultimo_Vin = DB::table('vinculacions')->insertGetId([
        'nombreCompleto'                => $request['nombreCompleto'],
        'id_perfil'                     => $request['id_perfil'],
        'id_status_formacion'           => $request['id_status_formacion'],
        'formAcademicLIC'               => $request['formAcademicLIC'],
        'id_area_LIC'                   => $areaslic,
        'formdeMaestria'                => $request['formdeMaestria'],
        'formEspecialidad'              => $request['formEspecialidad'],
        'formDoctorado'                 => $request['formDoctorado'],
        'formPostGrado'                 => $request['formPostGrado'],
        'id_status_gestion_laboral'     => $request['id_status_gestion_laboral'],
        'id_respuestas_gestion_laboral' => $last_resp_uno,
        'id_status_form_docencia'       => $request['id_status_form_docencia'],
        'id_respuestas_form_docencia'   => $last_resp_dos,
        'id_status_invest_bio'          => $request['id_status_invest_bio'],
        'investigacion_temas'           => $request['investigacion_temas'],
        'telefono'                      => $request['telefono'],
        'telefono2'                     => $request['telefono2'],
        'correo'                        => $request['correo'],
        'correo2'                       => $request['correo2'],
        'palabras_claves'               => $request['palabras_claves'],
        'gestion_laboral'               => $request['gestion_laboral'],
        'formacion_docencia'            => $request['formacion_docencia'],
        'temas_area'                    => $request['temas_area'],
        'relacion'                      => $request['relacion'],
        'created_at'                    => $this->horactual
    ]);//obtener ultimo id insertado

    $ambito1 = count($request['gestion_laboral_areas']);
      for($d=1;$d<=$ct;$d++){
        $registros[$d]=0;     
      }

      if ($ambito1 > 0) 
      {
        foreach ($listAmbitos as $ambitos)  {
          $idA = $ambitos->id ;
          $check = 0;
          foreach($request['gestion_laboral_areas'] as $value) {
              if ($idA == $value) {
                $check = 1;
              }
          }
          DB::insert('insert into vinculacions_ambitos ( id, id_vinculacions, id_ambitos, id_tipo, cheked,created_at) values (NULL, '.$ultimo_Vin.', '.$ambitos->id.', 11, '.$check.',"'.$this->horactual.'")');      
        }
      }
      else {
        foreach ($listAmbitos as $ambitos) {
          $idAmbito=$ambitos->id;
           
          $tablaP1                  = new vinculacions_ambitos;
          $tablaP1->id_vinculacions = $ultimo_Vin;
          $tablaP1->id_ambitos      = $ambitos->id;
          $tablaP1->id_tipo         = 11;
          $tablaP1->cheked          = 0;
          $resulP1                  = $tablaP1->save();
        }
      }
      $ambito2 = count($request['gestion_docencia_areas']);

      if ($ambito2 > 0) 
      {
        foreach ($listAmbitos as $ambitos)  {
          $idA = $ambitos->id ;
          $check = 0;
          foreach($request['gestion_docencia_areas'] as $value) {
              if ($idA == $value) {
                $check = 1;
              }
          }
          DB::insert('insert into vinculacions_ambitos ( id, id_vinculacions, id_ambitos, id_tipo, cheked,created_at) values (NULL, '.$ultimo_Vin.', '.$ambitos->id.', 12, '.$check.',"'.$this->horactual.'")');    
        }
      }
      else {
        foreach ($listAmbitos as $ambitos) {
          $idAmbito=$ambitos->id;
           
          $tablaP1                  = new vinculacions_ambitos;
          $tablaP1->id_vinculacions = $ultimo_Vin;
          $tablaP1->id_ambitos      = $ambitos->id;
          $tablaP1->id_tipo         = 12;
          $tablaP1->cheked          = 0;
          $resulP1                  = $tablaP1->save();
        }
      }
      $ambito3 = count($request['gestion_inves_areas']);
      if ($ambito3 > 0) 
      {
        foreach ($listAmbitos as $ambitos)  {
          $idA = $ambitos->id ;
          $check = 0;
          foreach($request['gestion_inves_areas'] as $value) {
              if ($idA == $value) {
                $check = 1;
              }
          }
          DB::insert('insert into vinculacions_ambitos ( id, id_vinculacions, id_ambitos, id_tipo, cheked,created_at) values (NULL, '.$ultimo_Vin.', '.$ambitos->id.', 13, '.$check.',"'.$this->horactual.'")');       
        }
      }
      else {
        foreach ($listAmbitos as $ambitos) {
          $idAmbito=$ambitos->id;
           
          $tablaP1                  = new vinculacions_ambitos;
          $tablaP1->id_vinculacions = $ultimo_Vin;
          $tablaP1->id_ambitos      = $ambitos->id;
          $tablaP1->id_tipo         = 13;
          $tablaP1->cheked          = 0;
          $resulP1                  = $tablaP1->save();
        }
      }
      $cabecera  = 'Información de Bióetica';
      $accion    = 'Nuevo Registro';
      $dataName  = $request['nombreCompleto'];
      $contenido = 'Se acaba de registrar en nuestra web <br> Usuario: <b>'.$dataName.'</b>';

    self::correo($dataName, $accion, $cabecera, $contenido);

      $msj ="Gracias por registrarte, tu perfil está en proceso de validación";
      return response()->json($msj);
      
    }
    else {
      $msj ="Verifica el recaptcha";
      return response()->json($msj);
    }
  }

  /*funcion encargada de  correos al administrador*/
  public function correo($dataName, $accion, $cabecera, $contenido) {
    Mail::send('emails.contact',compact('contenido','cabecera'), 
      function ($m) use ($dataName, $accion) {
        /*correo predeterminado del cual se enviaran correos*/
        $m->from('mundoweb321@gmail.com', 'Bióetica');
        $m->to('mundoweb321@gmail.com','Bioetica')->subject($accion);
      }
    );
  }

 
  public function perfil(){

    if (Auth::user()->perfil_id != 1 ) {
      $id = Auth::user()->id_vinculacion;
      $detalles = vinculacion::listadoU($id);
      $perfil   = referential::perfiles();
      $statusF  = referential::statusF();
      $statusL  = referential::statusL();
      $areasLic = areasLic::areasLic();
      $degree   = Degree::all();
      $ambitos  = ambito::ambitos();
      $selectA1 = vinculacions_ambitos::selectA1($id);
      $selectA2 = vinculacions_ambitos::selectA2($id);
      $selectA3 = vinculacions_ambitos::selectA3($id);

      //dd($selectA1);
      return view('usuarios.perfil',compact('detalles','degree','perfil','statusF','statusL','areasLic','ambitos','selectA1','selectA2','selectA3'));
    }
    else {
      return redirect::to('/');
    }
  }

  public function ActualizarDatos(Request $request){

        
    $idU = $request['idUser'];
    $listAmbitos = ambito::ambitos();
    /*si cambia su nombre y el correo principal tambien cambiar el de logeo*/
    $Reg            = new Registry;
    $Reg->author    = Auth::user()->id;
    $Reg->action    = 'actualizacion';
    $Reg->addressee = $request['idUser'];
    $Reg->created_at = $this->horactual;
    $Reg->save();

    $user = DB::select("UPDATE users SET  name = '".$request['nombreCompleto']."'  ,email = '".$request['correo']."'  WHERE users.id_vinculacion = ".$idU."");

    $rl             = respuesta::find($request['idrespuestas1']);
    $rl->respuesta1 = $request['rl1'];
    $rl->respuesta2 = $request['rl2'];
    $rl->respuesta3 = $request['rl3'];
    $rl->save();

    $rd=respuesta::find($request['idrespuestas2']);
    $rd->respuesta1 = $request['rd1'];
    $rd->respuesta2 = $request['rd2'];
    $rd->respuesta3 = $request['rd3'];
    $rd->save();

    $ambito1 = count($request['gestion_laboral_areas']);
    if ($ambito1 > 0) {  
      $distinto = '';
      foreach($request['gestion_laboral_areas'] as $sel1) { 
        /*poner true a los valores selecionados*/
        vinculacions_ambitos::where('id','=', $sel1)->update(['cheked' => 1 ,'updated_at'=> $this->horactual]);
        /*unir los valores seleccionados para ingresar a la consulta*/
        $distinto .= " and id !=".$sel1;
      }
      $falseAmb1 = DB::select(" select id from vinculacions_ambitos  where id_vinculacions = ".$idU." and id_tipo = 11 ".$distinto." ");
      foreach($falseAmb1 as $valueid) { 
        vinculacions_ambitos::where('id','=', $valueid->id)->update(['cheked' => 0 ,'updated_at'=> $this->horactual]);
      }

    }
    else {
      /*en caso de los check esten todos vacios*/
      $falseAmb1 = vinculacions_ambitos::select('id')->where('id_vinculacions',$idU)->get();

      foreach($falseAmb1 as $valueid) { 
        vinculacions_ambitos::where('id', $valueid->id)
          ->where('id_tipo',11)
          ->update([
            'cheked'     => 0,
            'updated_at' => $this->horactual
          ]);
      }
    }
    $ambito2 = count($request['gestion_docencia_areas']);
    if ($ambito2 > 0) {  
      $distinto2 = '';
      foreach($request['gestion_docencia_areas'] as $sel2) { 
       // poner true a los valores selecionados
        vinculacions_ambitos::where('id','=', $sel2)->update(['cheked' => 1 ,'updated_at'=> $this->horactual]);
        /*unir los valores seleccionados para ingresar a la consulta*/
        $distinto2 .= " and id !=".$sel2;
      } 
      $falseAmb1 = DB::select(" select id from vinculacions_ambitos  where id_vinculacions = ".$idU." and id_tipo = 12 ".$distinto2." ");
      foreach($falseAmb1 as $valueid) { 
        vinculacions_ambitos::where('id','=', $valueid->id)
          ->update([
            'cheked'     => 0,
            'updated_at' => $this->horactual
          ]);
      }
    }
    else {
      /*en caso de los check esten todos vacios*/
      $falseAmb1 = vinculacions_ambitos::select('id')
        ->where('id_vinculacions',$idU)
        ->get();
      foreach($falseAmb1 as $valueid) { 
        vinculacions_ambitos::where('id', $valueid->id)
          ->where('id_tipo',12)
          ->update([
            'cheked'    => 0,
            'updated_at'=> $this->horactual
          ]);
      }
    }

    $ambito3 = count($request['gestion_inves_areas']);
    if ($ambito3 > 0) {  
      $distinto3 = '';
      foreach($request['gestion_inves_areas'] as $sel3) { 
        /*poner true a los valores selecionados*/
        vinculacions_ambitos::where('id','=', $sel3)->update(['cheked' => 1 ,'updated_at'=> $this->horactual]);
        /*unir los valores seleccionados para ingresar a la consulta*/
        $distinto3 .= " and id !=".$sel3;
      } 
  
      $falseAmb1 = DB::select(" select id from vinculacions_ambitos  where id_vinculacions = ".$idU." and id_tipo = 13 ".$distinto3." ");
      foreach($falseAmb1 as $valueid) {
        vinculacions_ambitos::where('id','=', $valueid->id)
          ->update([
            'cheked'    => 0,
            'updated_at'=> $this->horactual
          ]);
      }
    }
    else {
      /*en caso de los check esten todos vacios*/
      $falseAmb1 = vinculacions_ambitos::select('id')->where('id_vinculacions',$idU)->get();
      foreach($falseAmb1 as $valueid) { 
        vinculacions_ambitos::where('id', $valueid->id)
          ->where('id_tipo',13)
          ->update([
            'cheked'    => 0,
            'updated_at'=> $this->horactual
          ]);
      }
    }

     $areaslic = DB::select("select areasLic from degrees where id = '".$request['formAcademicLIC']."' ");

      foreach ($areaslic as $area) {
        $areaslic = $area->areasLic;

      }


    $vn = vinculacion::find($idU);
    $vn->nombreCompleto            = $request['nombreCompleto'];
    $vn->id_perfil                 = $request['id_perfil'];
    $vn->id_status_formacion       = $request['id_status_formacion'];
    $vn->formAcademicLIC           = $request['formAcademicLIC'];
    $vn->id_area_LIC               = $areaslic;
    $vn->formdeMaestria            = $request['formdeMaestria'];
    $vn->formEspecialidad          = $request['formEspecialidad'];
    $vn->formDoctorado             = $request['formDoctorado'];
    $vn->formPostGrado             = $request['formPostGrado'];
    $vn->id_status_gestion_laboral = $request['id_status_gestion_laboral'];
    $vn->id_status_form_docencia   = $request['id_status_form_docencia'];
    $vn->id_status_invest_bio      = $request['id_status_invest_bio'];
    $vn->investigacion_temas       = $request['investigacion_temas'];
    $vn->telefono                  = $request['telefono'];
    $vn->telefono2                 = $request['telefono2'];
    $vn->correo                    = $request['correo'];
    $vn->correo2                   = $request['correo2'];
    $vn->gestion_laboral               = $request['gestion_laboral'];
    $vn->formacion_docencia            = $request['formacion_docencia'];
    $vn->temas_area                    = $request['temas_area'];
    $vn->palabras_claves           = $request['palabras_claves'];
    $vn->relacion           = $request['relacion'];
    
    $result                        = $vn->save();
    if ($result) {
      $msj ="Datos Actualizados";
      return response()->json($msj);
    }
  }

  /*dar de alta al usuario para que sus datos no puedan ser vistos*/
  public function deAlta($id){
    $res=  User::where('id_vinculacion', $id)
      ->update([
        'status'     => 9,
        'updated_at' => $this->horactual
      ]);
    
    if ($res) {
      $msj ="Usuario dado de Baja";
      return response()->json($msj);
    }
  }

  public function Activo($id){
    $res=  User::where('id_vinculacion', $id)
      ->update([
        'status'     => 8,
        'updated_at' => $this->horactual
      ]);
    if ($res) {
      $msj ="Usuario Reincorporado";
      return response()->json($msj);
    }
  }

  public function mail(Request $request, $correo=false, $rol=false){
    /*buscar si el correo esta siendo utilizado por otro usuario*/
    if ($request->ajax() && $correo !=false ) {
    /*consulta a la base de datos*/
      if ($rol == 'Admin') {
        $data = User::select('email')->where('email',$correo)->get();
        return response()->json($data);
      }
      else {
        $data = vinculacion::email($correo);
        return response()->json($data);
      }
      /*se obtiene la respuesta del query para ser filtrada por el condicional */     
    }
  }

  public function saveformacion(Request $request){

     $areaslic = DB::select("select color from areaslic where id = '".$request['area']."' ");
    $otro = DB::select("select id_tooltip_carrera from degrees where  areasLic =  '".$request['area']."' AND title like '%Otro%'");
    $otra = DB::select("select id_tooltip_carrera from degrees where  areasLic =  '".$request['area']."' AND title like '%Otra%'");


    if (!empty($otro)) {
      $tooltip = $otro;
    }elseif (!empty($otra)) {
      $tooltip = $otra;
    }
   foreach ($areaslic as $value) {
        $color = $value->color;
   } 
   foreach ($tooltip as $values) {
        $id_tooltip = $values->id_tooltip_carrera;
   }  
 

    $degree = new Degree;
    $degree->title    = $request['title'];
    $degree->id_tooltip_carrera   = $id_tooltip;
    $degree->areasLic = $request['area'];
    $degree->color    = '#'.$color;
    $degree->mapView  = 0;
    $degree->otros    = 0;
    $degree->save();


    return response()->json();

  }

  //creacion de select2 en UNETE  agrupacion de subcategorias por su area correspondiente
 public function areasLicSelect(){
    $areasLic = areasLic::all();
    $degree   = Degree::all();
    $array  =[];
    $info  =[];
     foreach($areasLic as $licDe){
        foreach($degree->where('areasLic',$licDe->id)->where('otros',0) as $dee){
          if ($licDe->id != 8 ) 
          {
             $datas=['id'=>"".$dee->id."",'text'=>$dee->title]; array_push($info, $datas); 
          }
        }
        foreach($degree->where('otros',1) as $dee){
          if ($licDe->id == 8) 
          {
             $datas=['id'=>"".$dee->id."",'text'=>$dee->title];   array_push($info, $datas);
          }
        }
     $data = ['text' =>$licDe->acronimo,"children"=>$info];
            array_push($array, $data); 
            $info  =[];
     }
    return response()->json($array);
  }

  public function norepeat(Request $request,$title=false){
    /*buscar si el correo esta siendo utilizado por otro usuario*/
    if ($request->ajax() && $title !=false ) {
    /*consulta a la base de datos*/
        $data = Degree::select('title')->where('title',$title)->get();
        return response()->json($data);
      /*se obtiene la respuesta del query para ser filtrada por el condicional */     
    }
  }
}
