<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\vinculacion;
use App\referential;
use App\areasLic;
use App\ambito;
use App\User;
use App\vinculacions_ambitos;
use App\respuesta;
use App\Registry;
use App\Degree;
use Auth;
use DB;
use Mail;
class AdminController extends Controller
{
    public $horactual;

       public function __construct()
    {
        $this->middleware('auth');
         $ubication = DB::select("select * from times");
        foreach ($ubication as  $v) {
         date_default_timezone_set($v->zona);

          $fecha = date('Y-m-d H:i:s'); //inicializo la fecha con la hora
          $this->horactual = strtotime ( '+'.$v->hour.' hour' , strtotime ( $fecha )) ;
          $this->horactual = strtotime ( '+'.$v->minute.' minute' , $this->horactual ) ; // utilizo "horactual"
          $this->horactual = strtotime ( '+'.$v->second.' second' , $this->horactual ) ; // utilizo "horactual"
          $this->horactual = date ( 'Y-m-d  H:i:s' , $this->horactual );
        }
          $this->horactual;

        
    }

    //retornar hora en panel administrativo
    public function hora(){
      return date('d-M-Y h:i a');
    }

    /*listado de usaurios resgistrados*/
    public function listado($id){
  		$detalles = vinculacion::listadoU($id);
  		$perfil   = referential::perfiles();
  		$statusF  = referential::statusF();
  		$statusL  = referential::statusL();
  		$areasLic = areasLic::areasLic();
  		$ambitos  = ambito::ambitos();
  		$selectA1 = vinculacions_ambitos::selectA1($id);
  		$selectA2 = vinculacions_ambitos::selectA2($id);
  		$selectA3 = vinculacions_ambitos::selectA3($id);
      $degree   = Degree::all();
		 // dd($detalles);
    	return view('usuarios.detalles',compact('detalles','degree','perfil','statusF','statusL','areasLic','ambitos','selectA1','selectA2','selectA3'));
    }

 /*llenar notificacion de registrados en sistema*/
    public function notiUsers(){

      $list = vinculacion::notificar();
      return response()->json($list);

    }
    //notificar de nuevas licenciaturas agregadas
  public function notiLice(){

      $list = Degree::notificar();
      return response()->json($list);

    }
    public function licvista($id){


      $lic= Degree::find($id);
      $lic->visto=1;
      $lic->save();

      return response()->json();

    }
    public function adminUpdate(Request $request){


      $id = Auth::user()->id;
      $idV = Auth::user()->id_vinculacion;



      $resul = User::where('id','=', $id)->update(['name' => $request['name'] ,'email'=> $request['email'] ]);

     if (!empty($idV)) {
        vinculacion::where('id','=', $idV)->update(['correo'=> $request['email'] ]);
     }

     if ($resul) {
       $msj = 1;
      return response()->json($msj);
     }

    }

    public function fichaAdimin($id){

      $data = User::select('*')->where('id',$id)->get();
      return view('admin.perfil',compact('data'));

    }

    /*generar contrasena aleatoria*/
    public function generaPass(){
	      $cadena = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
	      $longitudCadena=strlen($cadena);
	      $pass = "";
	      $longitudPass=10;
	      for($i=1 ; $i<=$longitudPass ; $i++){
	        $pos=rand(0,$longitudCadena-1);
	        $pass .= substr($cadena,$pos,1);
	      }
	      return $pass;
    }

    /*aceptar usuario y enviar correo para informar */
    public function validarUser($id){


      
      $dataUser    = vinculacion::find($id);
      $dataEmail   = $dataUser->correo;
      $dataName    = $dataUser->nombreCompleto;
      $dataperfil  = $dataUser->id_perfil;
      $contraseña  = self::generaPass();

      $usuario  = new User;
      $usuario->name=$dataName;
      $usuario->email=$dataEmail;
      $usuario->perfil_id=$dataperfil;
      $usuario->id_vinculacion=$id;
      $usuario->status=8;#status activo por defecto
      $usuario->password=bcrypt($contraseña);
      $resul=$usuario->save();

      $Reg = new Registry;
      $Reg->author = Auth::user()->id;
      $Reg->action = 'aceptado';
      $Reg->addressee = $id;
      $Reg->created_at = $this->horactual;
      $Reg->save();
      
      if ($resul) {
        $cabecera= 'Información de Bióetica';
        $accion = 'Sus datos han sido validados';
        $contenido = 'Se te envia este Mensaje con el fin de que ya puedes acceder a parte de nuestro sistema <br><br> <center><b>porfavor no responder este Mensaje</b></center>  <br><br> Usuario: <b>'.$dataEmail.'</b> <br><br> Contraseña: <b>'.$contraseña.'</b>';
      self::correo($dataEmail,$dataName,$accion,$cabecera,$contenido );
        $mensaje = "Guardado correctamente";
        return response()->json($mensaje);

        }else{
        	$mensaje = "Error al cargar datos";
        	return response()->json($mensaje);
        }
    	
    }
  
  /*funcion encargada de  correos y tomar plantilla para ser enviada*/
  public function correo($dataEmail,$dataName,$accion,$cabecera,$contenido ){
      Mail::send('emails.contact',compact('contenido','cabecera'), function ($m) use ($dataEmail,$dataName,$accion) {
          /*correo predeterminado del cual se enviaran correos*/
          $m->from('mundoweb321@gmail.com', 'Bióetica');
          $m->to($dataEmail,$dataName)->subject($accion);
      });
  }

  /*comprobar se el usuario tiene una sesion*/
  public function sesionA($id){

    $resultado = DB::select("select id_vinculacion from users where id_vinculacion = ".$id." ");

      if ($resultado == true) {
       $m = true;
       return response()->json($m);
      }else{
         $m = false;
       return response()->json($m);
      }
  }

  /*elimiar de forma permanente los datos*/
  public function deletedU($id){

     /*hacemos una consulta para extraer los id de las respuestas y poder eliminarlas */

     $vincu = vinculacion::select('nombreCompleto')->where('id',$id)->get();
    foreach ($vincu as $data) {
    $Reg = new Registry;
          $Reg->author = Auth::user()->id;
          $Reg->action = $data->nombreCompleto.' Eliminado';
          $Reg->addressee = '';
          $Reg->created_at = $this->horactual;
          $Reg->save();
    }
    $idRespuestas = vinculacion::select('id_respuestas_gestion_laboral', 'id_respuestas_form_docencia')->where('id',$id)->get();


    /*eliminar respuestas*/
    foreach ($idRespuestas as $idR) {


      respuesta::find($idR->id_respuestas_gestion_laboral)->delete();
      respuesta::find($idR->id_respuestas_form_docencia)->delete();

    }
    /*eliminar ambitos donde exista el usuario*/
    $tp  = vinculacions_ambitos::where('id_vinculacions',$id)->delete();

    /*cunsultamos en tabla users para obtener */
    $idUser = User::select('id', 'id_vinculacion')->where('id_vinculacion',$id)->get();

       /*eliminar datos del usuario en la tabla vinculacion*/
  
    foreach ($idUser as $idU) {
      /*eliminar datos del usuario de manera bruta */
      $user = DB::insert("DELETE FROM users WHERE users.id = ".$idU->id." ");

    }

      $vin = DB::insert("DELETE FROM `vinculacions` WHERE `vinculacions`.`id` = ".$id." ");

 

   
    
        $mensaje= "Datos Eliminados";
      return response()->json($mensaje);
      
    

  }



  /*eliminar de forma logiaca los datos asignando la fecha de eliminación*/
   public function destroy($id)
    {
      $user = User::find($id);
      $user->status = 9;
      $user->save();


      $desac = User::find($id);
      $desac->delete();

     

      $mensaje = "Sesión desactivada correctamente!";
      return response()->json($mensaje);
        
    }
  /*quitar fecha de eliminación*/

    public function activar($id)
    { 

      User::withTrashed()->where('users.id', $id)->restore();
      $user = User::find($id);
      $user->status = 8;
      $user->save();
      $mensaje = "Sesión Activada correctamente!";
          return response()->json($mensaje);
        
    }
        public function historialsView(){ 
          $listado   = vinculacion::listadoU();
          $historial = Registry::historial();
          $Dactive   ="active";

          return view('historial',compact('listado','historial','Dactive'));
    }

    public function viewEditLic($id){
      $lic = DB::table('degrees')->select('*')->where('id',$id)->get();
      $areasLic = areasLic::all();
      $otros    = Degree::otros();
          return view('admin.modalLic')->with('lic',$lic)->with('areasLic',$areasLic)->with('otros',$otros); 
    }
   
    public function gestionarLic(){
          $Lactive   ="active";
          return view('admin.licenciaturas',compact('Lactive'));
    }
    public function dataLic(){
      $listado =Degree::listado();
      $data=array("data"=>$listado);
      return response()->json($data);
    }
     public function updateLic(Request $request){

     

      $change = DB::table('vinculacions')->select('*')->where('formAcademicLIC',$request['id'])->get();

      foreach ($change as  $val) {
        DB::insert("update vinculacions SET id_area_LIC = '".$request['areasLic']."', deleted_at = NULL WHERE vinculacions.id = ".$val->id." ");
      }

      $areaslic = DB::select("select color from degrees where areasLic = '".$request['areasLic']."' ");
      $otro = DB::select("select id_tooltip_carrera from degrees where  areasLic =  '".$request['areasLic']."' AND title like '%Otro%'");
      $otra = DB::select("select id_tooltip_carrera from degrees where  areasLic =  '".$request['areasLic']."' AND title like '%Otra%'");

      if (!empty($otro)) {
        $tooltip = $otro;
      }elseif (!empty($otra)) {
        $tooltip = $otra;
      }

     foreach ($tooltip as $values) {
          $id_tooltip = $values->id_tooltip_carrera;
     }  

        $lic=Degree::find($request['id']);
        $lic->id_tooltip_carrera    = $id_tooltip;
        $lic->title    = $request['title'];
        $lic->color    = $request['color'];
        $lic->areasLic = $request['areasLic'];
        $lic->otros    = $request['otros'];
        $resul=$lic->save();
        if ($resul) {
          return response()->json();
        }
    }

    //al ser eliminada la licenciatura buscar usuarios asociados y enviarlos a otros de cada area
    public function destroyLic($id){

      $change = DB::table('vinculacions')->select('*')->where('formAcademicLIC',$id)->get();
      foreach ($change as  $val) {
        if ($val->id_area_LIC == 1) {
         DB::insert("update vinculacions SET formAcademicLIC = '7', deleted_at = NULL WHERE vinculacions.id = ".$val->id." ");
        }
        if ($val->id_area_LIC == 2) {
         DB::insert("update vinculacions SET formAcademicLIC = '26', deleted_at = NULL WHERE vinculacions.id = ".$val->id." ");
        }
        if ($val->id_area_LIC == 3) {
         DB::insert("update vinculacions SET formAcademicLIC = '38', deleted_at = NULL WHERE vinculacions.id = ".$val->id." ");
        }
        if ($val->id_area_LIC == 4) {
         DB::insert("update vinculacions SET formAcademicLIC = '44', deleted_at = NULL WHERE vinculacions.id = ".$val->id." ");
        }
        if ($val->id_area_LIC == 5) {
         DB::insert("update vinculacions SET formAcademicLIC = '48', deleted_at = NULL WHERE vinculacions.id = ".$val->id." ");
        }
        if ($val->id_area_LIC == 6) {
         DB::insert("update vinculacions SET formAcademicLIC = '58', deleted_at = NULL WHERE vinculacions.id = ".$val->id." ");
        }
        if ($val->id_area_LIC == 7) {
         DB::insert("update vinculacions SET formAcademicLIC = '63', deleted_at = NULL WHERE vinculacions.id = ".$val->id." ");
        }
      }

      $lic=DB::table('degrees')->where('id',$id)->get();
      foreach ($lic as $value) {
        $Reg = new Registry;
        $Reg->author = Auth::user()->id;
        $Reg->action = 'Elimino Lic. '.$value->title;
        $Reg->addressee = 'NULL';
        $Reg->created_at = $this->horactual;
        $Reg->save();
      }

      $res=Degree::where('id',$id)->delete();
      return response()->json($res);
    }

    public function timesConfig(){
      $config = DB::select("select * from times");
      return view('admin.times',compact('config'));
    }
    public function editarTimes(Request $request){

     $r = DB::insert("update times SET zona = '".$request['zona']."', `hour` = '".$request['hour']."', `minute` = '".$request['minute']."' WHERE `times`.`id` = 1");

     if ($r) {
        $msj="Configuración Guardada";
        return response()->json($msj);
     }else{
        $msj="Error al guardar";
        return response()->json($msj);
     }


    }

}
