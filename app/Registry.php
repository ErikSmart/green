<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Registry extends Model {
  protected $table = 'registris';
  protected $primarykey = 'id';
  protected $fillable = [
    'author', 'action', 'addressee',
  ];

  public function Author(){
    return $this->belongsTo('App\User','author');
  }
  public function Addressee(){
    return $this->belongsTo('App\vinculacion','addressee');
  }

  public static function historial(){
    return DB::select("select 
          u.name,
          v.nombreCompleto,
          registris.action,
           DATE_FORMAT(registris.created_at, '%d-%m-%Y %h:%i:%s %p') as fecha
          from registris
          LEFT JOIN vinculacions as v on registris.addressee = v.id
          JOIN users as u on registris.author = u.id
          order by registris.created_at desc");
  }
}
