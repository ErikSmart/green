<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class mapaModel extends Model
{
    //Modelo para hacer el respectivo calculo para retornar valores que se encargaran de graficar el mapa de colors y llenar el listado de colores
    protected $table = 'usuariosactivos';

    public function degrees(){
         return $this->hasMany(Degree::class, 'id');
    }
    
}

