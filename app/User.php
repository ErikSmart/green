<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use DB;
class User extends Authenticatable
{
    use SoftDeletes;
    
    protected $table = 'users';
    protected $fillable = [
        'name', 'email', 'password','status'
    ];
    protected $hidden = [
        'password', 'remember_token','perfil_id','id_vinculacion'
    ];
    protected $dates = ['deleted_at'];//para hacer un borrado logico de datos

    public  static function sesion($email){
        return  DB::select("
             select users.id,users.email, referentials.descripcion as perfil
             from users 
             join referentials on referentials.id = users.perfil_id 
             where users.email = '".$email."'");
    }

   /* ==============buscar el codigo en caso de existir================*/
    public static function codigo($codigo){

        return DB::table('users')->select('id','codigo')->where('codigo',$codigo)->get();

    }
        public function Registry(){
         return $this->hasMany('App\Registry','id');
    }
}
