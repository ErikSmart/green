  mapShow=(url,id,dataP)=>{

      //debugger;
      var canvas = document.getElementById(id);
      paper.setup(canvas);

      var cat1=[
        {
          "num_cat":1,
          "categoria": "Ciencias de la Salud",
          "data": [
            {"subcat":1,"nombre":"Médico Cirujano", "cantidad":79,"url":"http://google.com","color":"#563F97"},
            {"subcat":2,"nombre":"Psicología", "cantidad":0,"url":"http://google.com","color":"#D2BFED"},
            {"subcat":3,"nombre":"Nutrición", "cantidad":22,"url":"http://google.com","color":"#A999BE"},
            {"subcat":4,"nombre":"Terapia Física y Rehabilitación", "cantidad":93,"url":"http://google.com","color":"#A781E6"},
            {"subcat":5,"nombre":"Cirujano Dentista", "cantidad":1,"url":"http://google.com","color":"#7A55B5"},
            {"subcat":6,"nombre":"Biotecnología", "cantidad":51,"url":"http://google.com","color":"#7776B0"},
            {"subcat":7,"nombre":"Otros", "cantidad":31,"url":"http://google.com","color":"#713F96"}
          ]
        },
        {
          "num_cat":2,
          "categoria": "C. Sociales y Humanidades",
          "data": [
            {"subcat":1,"nombre":"Lenguas Modernas y Gestión Cultural", "cantidad":10,"url":"http://google.com","color":"#DA8B2E"},
            {"subcat":2,"nombre":"Derecho", "cantidad":23,"url":"http://google.com","color":"#FDC475"},
            {"subcat":3,"nombre":"Comunicación", "cantidad":94,"url":"http://google.com","color":"#CE9D63"},
            {"subcat":4,"nombre":"Diseño Gráfico", "cantidad":58,"url":"http://google.com","color":"#FC972F"},
            {"subcat":5,"nombre":"Administración Pública y Gobierno", "cantidad":27,"url":"http://google.com","color":"#F6CD9D"},
            {"subcat":6,"nombre":"Pedagogía", "cantidad":100,"url":"http://google.com","color":"#CE7000"},
            {"subcat":7,"nombre":"Historia", "cantidad":67,"url":"http://google.com","color":"#916E46"},
            {"subcat":8,"nombre":"Otros", "cantidad":66,"url":"http://google.com","color":"#774917"}
          ]
        },
        {
          "num_cat":3,
          "categoria": "C. Económicas",
          "data": [
            {"subcat":1,"nombre":"Administracion de Empresas", "cantidad":38,"url":"http://google.com","color":"#B1063A"},
            {"subcat":2,"nombre":"Contabilidad", "cantidad":32,"url":"http://google.com","color":"#D5446B"},
            {"subcat":3,"nombre":"Economía", "cantidad":99,"url":"http://google.com","color":"#F71C60"},
            {"subcat":4,"nombre":"Actuaria", "cantidad":60,"url":"http://google.com","color":"#FA7892"},
            {"subcat":5,"nombre":"Otros", "cantidad":51,"url":"http://google.com","color":"#82082D"}

          ]
        },
        {
          "num_cat":4,
          "categoria": "C. Naturales",
          "data": [
            {"subcat":1,"nombre":"Ingeniería Ambiental", "cantidad":200,"url":"http://google.com","color":"#489E43"},
            {"subcat":2,"nombre":"Ingeniería Biomédica", "cantidad":20,"url":"http://google.com","color":"#7B9B46"},
            {"subcat":3,"nombre":"Química", "cantidad":80,"url":"http://google.com","color":"#A9F5B7"},
            {"subcat":4,"nombre":"Geografía", "cantidad":40,"url":"http://google.com","color":"#99CA5D"},
            {"subcat":5,"nombre":"Física", "cantidad":30,"url":"http://google.com","color":"#6A946C"},
            {"subcat":6,"nombre":"Otros", "cantidad":100,"url":"http://google.com","color":"#75D081"}

          ]
        },
        {
          "num_cat":5,
          "categoria": "C. Religiosas",
          "data": [
            {"subcat":1,"nombre":"Ciencias Religiosas", "cantidad":22,"url":"http://google.com","color":"#E4C656"},
            {"subcat":2,"nombre":"Teología", "cantidad":35,"url":"http://google.com","color":"#DED3A6"},
            {"subcat":3,"nombre":"Filosofía", "cantidad":200,"url":"http://google.com","color":"#E2E452"},
            {"subcat":4,"nombre":"Otros", "cantidad":38,"url":"http://google.com","color":"#B59A25"}


          ]
        },
        {
          "num_cat":6,
          "categoria": "C. Tecnológicas",
          "data": [
            {"subcat":1,"nombre":"Ingeniería Industrial", "cantidad":100,"url":"http://google.com","color":"#32497D"},
            {"subcat":2,"nombre":"Ingeniería Mecatrónica", "cantidad":1,"url":"http://google.com","color":"#97B8C6"},
            {"subcat":3,"nombre":"Ingeniería Civil", "cantidad":200,"url":"http://google.com","color":"#4D86A1"},
            {"subcat":4,"nombre":"Ingeniería Informática", "cantidad":200,"url":"http://google.com","color":"#77CCE0"},
            {"subcat":5,"nombre":"Ingeniería en Construcción", "cantidad":1,"url":"http://google.com","color":"#28ADC0"},
            {"subcat":6,"nombre":"Otros", "cantidad":54,"url":"http://google.com","color":"#64A9B9"}

          ]
        },
        {
          "num_cat":7,
          "categoria": "Arte y Cultura",
          "data": [
            {"subcat":1,"nombre":"Artes Visuales", "cantidad":42,"url":"http://google.com","color":"#AEA095"},
            {"subcat":2,"nombre":"Música Contemporánea", "cantidad":59,"url":"http://google.com","color":"#C59E8F"},
            {"subcat":3,"nombre":"Teatro y Actuación", "cantidad":200,"url":"http://google.com","color":"#91566A"},
            {"subcat":4,"nombre":"Fotografía", "cantidad":200,"url":"http://google.com","color":"#C68684"},
            {"subcat":5,"nombre":"Otros", "cantidad":66,"url":"http://google.com","color":"#7C5A72"},

          ]
        },
        {
          "num_cat":8,
          "categoria":"Otros",
              "data":[
                {"subcat":1,"nombre":"Otras", "cantidad":36,"url":"http://google.com","color":"#932C7D"},
                {"subcat":2,"nombre":"Otras", "cantidad":52,"url":"http://google.com","color":"#B63089"},
                {"subcat":3,"nombre":"Otras", "cantidad":44,"url":"http://google.com","color":"#932C7D"},
                {"subcat":4,"nombre":"Otras", "cantidad":62,"url":"http://google.com","color":"#F7ACE4"},

              ]
        }];

      var cat1_json=JSON.stringify(cat1);
      var token = $("#tokenCanvas").val();
      $.ajax({
          url: url,
          type: 'GET',
          dataType: 'JSON',
          data: {ids:dataP},
          headers:{'X-CSRF-TOKEN': token},
          success: function (data) {
            getJson(JSON.stringify(data));
          }
      });
      //getJson(cat1_json);
    };
    showConent().then(mapShow('/mapData','myCanvas'));

    mapClick=(idfilter)=>{
      var token = $("#tokenCanvas").val();
      $('#myUL').hide();
      $("#resultados").show();
      $("#listadoResult").hide();
      $("#graficaResult").show();



      $.ajax({
          url: '/mapResult',
          type: 'GET',
          dataType: 'JSON',
          data: {ids:idfilter},
          headers:{'X-CSRF-TOKEN': token},
          beforeSend: function(){  
            efecto();
            $(".result").empty();
            $("#capaRespuesta").addClass('capaRespuesta');
            $("#showButton").hide();
            $("#mostrar").show();
          },
          success: function (response) {
            $('.nohaydatos').empty();
            ocultaSelect();
            $('#tooltip').hide()
            $('#myCanvas').hide()
            if(!$.isArray(response) || !response.length){ 
                $('.nohaydatos').append('<div class="col-md-12" style=" border-bottom: 3px solid #ccc; padding-top: 15px; padding-bottom: 15px"><center>No se encontraron resultados</center></div>');
                $('#myInput').val('');
              }
            var Base64={
              _keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
              encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9+/=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/rn/g,"n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}
              }
            for (i = 0; i < response.data.length; i++){
              idArray.push(response.data[i].id);
              nameespacio = response.data[i].nombreCompleto;
              name = nameespacio.replace("%20"," ");
              name_ruta = name+"/"+response.data[i].id_vinculacions+"/"+response.where;
              ruta_ir = "/ver/"+name_ruta;
              ruta = Base64.encode("/ver/"+name_ruta);
            /*mostrar los valores donde la fecha deleted sea NULL*/
              if (response.data[i].status == 'Activo') {
                $('.result').append('<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style=" border-bottom: 3px solid #ccc; padding-top: 15px; padding-bottom: 15px"><a  onClick="insertCookie(\''+name+'\' , \' '+ruta+' \');" href="'+ruta_ir+'"><div class="media"><div class="col-lg-4 col-md-5 col-sm-4 col-xs-12" style="color:#fff;margin-right: 6px;position: relative;height: 95px;background-color: #'+response.data[i].color+' ">'+response.data[i].nombreCompleto+'</div><div class="media-body" style="font-size: 14px;padding-top:10px;"><h4 class="media-heading" style="font-size: 12px;"><i class="fa fa-circle" style="color: #'+response.data[i].color+' "></i> '+response.data[i].acronimo+'</h4> <p style="color:#000; padding-left:15px;font-size: 13px;line-height: inherit; margin-bottom:0;">'+response.data[i].gestion.substr(0,80)+'... </p></div></div></a></div> '); 
                $('.nohaydatos').empty();
              }
            $('#myInput').val('');

            }

            
          }
      });
    }