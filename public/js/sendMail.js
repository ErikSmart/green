 $(document).on('submit', '.sendMail', function(event) {
                
                event.preventDefault();
                var formu=$(this);
                var quien=$(this).attr("id");//quien es el formulario de donde provienen los datos
                //se declara el id del formulario con su ruta 
                if(quien=="f_sendmail"){ var miurl="/enviocorreo";}

                $.ajax({
                     
                     type: "POST",
                      url : miurl,
                      datatype:'json',
                      data : formu.serialize(),
                      beforeSend: function(){
                        $("#enviarcorreo").css("display","none");
                        $("#correoenviado").show();

                      },
                    
                      success : function(response){
                        $("#correoenviado").hide();
                        $("#enviarcorreo").css("display","block");
                        $("#name").val('');
                        $("#correo").val('');
                        $("#mensaje").val('');
                        Push.create("Bioética", {
                              body: "Correo enviadó Exitosamente",
                              icon: 'img/logo/logo-pie-anahuac.png',
                              timeout: 4000,
                              onClick: function () {
                                  window.focus();
                                  this.close();
                              }
                          });
                        }, //si ha ocurrido un error
                      error: function(response){  
                            $.confirm({
                              icon: 'fa fa-warning',title:'Hoops!',
                              content:"Fallo el envió del correo",
                              confirmButton: 'Confirmar',cancelButton: false,
                              animation: 'zoom', confirmButtonClass: 'btn-info',
                            });
                      }
                });
            });