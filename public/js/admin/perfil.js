
$(document).ready(function() {
    $('#searchMail').keyup(function(event) {
            
            $.get("/mail/"+event.target.value+"/Admin",function(response){
                    
                    for (i = 0; i < response.length; i++) {
                        
                        if (response[i].email) {

                            $("#msj_mailA").empty().append('Correo ya está siendo utilizado');
                            $("#sendbutton").attr('disabled', true);
                          
                        }

                    }  
                    if(response== 'false'){
                            $("#msj_mailA").empty();
                            $("#sendbutton").attr('disabled', false);
                            
                        }
                 });
            
        }); 


    $('#newpassword').keyup(function(event) {
        var pass1 =$('#newpassword').val();

      if(pass1.length < 6) {  
          $("#msj_pass1").empty().append("El m&iacute;nimo es 6 car&aacute;cteres");  
          $("#subContra").attr('disabled', true);
          
        } else{
          $("#msj_pass1").empty();  
          $("#subContra").attr('disabled', false);

        }
    });
    $('#password_confirmation').keyup(function(event) {
        var pass1 =$('#newpassword').val();
        var pass2 =$('#password_confirmation').val();

        if(pass2.length < 6) {  
          $("#msj_pass").empty().append("El m&iacute;nimo es 6 car&aacute;cteres");  
            
        } else{
          $("#msj_pass").empty();  

        }

        if (pass1 == pass2) {
          $("#subContra").attr('disabled', false);
        $("#msj_pass").empty();

        }else{
          $("#subContra").attr('disabled', true);
        $("#msj_pass").empty().append('La contraseña no coincide');
        }
      });
});


$(document).on("submit",".form_perfil",function(e){
//funcion para atrapar los formularios y enviar los datos
e.preventDefault();
         
var formu=$(this);
var quien=$(this).attr("id");//quien es el formulario de donde provienen los datos
//se declara el id del formulario con su ruta 
if(quien=="password_edit"){ var miurl="/cambiar_password";  var pass1 = $('#newpassword').val(); var pass2 = $('#password_confirmation').val();}
  if (pass1 != '' && pass2 != '' && pass1 == pass2 ) {
      $.ajax({
          type: "POST",
          url : miurl,
          data : formu.serialize(),
          success : function(response){
               if (response == 1) {
                  $.confirm({
                      icon:'glyphicon glyphicon-ok text-green',
                      title: 'Datos Actualizados',
                      content: '<center><h3>La actualizaci&oacute;n de tus datos se realizo correctamente</h3></center>',
                      confirmButtonClass: 'btn-success',
                      confirmButton: 'Confirmar',
                      cancelButton: false,
                      confirm: function () {
                        location.reload(true);
                       } 
                    });
               }
            }, //si ha ocurrido un error
          error: function(response){  
                $.confirm({
                  icon: 'fa fa-warning',
                  title:'Hoops!',
                  content:'Erro al cargar datos',
                  confirmButton: 'Confirmar',
                  cancelButton: false,
                  animation: 'zoom', confirmButtonClass: 'btn-info',
                });
          }
    });
  }else{
    $("#subContra").attr('disabled', true);
    $.dialog({title:false,content:'<cener><h2>Completar campos</h2></center>'});
  }
})

$(document).on("submit",".update_perfil",function(e){
//funcion para atrapar los formularios y enviar los datos
e.preventDefault();
         
var formu=$(this);
var quien=$(this).attr("id");//quien es el formulario de donde provienen los datos
//se declara el id del formulario con su ruta 
if(quien=="f_edit_perfil"){ var miurl="/edit_perfil";}
 
      $.ajax({
          type: "POST",
          url : miurl,
          data : formu.serialize(),
          success : function(response){
               if (response == 1) {
                  $.confirm({
                      icon:'glyphicon glyphicon-ok text-green',
                      title: 'Datos Actualizados',
                      content: '<center><h3>La actualizaci&oacute;n de tus datos se realizo correctamente</h3></center>',
                      confirmButtonClass: 'btn-success',
                      confirmButton: 'Confirmar',
                      cancelButton: false,
                      confirm: function () {
                        location.reload(true);
                       } 
                    });
               }
            }, //si ha ocurrido un error
          error: function(response){  
                $.confirm({
                  icon: 'fa fa-warning',
                  title:'Hoops!',
                  content:'Erro al cargar datos',
                  confirmButton: 'Confirmar',
                  cancelButton: false,
                  animation: 'zoom', confirmButtonClass: 'btn-info',
                });
          }
    });
  
})

$(document).on("submit",".form_actualizar",function(e){
      
      e.preventDefault();
      var formu=$(this);
      var quien=$(this).attr("id");
      var token = $("#token").val();
      if(quien=="form_update_usuario"){ var miurl="/actualizardatos";  }
       $.ajax({
              url: miurl,
              type:"POST",
              headers:{'X-CSRF-TOKEN':token},
              data:formu.serialize(),

              beforeSend: function(){
                     $("#guardando").show();
                     $("#guardar").hide();
                  },
              success : function(response){
                  $("#guardando").hide();
                 $("#guardar").show();
                  $.confirm({
                      title: 'Mensaje Exitoso',
                      icon:'fa fa-check text-green',
                      content:'<center><h2>'+response+'</h2></center>',
                      confirmButton: 'Continuar',
                      cancelButton: false,
                      confirmButtonClass: 'btn-success',
                      animation: 'zoom', 
                      confirm: function(){
                         

                         location.reload(true);
                      }
                  });
              },
              error:function(){
                   $("#guardando").hide();
                   $("#guardar").show();

              }
          });


})