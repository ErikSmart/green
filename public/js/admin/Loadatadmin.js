$(document).ready(function() {
	LoadRegistros();
	LoadLicenciaturas();
	Reloj();
});

function LoadRegistros(){  
	$("#newRegistros").empty();   
	$.get('/newRegistros', function(response) {

		if( !$.isArray(response) ||  !response.length ){  
		  $('#newRegistros').empty().append('<li><a href="#"><i class="fa fa-times text-danger"></i> No hay usuarios registrados </a></li>');
		}
		cant = response.length ;
		$("#cantRN").empty().append(''+cant+'');
		$("#cantRT").empty().append('Hay '+cant+' nuevos registros');

		for (var i = 0; i <= (cant-1); i++) {
			
			$("#newRegistros").append('<li><a href="#" onclick="fichaUsuario('+response[i].idV+');"><i class="fa fa-user-plus text-aqua"></i> Se ha unido '+response[i].nombreCompleto+' </a></li>');	

		}

	});
}
function LoadLicenciaturas(){     
	$("#newLice").empty();
	$.get('/newLice', function(response) {

		if( !$.isArray(response) ||  !response.length ){  
		  $('#newLice').append('<li><a href="#"><i class="fa fa-times text-danger"></i> No hay registro de nuevas Lic. </a></li>');
		}
		cant = response.length ;
		$("#LicN").empty().append(''+cant+'');
		$("#LicNT").empty().append('Se agregarón '+cant+' Lic.');

		for (var i = 0; i <= (cant-1); i++) {
			
			$("#newLice").append('<li ><a href="#" title="marcar como visto al dar click" onclick="licvista('+response[i].id+')" ><i class="fa fa-plus text-green"></i> Se añadio Lic. '+response[i].title+'   </a></li>');	

		}

	});
}
function licvista(id){
	
	$.get('/licvista/'+id, function(response) {
		$(".licO").removeClass('licO').addClass('open');
	$(".licA").removeAttr('aria-expanded').attr({'aria-expanded': true});
		LoadLicenciaturas();
	});
}

function Reloj() {



	$.get('/hora', function(response) {
				document.getElementById('hora').innerHTML = response;
	});
	setTimeout('Reloj()', 10000);
}


$(document).on("submit",".form_times",function(e){
// $('sendbutton').on("click", function(e) {
//funcion para atrapar los formularios y enviar los datos

        e.preventDefault();
        var formu=$(this);
        var quien=$(this).attr("id");
        var token = $("#token").val();
           if(quien=="form_time"){ var miurl="/dataTimes";  }
         $.ajax({
              url: miurl,
              type:"POST",
              headers:{'X-CSRF-TOKEN':token},
			  data:formu.serialize(),

              success : function(response){
              		$('#ModalTimes').modal('hide');
                   $.dialog({
                   	title:'Mensaje',
                      icon:'fa fa-check fa-lg text-green',
                      content:'<center><h2>'+response+'</h2></center>'
                  });
              },
              error:function(){
          			$.dialog({
          				title:'Mensaje',
                      icon:'fa fa-times fa-lg text-danger',
                      content:'<center><h2>'+response+'</h2></center>'
                  });
              }
          })
  });
