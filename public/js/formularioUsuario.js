/*validar campos requeridos*/
$(document).ready(function(){
    
    /*buscar correo e informar en caso de que exista*/
    $('#searchMail').keyup(function(event) {
            
            $.get("/mail/"+event.target.value+"",function(response){
                    
                    for (i = 0; i < response.length; i++) {
                        
                        if (response[i].correo) {

                            $("#msj_mailT").empty().append('Correo ya está siendo utilizado');
                            // why disable the sendButton?
                            // $("#sendbutton").attr('disabled', true);
                            $('#first_name').parent().css('border-color','#a94442');
                        }

                    }  
                    if(response== 'false'){
                            $("#msj_mailT").empty();
                            $('#first_name').parent().css('border-color','none');
                            
                            $("#sendbutton").attr('disabled', false);
                            
                        }
                 });
            
        }); 


  /*validacion de formulario*/
    $('#form_usuario').bootstrapValidator({

         fields: {  
            
             nombreCompleto: {
                 validators: {
                     notEmpty: {
                         message: 'El Nombre de Usuario es requerido'
                     }
                 }
             },
            
     
                formAcademicLIC: {
                 validators: {
                     notEmpty: {
                         message: 'Campo requerido'
                     }
                 }
             },
     
      

              telefono: {
                 validators: {
                     notEmpty: {
                         message: 'Campo requerido'
                     }
                 }
             },
            correo: {
                validators: {
                    notEmpty: {
                         message: 'El correo es requerido'
                     },

                    emailAddress: {
                        message: 'Ingrese un correo valido'
                    },
                    
                        regexp: {
                            regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
                            message: 'El correo es incorrecto'
                        }
                }
            },
                 palabras_claves:{
                    validators: {
                    notEmpty:{
                        message:'Campo requerido'
                    }
                 }
             },
         }
           
   });
   
})

