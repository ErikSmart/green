var var_snap;
var area_categorias=[];
var pathfullySelected



function setGray(cat){

	cat.fillColor="CCCCCC";


}

function setGrayMultiPath(Cat){

	var num=Cat.length;
    for(var i=1; i>=num; i++){
  	    Cat[i].fillColor="CCCCCC";
    }


}



var xobject = 0;
function eventCat(Cat) {

  Cat['path'].onClick=function(event){
		//window.location.href = Cat['url'];
		mapClick(Cat['url']);
  }
  Cat['path'].onMouseEnter=function(event){
	  document.body.style.cursor = "pointer";

	  var x=event.event.x+10;
	  var y=event.event.y-10;
	  xobject=$('#tooltip')[0].clientWidth;
	  $('#tooltip').html(Cat['text']);
	  
	  $('#tooltip').removeClass('ocultar');
	  $('#tooltip').css({left:x, top:y-20});
  }
  Cat['path'].onMouseMove=function(event){ 
	  var x=event.event.x+10;
	  var y=event.event.y-10;

	  var xw = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
	  var yw = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
	  if((xw-x)<($('#tooltip')[0].clientWidth+20)){
		  $('#tooltip').css({left:x-xobject-20, top:y+20});
	  }
	  else{
		  $('#tooltip').css({left:x, top:y});
	  }

  }

  Cat['path'].onMouseLeave=function(event){
		document.body.style.cursor = "default";
		$('#tooltip').addClass('ocultar');
		
		//tooltipRect.visibility=false;
	    //this.parent.children['tooltip'].remove();
	}

}






function getJson(json_var){
	var obj=JSON.parse(json_var);
	var num_data=obj.length;
	for (var i=0; i<num_data; i++) {
		var obj_simple;
		obj_simple=obj[i];
		obj_simple['data'].sort(function(a,b) {return (a.cantidad > b.cantidad) ? 1 : ((b.cantidad > a.cantidad) ? -1 : 0);} );
		switch(obj_simple['num_cat']){
			case 1:
				Cat1( obj_simple);
				break;
			case 2:
				Cat2( obj_simple);
				break;
			case 3:
				Cat3( obj_simple);
				break;
			case 4:
				Cat4( obj_simple);
				break;
			case 5:
				Cat5( obj_simple);
				break;
			case 6:
				Cat6( obj_simple);
				break;
			case 7:
				Cat7( obj_simple);
				break;
			case 8:
				Cat8( obj_simple);
				break;
		}
	}
	CatOrdered();

}

function CatOrdered(){
	var TotalArea=0;
	var TotalCantidad=0;
	var i;
	var PorcentAct=[];
	var PorcentDef;
	var scale=0;
	var x;
	var y;
	var x_m=0;
	var y_m=0;
	var punto;
	var mover;

	for (i=1; i<=8; i++){
		TotalArea=TotalArea+area_categorias[i].area;
	}
	for (i=1; i<=8; i++){
		TotalCantidad=TotalCantidad+area_categorias[i].cantidad;
		PorcentAct[i]=area_categorias[i].area/TotalArea;
	}


	for (i=1; i<=8; i++){
		mover=0;
		PorcentDef=area_categorias[i].cantidad/TotalCantidad;
		scale=PorcentDef/PorcentAct[i];
		if(scale>1.2){
			scale=1.2;
		}
		if(scale<0.15){
			scale=0.15
		}
		x=area_categorias[i].grupo.position._x;
		y=area_categorias[i].grupo.position._y;
		x_m=0;
		y_m=0;
		switch (i){
			case 1:
				x=x-50;
				y=y-40;


				break;
			case 2:
				y=y-190;
				/*punto=new paper.Point(x,y);
				area_categorias[i].grupo.scale(scale,punto);
				area_categorias[i].scale=scale;*/
				/*while (area_categorias[i].grupo.intersects(area_categorias[1].grupo)){
					mover=1;
					x_m=x_m+10
					x=area_categorias[i].grupo.position._x;
					y=area_categorias[i].grupo.position._y;
					x=x+x_m;
					y=y+y_m;
					area_categorias[i].grupo.position=new paper.Point(x, y);

				}*/
				break;
			case 3:
				x=x+150;
				y=y-50;
				break;
			case 4:
				//x=x-80;
				//y=y-80;
				//mover=1;
				//x_m=((area_categorias[1].scale-1)*(-50.52)+(scale-1)*(50.48));
				//y_m=((area_categorias[2].scale-1)*(-50)+(scale-1)*(96.426));
				break;
			case 5:
				y=y-50;
				x=x-100;
				break;

			case 6:
				y=y+150;
				x=x-0;
				break;

		}


		pathfullySelected = true;
		//x=area_categorias[i].grupo.position._x+x;
		//y=area_categorias[i].grupo.position._y+y;
		punto=new paper.Point(x,y);
		area_categorias[i].grupo.scale(scale,punto);
		area_categorias[i].scale=scale;
		if (mover===1){

			x_m=area_categorias[i].grupo.position._x+x_m;
			y_m=area_categorias[i].grupo.position._y+y_m

			area_categorias[i].grupo.position=new paper.Point(x_m, y_m);
			//x=area_categorias[i].textPath.position._x;
			//y=area_categorias[i].textPath.position._y
			//x=x+x_m;
			//y=y+y_m;
			//area_categorias[i].textPath.position=new paper.Point(x,y);

		}
		createAlignedText('      '+area_categorias[i].text, area_categorias[i].textPath, {fontSize: 18});
	}
	/*for (i=1; i<=8; i++){

	}*/

}

function Cat1(obj){
	var porcMinimo=0.1; //10%
	var colorVacio='#cccccc'
	var numSubCats=7;
	var areaBase;
	var orden=[];
	var total_area=0;
	var totalCantidad=0;
	var Cat=[];
	//var SubCatArray=[];
	var cant_vacio;
	var subCatBase;
	var indexBase;
	var prevPorcent=0;



	area_categorias[1]={cantidad:0,area:0, grupo:new paper.Group(), texto:'', textPath:null, scale:null};



	Cat=[
		//0
		{   sub:1,
			area:0,
			path:new paper.Path('M5.4,221.051c71.6-132,262.9-259.8,267.2,88c14.5-146.7,25.6-280.8-6.8-420.1c-22.2-96.4-58-171.8-104.8-134.1 c-85.3,67.1-128.8,329-171,501.8L5.4,221.051z'),
			color:'#563f97',

			base:0,
			porc:0,
			xinit:151.41199552888756,
			yinit:30,
			dir:90,
			url:'', text:''


		},
		//1
		{   sub:2,
			area:0,
			path:new paper.Path('M184.9,119.9c19-7.4,58.7-21.7,66.1,25.8c4.8-32.9,19.4-81,13.4-116.8c2.2-59-2.4-211-15.5-214.6c-41.3-46.9-128.3-19.9-143.1,41.5 C71.7-2.1,86,92.3,83.6,257.9C89.2,168,157.9,131.9,184.9,119.9z'),
			color:'#d2bfed',

			base:0,
			porc:0,
			xinit:151.41199552888756,
			yinit:30,
			dir:90,
			url:'', text:''

		},
		//2
		{
			sub:6,
			area:0,
			path:new paper.Path('M202.6,417.7C144.821,363.511,49,556.1,102.4,680.4C195.9,898,360.297,565.598,202.6,417.7z'),
			color:'#713f96',

			base:0,
			porc:0,
			xinit:0,
			yinit:0,
			dir:0,
			url:'', text:''
		},
		//3
		{
			sub:7,
			area:0,
			path:new paper.Path('M202.6,417.7C144.821,363.511,49,556.1,102.4,680.4C195.9,898,360.297,565.598,202.6,417.7z'),
			color:'#7776b0',

			base:0,
			porc:0,
			xinit:0,
			yinit:0,
			dir:0,
			url:'', text:''
		},
		//4

		{
			sub:4,
			area:0,
			path:new paper.Path('M230,142.9c-50.55,53.231-47.4,112.8,0.4,168.4c43.7,49.8,273.65,342,370.65,333.2c72.1-9.1,46.7-679.7,8-729 C565.35-140.1,395.3,68.3,333.4,82.1C283.2,98.7,256.4,115.1,230,142.9z'),
			color:'#a781e6',

			base:0,
			porc:0,
			xinit:0,
			yinit:0,
			dir:0,
			url:'', text:''
		},
		//5
		{
			sub:5,
			area:0,
			path:new paper.Path('M116.4,419.3c9.36-82.68-87.323-81.831-133.022-128.016c-39.98-40.405-63.07-97.782-91.555-146.17 C-127.685,111.976-150.544-68.819-197.95-63.5c-42.202,4.735-212.783,84.164-222,118c-27.98,102.722-79.846,541.695-5,628 c42.17,48.627,299.182-147.645,357.217-154.101C8.519,519.916,106.454,507.152,116.4,419.3z'),
			color:'#7a55b5',

			base:0,
			porc:0,
			xinit:0,
			yinit:0,
			dir:0,
			url:'', text:''
		},
		//6
		{
			sub:3,
			area:0,
			path:new paper.Path('M134.1,145.3c-36.9,60.1-38.9,126.5-27.9,195c4.4,27.4,15,58.9,43.1,69.9c28.8,11.4,60.9-8.8,75.5-33.3c17.6-29.6,6.6-60.3-9.8-87.5c-19-31.8-35.9-59.7-26.2-98.2c7-28.2,39.5-74.5,9.4-99.2C172.2,70.8,143.7,129.3,134.1,145.3z'),
			color:'#a999be',

			base:1,
			porc:0,
			xinit:0,
			yinit:0,
			dir:0,
			url:'', text:''
		}




	]





	var num_data=Cat.length;
	var total=0;
	var i;
	var aumentaX=35;

	cant_vacio=0;
	totalCantidad=0;
	//todo reconocer valores no definidos en el objeto ingresado por json, para lo cual se agrega como 0
	//obtiene el area total
	for (i=0; i<num_data; i++){
		/*if (i==5){
			Cat[i]['path'].position=new paper.Point(Cat[i]['path'].position._x+aumentaX,Cat[i]['path'].position._y+190);
		}
		else{
			Cat[i]['path'].position=new paper.Point(Cat[i]['path'].position._x+aumentaX,Cat[i]['path'].position._y+140);
		}*/

		/*Cat[i]['path'].position=new paper.Point(Cat[i]['path'].position._x,Cat[i]['path'].position._y+190);*/

		Cat[i]['text']=obj['data'][i]['nombre'];
		if(obj['data'][i]['cantidad']===0){
			cant_vacio=cant_vacio+1;
			Cat[i]['color']=colorVacio;

		}
		totalCantidad=totalCantidad+obj['data'][i]['cantidad'];
		if(Cat[i]['base']===1){
			//asigna el path base
			subCatBase=Cat[i]['path'];
			total_area=subCatBase.area;
			area_categorias[1].area=subCatBase.area;
			indexBase=i;
		}
		//debugger;
		var sortIndex;
		sortIndex=Cat[i]['sub']-1;
		Cat[i]['url']=obj['data'][i]['url'];
		//Cat[i]['path'].parent=area_categorias[1].grupo;
		 area_categorias[1].grupo.addChild(Cat[i]['path']);


		//total_area=total_area+area;


	}

	area_categorias[1].cantidad=totalCantidad;
	//todo: Validar que el cálculo para valores vacios está bien
	//Calcular porcentajes
	num_data=Cat.length;



	var restoPorcVacio=1-cant_vacio*0.1;
	for (i=0; i<num_data; i++){
		var x;
		var porc;
		var area;
		if(cant_vacio===Object.keys(obj['data']).length) {
			porc = 1 / cant_vacio;

		}
		else{
			if(obj['data'][i]['cantidad']===0){
				porc=0.025;
			}
			else{
				porc=(obj['data'][i]['cantidad']/totalCantidad)*restoPorcVacio;
			}

		}
		area=Math.ceil(porc*total_area);

		Cat[i]['porc']=porc;
		Cat[i]['area']=area;
		Cat[i]['text']=Cat[i]['text']+': '+obj['data'][i]['cantidad'];

	}


	var Cant10=0;
	var numCat10=0;
	for (i=0; i<num_data; i++){

		if (Cat[i]['porc']<0.02){
			Cant10=Cant10+(0.02-Cat[i]['porc'])
			Cat[i]['porc']=0.02;
			numCat10=numCat10+1;
		}

	}

	numCat10=numSubCats-numCat10;


	for (i=0; i<num_data; i++){

		if (Cat[i]['porc']>0.02){
			if( ( Cat[i]['porc']-(Cant10/numCat10) )<0.02 ){
				Cant10=Cant10-(Cat[i]['porc']-0.02);
				Cat[i]['porc']=0.02;

			}
			else{
				Cat[i]['porc']=Cat[i]['porc']-Cant10/numCat10;
				Cant10=Cant10-Cant10/numCat10;

			}

			numCat10=numCat10-1;
		}

	}

	var result;
	i=30;
	var flagInicio=0;
	var varx=0;
	var porcentResult, porcentSubcat;
	Cat[indexBase]['path'].strokeWidth=2;
	Cat[indexBase]['path'].fillColor=Cat[indexBase]['color'];
	Cat[indexBase]['path'].strokeColor='white';


	while(varx<=numSubCats-1){
	//break;
		// if(result){
		// 	result.remove();
		// }
		switch(varx){
			case 0:
				if(flagInicio===0){
					i=1;
					flagInicio=1;
				}
				Cat[varx]['path'].position=new paper.Point(151.41199552888756+aumentaX, 88.14909243178101+i);
				break;
			case 1:
				if(flagInicio===0){
					i=1;
					flagInicio=1;
				}
				Cat[varx]['path'].position=new paper.Point(160.41199552888756+aumentaX-10, 64.20984327207412+i);
				break;
			case 2:
				if(flagInicio===0){
					i=1;
					flagInicio=1;
				}
				Cat[varx]['path'].position=new paper.Point(170.08373789001706+aumentaX, 583.6524654036434-i);
				break;
			case 3:
				if(flagInicio===0){
					i=1;
					flagInicio=1;
				}
				Cat[varx]['path'].position=new paper.Point(170.08373789001706+aumentaX, 583.6524654036434-i);
				break;
			case 4:
				if(flagInicio===0){
					i=60;
					flagInicio=1;
				}
				//console.log(Cat[varx]);
				Cat[varx]['path'].position=new paper.Point(389.2855524124327+aumentaX-i, 239.47977803161533);
				break;
			case 5:
				if(flagInicio===0){
					i=1;
					flagInicio=1;
				}
				Cat[varx]['path'].position=new paper.Point(-82.02625740992369+aumentaX+i, 400.2656571812021);
				break;
		}

	//grafica Subcat1



		result=inters(Cat,varx, indexBase);

		porcentResult=Math.abs(result.area/total_area);
		porcentSubcat=Math.abs(Cat[varx]['porc']);

		if( (porcentResult>=(porcentSubcat-0.01) && porcentResult<=(porcentSubcat+0.01))){
			//console.log("Cat[varx]['path'].area"+Cat[varx]['path'].area);
			result.select=true;
			result.fillColor=Cat[varx]['color'];
			result.strokeColor='white';
			result.strokeWidth=2;

			Cat[varx]['path']=result;
			//console.log(Cat[varx]['path'].area);
			eventCat(Cat[varx])
			//console.log(Cat[indexBase]['path'].area);
			result=Cat[indexBase]['path'].subtract(Cat[varx]['path']);
			Cat[indexBase]['path']=result;
			//console.log(Cat[indexBase]['path'].area);
			varx=varx+1;
			flagInicio=0;
			if(varx===indexBase){
				eventCat(Cat[varx]);
				varx=varx+1;

			}
			porcentResult=0;
			porcentSubcat=0;
			// if(varx===7){
			// 	break;}


			//if(varx>2){
			// 	clearInterval(interval);
			// }
		}
		if(porcentResult<(porcentSubcat-0.01)){
			i=i+4;
			//debugger;
		}
		else if(porcentResult>(porcentSubcat+0.01)){
			//debugger;
			i=i-2;
		}

	}
	//todo graficar bien los datos




	//var path = new paper.Path('M131.7,484.4c-11-68.5-3.6-143.4,33.3-203.5c3.4-5.4-38.7,63.5,0,0 c-4.6,7.6-11.2,18.2-16.4,27c5.4-9,10.8-18,16.4-27c-1.8,3.2-3.8,6.4-5.8,9.4c2-3,3.8-6.2,5.8-9.4c-1.8,3-3.6,6-5.4,8.6 c9.6-16,38.1-74.5,64.1-53.3');
	//area_categorias[1].grupo.addChild(path);

	area_categorias[1].text=obj['categoria'];
	area_categorias[1].grupo.position=new paper.Point(area_categorias[1].grupo.position._x+40, area_categorias[1].grupo.position._y+150);
	area_categorias[1].textPath=new paper.Path('M170.089,538.375c-49.974,0.125-47.489-60.973-44-82.125c-4.501-71,13.14-101.594,25-186c9.235-65.725,46.593-58.317,68.85-88.875');
	area_categorias[1].textPath.parent=area_categorias[1].grupo;
	var punto=new paper.Point(area_categorias[1].grupo.position._x-30,area_categorias[1].grupo.position._y);
	area_categorias[1].grupo.scale(0.8, punto);

	//area_categorias[1]
	//pathfullySelected = true;
	//createAlignedText('      '+obj['categoria'], path, {fontSize: 18});


}
function inters(Cat,i, indexBase){
	var result=Cat[i]['path'].intersect(Cat[indexBase]['path']);

	return result;
}
function Cat2(obj){
	var porcMinimo=0.1; //10%
	var colorVacio='#cccccc'
	var numSubCats=8;
	var areaBase;
	var orden=[];
	var total_area=0;
	var totalCantidad=0;
	var Cat=[];
	//var SubCatArray=[];
	var cant_vacio;
	var subCatBase;
	var indexBase;

	/*	orden[0]={sub:0, base:0};
	 orden[1]={sub:1, base:0};
	 orden[2]={sub:2, base:1};*/

	area_categorias[2]={cantidad:0,area:0, grupo:new paper.Group(), texto:'', textPath:null, scale:null};


	Cat=[
		//0
		{   sub:1,
			area:0,
			path:new paper.Path('M8.4,298.9c26.3,9.6,52,19.3,79.4,28.4c2.2-0.9,5.6-1.7,7.8-2c12.9-1.1,24-5.2,33.5-7.6c26.3-7.6,77.1-2.5,97.8-11.2 c36.9-16.2,39.7-47.5,52-66.1c14-20.8,15.1-41.7,10.6-62.7c-3.4-11.3-5.6-22.3-12.9-33.5c-3.4-5.2-5.6-10.5-10.6-15.6c0-0.3-1.1-1.7-2.2-3.2c-501-102.5-458.3,25.5-395.7,96.4c-9.5,15,14,30.9,74.9,52.4C-36.8,282.2-15,290.5,8.4,298.9z'),
			color:'#da8b2e',

			base:0,
			porc:0,
			xinit:151.41199552888756,
			yinit:30,
			dir:90,
			url:'', text:''


		},
		//1
		{   sub:3,
			area:0,
			path:new paper.Path('M540.841,296.901 c-10.797-53.145-33.461-73.767-87.972-56.872c-56.197,17.418-107.009,49.18-168.199,35.479 c-57.569-12.89-101.378-44.165-162.377-19.204C-20.255,314.637-132.147,619.116,60.15,646.85c64.78,9.343,521.415-24.218,588-26 c75.288-2.015,47.373-110.692,33.363-179.473C676.572,417.124,551.228,348.029,540.841,296.901z'),
			color:'#ce9d63',

			base:0,
			porc:0,
			xinit:151.41199552888756,
			yinit:30,
			dir:90,
			url:'', text:''

		},
		//2
		{
			sub:8,
			area:0,
			path:new paper.Path('M658.7,292.7 c-2.2,10.3-6.8,19.4-13.8,27.3c41.3-9.7,90.1-2.6,134.1,0c225.2,13.5,276.2-17.5,276.2-40.6c0-161.7-374.6-151.7-447-185 C629.4,127.7,669,253.4,658.7,292.7z'),
			color:'#774917',

			base:0,
			porc:0,
			xinit:0,
			yinit:0,
			dir:0,
			url:'', text:''
		},
		//3
		{
			sub:6,
			area:0,
			path:new paper.Path('M1108.4,235.9 c-17-36.4-31-83.3-63.1-109.6C1007,94.9,939.4,91,892.5,87c-128.1-11.1-272.6,9.4-395-37.5c0,113,121.5,177.7,213.4,211.9 c123,45.7,259,61.6,389.6,63.6C1051.3,313,1125.1,264.9,1108.4,235.9C1102.6,223.4,1127.6,269.2,1108.4,235.9z'),
			color:'#ce7000',

			base:0,
			porc:0,
			xinit:0,
			yinit:0,
			dir:0,
			url:'', text:''
		},
		//4

		{
			sub:7,
			area:0,
			path:new paper.Path('M631.7,460.1 c-4.7-24.3-9.9-48.9-21.6-72.4c-23.9-44.6-10.7-148.9-74.4-140.9c-78.9,9.8-169.7,115.1-272.7,92c-58.9-9.8-66.5,14-126,31.3c20.4,12.7,75.9,31.4,97.5,46.1c170.2,123.8,264.2,78.1,392,124.5C639.9,515.6,637.5,487.2,631.7,460.1z'),
			color:'#916e46',

			base:0,
			porc:0,
			xinit:0,
			yinit:0,
			dir:0,
			url:'', text:''
		},
		//5
		{
			sub:5,
			area:0,
			path:new paper.Path('M1063,316.1c-13.1-21.2,31.6-65.9,1.7-81.1 c-36.8-18.8-81.1-47.2-125.4-61c-14.6-4.2-221.8-41.6-237.4-45c7.7,21.6-53.1,189.2-49.9,211.4c14.3,4.8,163.7,22.3,176.5,28.6 c39.2,19.6,134.6-9.5,184,3.8C1041.2,380.4,1075,336.1,1063,316.1z'),
			color:'#f6cd9d',

			base:0,
			porc:0,
			xinit:0,
			yinit:0,
			dir:0,
			url:'', text:''
		},
		//6
		{
			sub:4,
			area:0,
			path:new paper.Path('M500.15,37.85c51.95,31.95,24.5,178.576,90,236.476c22.1,19.5,211.55-25.776,240.65-24.676c54,2.2,113.6,23.2,166.5,20.3 c89.3-4.8-15.1-145.2-43.6-161.9c-44.8-26.3-118.6-24.3-169.8-30c-59.7-6.7-111.75-4.25-162.25-32.5 c-1.484-0.83-11.25-0.5-25.75,0.589C579.209,47.392,456.002,10.699,500.15,37.85z'),
			color:'#fc972f',

			base:0,
			porc:0,
			xinit:0,
			yinit:0,
			dir:0,
			url:'', text:''
		},
		//7
		{
			sub:2,
			area:0,
			path:new paper.Path('M659,278.1c-18.9-57.2-100.6-113.5-155-124.5 c-35.3-7.2-72-12.4-108.2-11.6c-25.2,0.5-106.2,17.8-104.9,54.5c1,27.5,43.8,63.9,70.2,61c43.2-5,76.9-37.2,121.2-40.9 c37.6-3.2,64,16.9,89.1,42.4C578.6,266.2,677.2,335.1,659,278.1C652,256.9,665.4,298.1,659,278.1z'),
			color:'#efb67d',

			base:1,
			porc:0,
			xinit:0,
			yinit:0,
			dir:0,
			url:'', text:''
		}




	]


	var num_data=Cat.length;
	var total=0;
	var i;

	cant_vacio=0;
	totalCantidad=0
	//todo reconocer valores no definidos en el objeto ingresado por json, para lo cual se agrega como 0
	//obtiene el area total
	for (i=0; i<num_data; i++){
		
		Cat[i]['text']=obj['data'][i]['nombre'];
		if(obj['data'][i]['cantidad']===0){
			cant_vacio=cant_vacio+1;
			Cat[i]['color']=colorVacio;

		}
		totalCantidad=totalCantidad+obj['data'][i]['cantidad'];
		if(Cat[i]['base']===1){
			//asigna el path base
			subCatBase=Cat[i]['path'];
			total_area=subCatBase.area;
			area_categorias[2].area=subCatBase.area;
			indexBase=i;
		}
		var sortIndex;
		sortIndex=Cat[i]['sub']-1;
		Cat[i]['url']=obj['data'][i]['url'];
		area_categorias[2].grupo.addChild(Cat[i]['path']);
		//total_area=total_area+area;


	}
	area_categorias[2].cantidad=totalCantidad;
	//todo: Validar que el cálculo para valores vacios está bien
	//Calcular porcentajes
	num_data=Cat.length;



	var restoPorcVacio=1-cant_vacio*0.1;
	for (i=0; i<num_data; i++){
		var x;
		var porc;
		var area;
		if(cant_vacio===Object.keys(obj['data']).length) {
			porc = 1 / cant_vacio;

		}
		else{
			if(obj['data'][i]['cantidad']===0){
				porc=0.025;
			}
			else{
				porc=(obj['data'][i]['cantidad']/totalCantidad)*restoPorcVacio;
			}

		}
		area=Math.ceil(porc*total_area);

		Cat[i]['porc']=porc;
		Cat[i]['area']=area;
		Cat[i]['text']=Cat[i]['text']+': '+obj['data'][i]['cantidad'];

	}

	var Cant10=0;
	var numCat10=0;
	for (i=0; i<num_data; i++){

		if (Cat[i]['porc']<0.015){
			Cant10=Cant10+(0.015-Cat[i]['porc'])
			Cat[i]['porc']=0.015
			numCat10=numCat10+1;
		}

	}

	numCat10=numSubCats-numCat10;


	for (i=0; i<num_data; i++){

		if (Cat[i]['porc']>0.015){
			if( ( Cat[i]['porc']-(Cant10/numCat10) )<0.015 ){
				Cant10=Cant10-(Cat[i]['porc']-0.015);
				Cat[i]['porc']=0.015;

			}
			else{
				Cat[i]['porc']=Cat[i]['porc']-Cant10/numCat10;
				Cant10=Cant10-Cant10/numCat10;

			}

			numCat10=numCat10-1;
		}

	}


	var result;
	i=30;
	var flagInicio=0;
	var varx=0;
	var porcentResult=0, porcentSubcat=0;
	Cat[indexBase]['path'].strokeWidth=2;
	Cat[indexBase]['path'].fillColor=Cat[indexBase]['color'];
	Cat[indexBase]['path'].strokeColor='white';
	//posición inicial




	while(varx<=numSubCats-1){
		//break;
		//  if(result){
		//  	result.remove();
		//  }
		switch(varx){
			case 0://1
				if(flagInicio===0){
					i=1;
					flagInicio=1;
				}
				Cat[varx]['path'].position=new paper.Point(61.5434100370731+i, 207.2097917829845);
				break;
			case 1://3
				if(flagInicio===0){
					i=1;
					flagInicio=1;
				}
				Cat[varx]['path'].position=new paper.Point(240.86451985700626, 379.38223212603464-i);
				break;
			case 2://8
				if(flagInicio===0){
					i=1;
					flagInicio=1;
				}
				Cat[varx]['path'].position=new paper.Point(831.7-i, 208.84228638270017);
				break;
			case 3://6
				if(flagInicio===0){
					i=1;
					flagInicio=1;
				}
				Cat[varx]['path'].position=new paper.Point( 806.6410324443113, 187.25+i);
				break;
			case 4://7
				if(flagInicio===0){
					i=1;
					flagInicio=1;
				}
				//console.log(Cat[varx]);
				Cat[varx]['path'].position=new paper.Point(400.86451985700626, 379.38223212603464-i);
				break;
			case 5://5
				if(flagInicio===0){
					i=1;
					flagInicio=1;
				}
				Cat[varx]['path'].position=new paper.Point(863.5490453312989-i, 252.50926578795327);
				break;
			case 6://4
				if(flagInicio===0){
					i=1;
					flagInicio=1;
				}
				Cat[varx]['path'].position=new paper.Point(767.8983324195076-i, 216.88715954188325);
				break;
			/*case 7://2
				if(flagInicio===0){
					i=1;
					flagInicio=1;
				}
				Cat[varx]['path'].position=new paper.Point(-82.02625740992369+i, 262.2656571812021);
				break;*/
		}

		//grafica Subcat1

		//debugger;
		result=inters(Cat,varx, indexBase);

		porcentResult=Math.abs(result.area/total_area);
		porcentSubcat=Math.abs(Cat[varx]['porc']);

		if( (porcentResult>=(porcentSubcat-0.01) && porcentResult<=(porcentSubcat+0.01))|| i>1500 ){
			//console.log("Cat[varx]['path'].area"+Cat[varx]['path'].area);
			result.select=true;
			result.fillColor=Cat[varx]['color'];
			result.strokeColor='white';
			result.strokeWidth=2;
			Cat[varx]['path']=result;
			//console.log(Cat[varx]['path'].area);
			eventCat(Cat[varx])
			//console.log(Cat[indexBase]['path'].area);
			result=Cat[indexBase]['path'].subtract(Cat[varx]['path']);
			Cat[indexBase]['path']=result;
			//console.log(Cat[indexBase]['path'].area);
			varx=varx+1;
			flagInicio=0;
			if(varx===indexBase){
				eventCat(Cat[varx]);
				varx=varx+1;

			}
			porcentResult=0;
			porcentSubcat=0;
			// if(varx===7){
			// 	break;}


			//if(varx>2){
			// 	clearInterval(interval);
			// }
		}
		if(porcentResult<(porcentSubcat-0.01)){
			i=i+4;
			//debugger;
		}
		else if(porcentResult>(porcentSubcat+0.01)){
			//debugger;
			i=i-2;
		}

	}
	//todo graficar bien los datos

	area_categorias[2].textPath=new paper.Path('M231.939,144.722c61.355-28.002,142.587-32.548,198.655-23.605c40.156,6.405,204.955,35.224,217.556,107.066');
	area_categorias[2].text=obj['categoria'];
	area_categorias[2].textPath.parent=area_categorias[2].grupo;
	var punto=new paper.Point(area_categorias[2].grupo.position._x-140,area_categorias[2].grupo.position._y-80);
	area_categorias[2].grupo.scale(0.8, punto);

	//var path = new paper.Path('M292.7,184.2c-0.4-11.9,13.6-21.4,22.7-27.5c20.4-13.7,43-23.6,67.9-26.1 c39.4-4.2,82-1.2,121.2,10.1');
	//pathfullySelected = true;
	//createAlignedText('      '+obj['categoria'], path, {fontSize: 18});




}

function Cat3(obj){
	var porcMinimo=0.1; //10%
	var colorVacio='#cccccc'
	var numSubCats=5;
	var areaBase;
	var orden=[];
	var total_area=0;
	var totalCantidad=0;
	var Cat=[];
	//var SubCatArray=[];
	var cant_vacio;
	var subCatBase;
	var indexBase;
	var contarProblema=0;

	/*	orden[0]={sub:0, base:0};
	 orden[1]={sub:1, base:0};
	 orden[2]={sub:2, base:1};*/

	area_categorias[3]={cantidad:0,area:0, grupo:new paper.Group(), texto:'', textPath:null, scale:null};


	Cat=[
		//0
		{   sub:5,
			area:0,
			path:new paper.Path('M571.1,677.4c62.2,0,61.3-70.1,56.9-112.9c-2.1-22.8,14.5-86.9-7.8-99.3c-27.3-15.2-85.8,1.6-113.7,11.1 c-56.6,19-121.5,69.1-123,130C442.5,630.5,504.5,677.4,571.1,677.4z'),
			color:'#82082d',

			base:0,
			porc:0,
			xinit:151.41199552888756,
			yinit:30,
			dir:90,
			url:'', text:''


		},
		//1
		{   sub:4,
			area:0,
			path:new paper.Path('M232.15,266.517c-40.245-10.403-148.973,49.755-166.538,88.093c-21.108,46.072,4.764,73.281,23.304,112.639c16.749,35.556,92.902,35.413,123.127,27.415c29.46-7.795,99.39-19.417,126.957-25.664C456.667,442.333,421,315.333,232.15,266.517z'),
			color:'#fa7892',

			base:0,
			porc:0,
			xinit:151.41199552888756,
			yinit:30,
			dir:90,
			url:'', text:''

		},
		//2
		{
			sub:3,
			area:0,
			path:new paper.Path('M232.15,266.517c-40.245-10.403-148.973,49.755-166.538,88.093c-21.108,46.072,4.764,73.281,23.304,112.639c16.749,35.556,92.902,35.413,123.127,27.415c29.46-7.795,99.39-19.417,126.957-25.664C456.667,442.333,421,315.333,232.15,266.517z'),
			color:'#f71c60',

			base:0,
			porc:0,
			xinit:0,
			yinit:0,
			dir:0,
			url:'', text:''
		},
		//3
		{
			sub:2,
			area:0,
			path:new paper.Path('M232.15,266.517c-40.245-10.403-148.973,49.755-166.538,88.093c-21.108,46.072,4.764,73.281,23.304,112.639c16.749,35.556,92.902,35.413,123.127,27.415c29.46-7.795,99.39-19.417,126.957-25.664C456.667,442.333,421,315.333,232.15,266.517z'),
			color:'#f71c60',

			base:0,
			porc:0,
			xinit:0,
			yinit:0,
			dir:0,
			url:'', text:''
		},
		//4

		{
			sub:1,
			area:0,
			path:new paper.Path('M615.6,424.2c-1.9-21.3-9.2-41.7-18.4-60.9 c-9-18.8-17.2-38.2-28.8-55.6c-8.6-13-20.9-28.5-38.4-27.2c-17.3,1.3-26.9,17.8-36.5,30.1c-11.6,14.9-26.4,27.1-42.9,36.3 c-14.8,8.2-34.7,15.9-41,33.2c-6.2,17.3,5.6,35.8,18.6,46.4c17.2,14,38.1,6.3,57.9,3.7c16.4-2.2,32.8-3,48.9,2.1 c14.2,4.5,27.1,12.1,40.4,18.7c10,5,26.3,11.6,35.4,1.4c6.7-7.5,5.8-19.7,4.6-29z'),
			color:'#b1063a',

			base:1,
			porc:0,
			xinit:0,
			yinit:0,
			dir:0,
			url:'', text:''
		}




	]


	var num_data=Cat.length;
	var total=0;
	var i;

	cant_vacio=0;
	totalCantidad=0
	//todo reconocer valores no definidos en el objeto ingresado por json, para lo cual se agrega como 0
	//obtiene el area total
	for (i=0; i<num_data; i++){
		Cat[i]['text']=obj['data'][i]['nombre'];
		if(obj['data'][i]['cantidad']===0){
			cant_vacio=cant_vacio+1;
			Cat[i]['color']=colorVacio;

		}
		totalCantidad=totalCantidad+obj['data'][i]['cantidad'];
		if(Cat[i]['base']===1){
			//asigna el path base
			subCatBase=Cat[i]['path'];
			total_area=subCatBase.area;
			area_categorias[3].area=subCatBase.area;
			indexBase=i;
		}
		var sortIndex;
		sortIndex=Cat[i]['sub']-1;
		Cat[i]['url']=obj['data'][i]['url'];
		area_categorias[3].grupo.addChild(Cat[i]['path']);
		//total_area=total_area+area;


	}
	area_categorias[3].cantidad=totalCantidad;
	//todo: Validar que el cálculo para valores vacios está bien
	//Calcular porcentajes
	num_data=Cat.length;



	var restoPorcVacio=1-cant_vacio*0.1;
	for (i=0; i<num_data; i++){
		var x;
		var porc;
		var area;
		if(cant_vacio===Object.keys(obj['data']).length) {
			porc = 1 / cant_vacio;

		}
		else{
			if(obj['data'][i]['cantidad']===0){
				porc=0.025;
			}
			else{
				porc=(obj['data'][i]['cantidad']/totalCantidad)*restoPorcVacio;
			}

		}
		area=Math.ceil(porc*total_area);

		Cat[i]['porc']=porc;
		Cat[i]['area']=area;
		Cat[i]['text']=Cat[i]['text']+': '+obj['data'][i]['cantidad'];

	}


	var Cant10=0;
	var numCat10=0;
	for (i=0; i<num_data; i++){

		if (Cat[i]['porc']<0.015){
			Cant10=Cant10+(0.015-Cat[i]['porc'])
			Cat[i]['porc']=0.015
			numCat10=numCat10+1;
		}

	}

	numCat10=numSubCats-numCat10;


	for (i=0; i<num_data; i++){

		if (Cat[i]['porc']>0.015){
			if( ( Cat[i]['porc']-(Cant10/numCat10) )<0.015 ){
				Cant10=Cant10-(Cat[i]['porc']-0.015);
				Cat[i]['porc']=0.015;

			}
			else{
				Cat[i]['porc']=Cat[i]['porc']-Cant10/numCat10;
				Cant10=Cant10-Cant10/numCat10;

			}

			numCat10=numCat10-1;
		}

	}

	var result;
	i=30;
	var flagInicio=0;
	var varx=0;
	var porcentResult=0, porcentSubcat=0;
	Cat[indexBase]['path'].strokeWidth=2;
	Cat[indexBase]['path'].fillColor=Cat[indexBase]['color'];
	Cat[indexBase]['path'].strokeColor='white';
	//posición inicial

	var ivarx=0;
	/*for (ivarx=0; ivarx<numSubCats; ivarx++)
	{
		Cat[ivarx]['path'].strokeWidth=2;
		Cat[ivarx]['path'].fillColor=Cat[ivarx]['color'];
		Cat[ivarx]['path'].strokeColor='white';
		console.log(ivarx+':');
		console.log(Cat[ivarx]['path'].position);

	}*/
	var conteo=0;
	while(varx<=numSubCats-1){
		//debugger
		conteo=conteo+1;
		//break;
		  // if(result){
		  // 	result.remove();
		  // }
		switch(varx){
			case 0://5
				if(flagInicio===0){
					i=1;
					flagInicio=1;
				}
				Cat[varx]['path'].position=new paper.Point(507.7409658526851, 568.2455013975491-i);
				break;
			case 1://4
				if(flagInicio===0){
					i=1;
					flagInicio=1;
				}
				Cat[varx]['path'].position=new paper.Point(332.86451985700626+i, Cat[varx]['path'].position._y);
				break;
			case 2://3
				if(flagInicio===0){
					i=1;
					flagInicio=1;
				}
				Cat[varx]['path'].position=new paper.Point(282.91961921963286+i, Cat[varx]['path'].position._y);
				break;
			case 3://2
				if(flagInicio===0){
					i=1;
					flagInicio=1;
				}
				Cat[varx]['path'].position=new paper.Point( 287.314529907021+i , Cat[varx]['path'].position._y);
				break;

		}


		result=inters(Cat,varx, indexBase);

		porcentResult=Math.abs(result.area/total_area);
		porcentSubcat=Math.abs(Cat[varx]['porc']);

		if(varx===2 && conteo>50 ){
			debugger;
		}

		if( ( porcentResult>=(porcentSubcat-0.01) && porcentResult<=(porcentSubcat+0.01) ) ){
			//console.log("Cat[varx]['path'].area"+Cat[varx]['path'].area);
			result.select=true;
			result.fillColor=Cat[varx]['color'];
			result.strokeColor='white';
			result.strokeWidth=2;
			conteo=0;
			Cat[varx]['path']=result;
			//console.log(Cat[varx]['path'].area);
			eventCat(Cat[varx])
			//console.log(Cat[indexBase]['path'].area);
			result=Cat[indexBase]['path'].subtract(Cat[varx]['path']);
			Cat[indexBase]['path']=result;
			//console.log(Cat[indexBase]['path'].area);
			varx=varx+1;
			flagInicio=0;
			if(varx===indexBase){
				eventCat(Cat[varx]);
				varx=varx+1;

			}
			porcentResult=0;
			porcentSubcat=0;
			 //if(varx===4){
			 //	break;}


			//if(varx>2){
			// 	clearInterval(interval);
			// }
		}
		if(porcentResult<(porcentSubcat-0.01)){
			i=i+4;
			//debugger;
		}
		else if(porcentResult>(porcentSubcat+0.01)){
			//debugger;
			i=i-1;
		}

	}
	//todo graficar bien los datos

	//area_categorias[3].textPath=new paper.Path('M539.1,273.4c17.4-1.3,29.7,14.2,38.4,27.2c11.6,17.5,19.8,36.8,28.8,55.6 c9.2,19.2,16.4,39.6,18.4,60.9');
	area_categorias[3].textPath=new paper.Path('M388.366,373.914c9.616-20.086,72.566-40.706,96.384-59.414c13.097-10.287,22.841-48.832,41.538-47.443c18.483,1.389,28.74,19.017,38.997,32.159c12.393,15.919,45.235,73.301,51.966,91.784c6.624,18.483,3.389,20.425-10.5,31.75c-18.376,14.958-12.405,7.024-33.56,4.246c-17.522-2.35-35.043-3.205-52.245,2.244c-15.171,4.808-28.954,12.928-43.163,19.979c-10.684,5.342-28.099,12.393-37.821,1.496C432.803,442.702,378.536,394.427,388.366,373.914z');
	area_categorias[3].text=obj['categoria'];
	area_categorias[3].textPath.parent=area_categorias[3].grupo;
	var punto=new paper.Point(area_categorias[3].grupo.position._x+80,area_categorias[3].grupo.position._y+80);
	area_categorias[3].grupo.scale(0.8, punto);
	//var path = new paper.Path('M539.1,273.4c17.4-1.3,29.7,14.2,38.4,27.2c11.6,17.5,19.8,36.8,28.8,55.6 c9.2,19.2,16.4,39.6,18.4,60.9');
	//pathfullySelected = true;
	//createAlignedText('      '+obj['categoria'], path, {fontSize: 18});



}

function Cat4(obj){
	var porcMinimo=0.1; //10%
	var colorVacio='#cccccc'
	var numSubCats=6;
	var areaBase;
	var orden=[];
	var total_area=0;
	var totalCantidad=0;
	var Cat=[];
	//var SubCatArray=[];
	var cant_vacio;
	var subCatBase;
	var indexBase;


	area_categorias[4]={cantidad:0,area:0, grupo:new paper.Group(), texto:'', textPath:null, scale:null};



	Cat=[
		//0
		{   sub:1,
			area:0,
			path:new paper.Path('M459.4,298.5c23.2,49.8,36.6,107.6,22,160.9c205-46.3,245.2-117.3,196.9-184.7c-34.7-48.1-102.6-69.1-296.3-64.5 C414.4,225.5,441.6,259.7,459.4,298.5C471.8,324.6,441.5,259,459.4,298.5z'),
			color:'#489E43',

			base:0,
			porc:0,
			xinit:151.41199552888756,
			yinit:30,
			dir:90,
			url:'', text:''


		},
		//1
		{   sub:2,
			area:0,
			path:new paper.Path('M461.4,302.7c23.2,49.8,36.6,107.6,22,160.9c205-46.3,245.2-117.3,196.9-184.7c-34.7-48.1-102.6-69.1-296.3-64.5 C416.4,229.7,443.6,263.9,461.4,302.7C473.8,328.8,443.5,263.2,461.4,302.7z'),
			color:'#7B9B46',

			base:0,
			porc:0,
			xinit:151.41199552888756,
			yinit:30,
			dir:90,
			url:'', text:''

		},
		//2
		{
			sub:5,
			area:0,
			path:new paper.Path('M59.8,241.9C2.9,267.1-9.4,342.8,11.9,380.2c11.1,20,63.3,73.4,88.7,75.7c31.1,2.9,77.9-36.2,99.5-56.2 c44.1-40.3,83.2-112.3,59.9-168.6C196.3,232.9,120.7,215,59.8,241.9z'),
			color:'#6A946C',

			base:0,
			porc:0,
			xinit:0,
			yinit:0,
			dir:0,
			url:'', text:''
		},
		//3
		{
			sub:4,
			area:0,
			path:new paper.Path('M61,241.9C4.1,267.1-8.2,342.8,13.1,380.2c11.1,20,63.3,73.4,88.7,75.7c31.1,2.9,77.9-36.2,99.5-56.2 c44.1-40.3,83.2-112.3,59.9-168.6C197.6,232.9,121.9,215,61,241.9z'),
			color:'#99CA5D',

			base:0,
			porc:0,
			xinit:0,
			yinit:0,
			dir:0,
			url:'', text:''
		},
		//4

		{
			sub:6,
			area:0,
			path:new paper.Path('M347.5,662.2c56.4,26.4,85.2-37.5,99.4-78.1c7.8-21.6,50-72.6,35-93.2c-18.3-25.3-78.5-34.9-107.7-38.1 c-59.4-6.8-139.3,11.1-166.5,65.7C250.9,565.3,287.1,634,347.5,662.2z'),
			color:'#75D081',

			base:0,
			porc:0,
			xinit:0,
			yinit:0,
			dir:0,
			url:'', text:''
		},
		//5
		{
			sub:3,
			area:0,
			path:new paper.Path('M452.8,289.4c-9.9-13.6-31-12-46-12.5 c-21.9-0.8-43.5,4.1-65.3,3.4c-19.8-0.6-41.8-4.3-60,5.7c-15.3,8.4-22.4,25-24.7,41.6c-1.4,10.5-1.7,21.2-2.3,31.9 c-0.6,10.5-0.3,21,3.9,30.7c7.1,16.7,22.2,28.5,36.3,39.2c14.5,11,32,22.6,51,22c18.4-0.6,24.6-19.1,28.9-34 c3-10.2,5.4-20.6,6.9-31.2c1.2-9.2,0.8-18.8,3.3-27.8c4.3-15.4,19-21.5,32.5-27C431.9,325.5,469.7,310.7,452.8,289.4z'),
			color:'#A9F5B7',

			base:1,
			porc:0,
			xinit:0,
			yinit:0,
			dir:0,
			url:'', text:''
		}





	]


	var num_data=Cat.length;
	var total=0;
	var i;

	cant_vacio=0;
	totalCantidad=0
	//todo reconocer valores no definidos en el objeto ingresado por json, para lo cual se agrega como 0
	//obtiene el area total
	for (i=0; i<num_data; i++){
		Cat[i]['text']=obj['data'][i]['nombre'];
		if(obj['data'][i]['cantidad']===0){
			cant_vacio=cant_vacio+1;
			Cat[i]['color']=colorVacio;

		}
		totalCantidad=totalCantidad+obj['data'][i]['cantidad'];
		if(Cat[i]['base']===1){
			//asigna el path base
			subCatBase=Cat[i]['path'];
			total_area=subCatBase.area;
			area_categorias[4].area=subCatBase.area;
			indexBase=i;
		}
		var sortIndex;
		sortIndex=Cat[i]['sub']-1;
		Cat[i]['url']=obj['data'][i]['url'];
		area_categorias[4].grupo.addChild(Cat[i]['path']);
		//total_area=total_area+area;


	}
	area_categorias[4].cantidad=totalCantidad;
	//todo: Validar que el cálculo para valores vacios está bien
	//Calcular porcentajes
	num_data=Cat.length;



	var restoPorcVacio=1-cant_vacio*0.1;
	for (i=0; i<num_data; i++){
		var x;
		var porc;
		var area;
		if(cant_vacio===Object.keys(obj['data']).length) {
			porc = 1 / cant_vacio;

		}
		else{
			if(obj['data'][i]['cantidad']===0){
				porc=0.025;
			}
			else{
				porc=(obj['data'][i]['cantidad']/totalCantidad)*restoPorcVacio;
			}

		}
		area=Math.ceil(porc*total_area);

		Cat[i]['porc']=porc;
		Cat[i]['area']=area;
		Cat[i]['text']=Cat[i]['text']+': '+obj['data'][i]['cantidad'];

	}

	var Cant10=0;
	var numCat10=0;
	for (i=0; i<num_data; i++){

		if (Cat[i]['porc']<0.015){
			Cant10=Cant10+(0.015-Cat[i]['porc'])
			Cat[i]['porc']=0.015
			numCat10=numCat10+1;
		}

	}

	numCat10=numSubCats-numCat10;


	for (i=0; i<num_data; i++){

		if (Cat[i]['porc']>0.015){
			if( ( Cat[i]['porc']-(Cant10/numCat10) )<0.015 ){
				Cant10=Cant10-(Cat[i]['porc']-0.015);
				Cat[i]['porc']=0.015;

			}
			else{
				Cat[i]['porc']=Cat[i]['porc']-Cant10/numCat10;
				Cant10=Cant10-Cant10/numCat10;

			}

			numCat10=numCat10-1;
		}

	}


	var result;
	i=30;
	var flagInicio=0;
	var varx=0;
	var porcentResult=0, porcentSubcat=0;
	Cat[indexBase]['path'].strokeWidth=2;
	Cat[indexBase]['path'].fillColor=Cat[indexBase]['color'];
	Cat[indexBase]['path'].strokeColor='white';
	//posición inicial



	while(varx<=numSubCats-1){
		// break;
		//  if(result){
		//  	result.remove();
		//  }
		switch(varx){
			case 0://1
				if(flagInicio===0){
					i=1;
					flagInicio=1;
				}
				Cat[varx]['path'].position=new paper.Point(539.3713335169481-i, 334.49058083355953);
				break;
			case 1://2
				if(flagInicio===0){
					i=1;
					flagInicio=1;
				}
				Cat[varx]['path'].position=new paper.Point(541.3713335169481-i, 338.6905808335596);
				break;
			case 2://5
				if(flagInicio===0){
					i=1;
					flagInicio=1;
				}
				Cat[varx]['path'].position=new paper.Point(134.5278573232214+i, 341.4851013749677);
				break;
			case 3://4
				if(flagInicio===0){
					i=1;
					flagInicio=1;
				}
				Cat[varx]['path'].position=new paper.Point( 135.72785732322137+i , 341.4851013749677);
				break;
			case 4://6
				if(flagInicio===0){
					i=1;
					flagInicio=1;
				}
				Cat[varx]['path'].position=new paper.Point( 346.3991595015931 , 559.9149062673655-i);
				break;

		}


		result=inters(Cat,varx, indexBase);

		porcentResult=Math.abs(result.area/total_area);
		porcentSubcat=Math.abs(Cat[varx]['porc']);

		if( porcentResult>=(porcentSubcat-0.01) && porcentResult<=(porcentSubcat+0.01) ){
			//console.log("Cat[varx]['path'].area"+Cat[varx]['path'].area);
			result.select=true;
			result.fillColor=Cat[varx]['color'];
			result.strokeColor='white';
			result.strokeWidth=2;

			Cat[varx]['path']=result;
			//console.log(Cat[varx]['path'].area);
			eventCat(Cat[varx])
			//console.log(Cat[indexBase]['path'].area);
			result=Cat[indexBase]['path'].subtract(Cat[varx]['path']);
			Cat[indexBase]['path']=result;
			//console.log(Cat[indexBase]['path'].area);
			varx=varx+1;
			flagInicio=0;
			if(varx===indexBase){
				eventCat(Cat[varx]);
				varx=varx+1;

			}
			porcentResult=0;
			porcentSubcat=0;
			if(varx===5){
				break;}



		}
		if(porcentResult<(porcentSubcat-0.01)){
			i=i+4;
			//debugger;
		}
		else if(porcentResult>(porcentSubcat+0.01)){
			//debugger;
			i=i-2;
		}

	}
	//todo graficar bien los datos


	area_categorias[4].textPath=new paper.Path('M256.8,321c2.3-16.6,9.5-33.2,24.7-41.6c18.2-10,40.2-6.3,60-5.7 c21.9,0.7,43.5-4.1,65.3-3.4');
	//area_categorias[4].textPath.position=new paper.Point(area_categorias[4].textPath.position._x+20,area_categorias[4].textPath.position.y-50);
	area_categorias[4].text=obj['categoria'];
	area_categorias[4].textPath.parent=area_categorias[4].grupo;
	var punto=new paper.Point(area_categorias[4].grupo.position._x-40,area_categorias[4].grupo.position._y+30);
	area_categorias[4].grupo.scale(0.8, punto);

	// var path = new paper.Path('M256.8,321c2.3-16.6,9.5-33.2,24.7-41.6c18.2-10,40.2-6.3,60-5.7 c21.9,0.7,43.5-4.1,65.3-3.4');
	// pathfullySelected = true;
	// createAlignedText('      '+obj['categoria'], path, {fontSize: 18});



}


function Cat5(obj){
	var porcMinimo=0.1; //10%
	var colorVacio='#cccccc'
	var numSubCats=4;
	var areaBase;
	var orden=[];
	var total_area=0;
	var totalCantidad=0;
	var Cat=[];
	//var SubCatArray=[];
	var cant_vacio;
	var subCatBase;
	var indexBase;


	area_categorias[5]={cantidad:0,area:0, grupo:new paper.Group(), texto:'', textPath:null, scale:null};



	Cat=[
		//0
		{   sub:1,
			area:0,
			path:new paper.Path('M219.9,515.7c16-13,32.1-16.1,52.4-16.7c19.1-0.5,38.2-6,54.7-15.5c16.1-9.3,6.6-72.7,20.8-85.5C494,266.6,388.7,77.8,271.7,254.6 c-10.2,15.4-30.6,9.5-36,26.2c-16.2,15-27.3,19.8-44.5,28.5c-16.8,8.4-9.5,91-24.8,101.6c-16.4,11.3,30.2,108.8,41,119.1 C207.8,525.8,216.5,518.4,219.9,515.7C225.1,511.4,217,518.1,219.9,515.7z'),
			color:'#E4C656',

			base:0,
			porc:0,
			xinit:151.41199552888756,
			yinit:30,
			dir:90,
			url:'', text:''


		},
		//1
		{   sub:3,
			area:0,
			path:new paper.Path('M411.2,838c13.5-5.9,60.3-38.7,69.1-51.5c8.9-13-58.2-157-71.8-172c-32.3-35.7-146.2,162-205,189.3 C237.2,963.2,393.7,845.7,411.2,838z'),
			color:'#E2E452',

			base:0,
			porc:0,
			xinit:151.41199552888756,
			yinit:30,
			dir:90,
			url:'', text:''

		},
		//2
		{
			sub:4,
			area:0,
			path:new paper.Path('M458.6,577.2c38.7,14.5,47.8,45.7,140.6,74.4c465-85.3-67-285.9-181.4-181.8c-5.5,5-11.1,52.7-10.2,59.6C408.5,537.3,399.2,554.9,458.6,577.2z'),
			color:'#B59A25',

			base:0,
			porc:0,
			xinit:0,
			yinit:0,
			dir:0,
			url:'', text:''
		},
		//3
		{
			sub:2,
			area:0,
			path:new paper.Path('M409.5,552.8c-7.7-28.6-22.5-69.4-57.2-72.9c-42.6-4.3-47.8,46.5-74,67.6 c-11.9,9.6-25.9,14.5-40,19.8c-11,4.1-26.6,10.4-28.5,23.8c-2.2,16.1,13.1,30.3,24.5,39.1c15.6,12,33.1,17.9,52.1,10.2 c14.9-6,27-17,43.2-20c16.9-3.1,34.8,2.4,51.2-4.5C404.1,606.3,415.7,576.3,409.5,552.8z'),
			color:'#DED3A6',

			base:1,
			porc:0,
			xinit:0,
			yinit:0,
			dir:0,
			url:'', text:''
		}



	]


	var num_data=Cat.length;
	var total=0;
	var i;

	cant_vacio=0;
	totalCantidad=0
	//todo reconocer valores no definidos en el objeto ingresado por json, para lo cual se agrega como 0
	//obtiene el area total
	for (i=0; i<num_data; i++){
		Cat[i]['text']=obj['data'][i]['nombre'];
		if(obj['data'][i]['cantidad']===0){
			cant_vacio=cant_vacio+1;
			Cat[i]['color']=colorVacio;

		}
		totalCantidad=totalCantidad+obj['data'][i]['cantidad'];
		if(Cat[i]['base']===1){
			//asigna el path base
			subCatBase=Cat[i]['path'];
			total_area=subCatBase.area;
			area_categorias[5].area=subCatBase.area;
			indexBase=i;
		}
		var sortIndex;
		sortIndex=Cat[i]['sub']-1;
		Cat[i]['url']=obj['data'][i]['url'];
		area_categorias[5].grupo.addChild(Cat[i]['path']);
		//total_area=total_area+area;


	}
	area_categorias[5].cantidad=totalCantidad;
	//todo: Validar que el cálculo para valores vacios está bien
	//Calcular porcentajes
	num_data=Cat.length;



	var restoPorcVacio=1-cant_vacio*0.1;
	for (i=0; i<num_data; i++){
		var x;
		var porc;
		var area;
		if(cant_vacio===Object.keys(obj['data']).length) {
			porc = 1 / cant_vacio;

		}
		else{
			if(obj['data'][i]['cantidad']===0){
				porc=0.025;
			}
			else{
				porc=(obj['data'][i]['cantidad']/totalCantidad)*restoPorcVacio;
			}

		}
		area=Math.ceil(porc*total_area);

		Cat[i]['porc']=porc;
		Cat[i]['area']=area;
		Cat[i]['text']=Cat[i]['text']+': '+obj['data'][i]['cantidad'];

	}


	var Cant10=0;
	var numCat10=0;
	for (i=0; i<num_data; i++){

		if (Cat[i]['porc']<0.015){
			Cant10=Cant10+(0.015-Cat[i]['porc'])
			Cat[i]['porc']=0.015
			numCat10=numCat10+1;
		}

	}

	numCat10=numSubCats-numCat10;


	for (i=0; i<num_data; i++){

		if (Cat[i]['porc']>0.015){
			if( ( Cat[i]['porc']-(Cant10/numCat10) )<0.015 ){
				Cant10=Cant10-(Cat[i]['porc']-0.015);
				Cat[i]['porc']=0.015;

			}
			else{
				Cat[i]['porc']=Cat[i]['porc']-Cant10/numCat10;
				Cant10=Cant10-Cant10/numCat10;

			}

			numCat10=numCat10-1;
		}

	}

	var result;
	i=30;
	var flagInicio=0;
	var varx=0;
	var porcentResult=0, porcentSubcat=0;
	Cat[indexBase]['path'].strokeWidth=2;
	Cat[indexBase]['path'].fillColor=Cat[indexBase]['color'];
	Cat[indexBase]['path'].strokeColor='white';
	//posición inicial



	while(varx<=numSubCats-1){
		//break;
		//  if(result){
		//  	result.remove();
		//  }
		switch(varx){
			case 0://1
				if(flagInicio===0){
					i=1;
					flagInicio=1;
				}
				Cat[varx]['path'].position=new paper.Point(291.05201997991486, 356.3540020803689+i);
				break;
			case 1://3
				if(flagInicio===0){
					i=1;
					flagInicio=1;
				}
				Cat[varx]['path'].position=new paper.Point(342.3049195252445, 748.5602042755283-i);
				break;
			case 2://4
				if(flagInicio===0){
					i=1;
					flagInicio=1;
				}
				Cat[varx]['path'].position=new paper.Point(597.1268054363372-i, 546.0703704378207);
				break;

		}

		//grafica Subcat1

		//debugger;
		result=inters(Cat,varx, indexBase);

		porcentResult=Math.abs(result.area/total_area);
		porcentSubcat=Math.abs(Cat[varx]['porc']);

		if( porcentResult>=(porcentSubcat-0.01) && porcentResult<=(porcentSubcat+0.01) ){
			//console.log("Cat[varx]['path'].area"+Cat[varx]['path'].area);
			result.select=true;
			result.fillColor=Cat[varx]['color'];
			result.strokeColor='white';
			result.strokeWidth=2;

			Cat[varx]['path']=result;
			//console.log(Cat[varx]['path'].area);
			eventCat(Cat[varx])
			//console.log(Cat[indexBase]['path'].area);
			result=Cat[indexBase]['path'].subtract(Cat[varx]['path']);
			Cat[indexBase]['path']=result;
			//console.log(Cat[indexBase]['path'].area);
			varx=varx+1;
			flagInicio=0;
			if(varx===indexBase){
				eventCat(Cat[varx]);
				varx=varx+1;

			}
			porcentResult=0;
			porcentSubcat=0;

		}
		if(porcentResult<(porcentSubcat-0.01)){
			i=i+4;
			//debugger;
		}
		else if(porcentResult>(porcentSubcat+0.01)){
			//debugger;
			i=i-2;
		}

	}
	//todo graficar bien los datos


	//area_categorias[5].textPath=new paper.Path('M217.1,574.5c-11,4.1-19.3,15.9-21.1,29.3c-2.2,16.1,19.1,35.3,30.6,44.2 c10.3,8,71.3,35.4,90.4,27.6');
	area_categorias[5].textPath=new paper.Path('M177.382,488.316c-101.858,101.143,44.465,199.609,61.02,212.388c22.654,17.426,80.731,20.169,108.322,8.987');

	area_categorias[5].text=obj['categoria'];
	area_categorias[5].textPath.parent=area_categorias[5].grupo;
	var punto=new paper.Point(area_categorias[5].grupo.position._x-280,area_categorias[5].grupo.position._y+120);
	area_categorias[5].grupo.scale(0.8, punto);

	// var path = new paper.Path('M217.1,574.5c-11,4.1-19.3,15.9-21.1,29.3c-2.2,16.1,19.1,35.3,30.6,44.2 c10.3,8,71.3,35.4,90.4,27.6');
	// pathfullySelected = true;
	// createAlignedText('      '+obj['categoria'], path, {fontSize: 18});




}

function Cat6(obj){
	var porcMinimo=0.1; //10%
	var colorVacio='#cccccc'
	var numSubCats=6;
	var areaBase;
	var orden=[];
	var total_area=0;
	var totalCantidad=0;
	var Cat=[];
	//var SubCatArray=[];
	var cant_vacio;
	var subCatBase;
	var indexBase;


	area_categorias[6]={cantidad:0,area:0, grupo:new paper.Group(), texto:'', textPath:null, scale:null};



	Cat=[
		//0
		{   sub:1,
			area:0,
			path:new paper.Path('M633.9,293.4c8,6.8,61.8,47.2,71.3,28.1c8.8-33.2,2.6-308.3-9.3-324.5c-21.3-28.8-69,31.2-91.5,49.3c-52,36.3-25.3,159.4-13.6,187.2 C601.8,258.8,617.1,278.8,633.9,293.4C646.1,303.4,610,273.3,633.9,293.4z'),
			color:'#32497D',

			base:0,
			porc:0,
			xinit:151.41199552888756,
			yinit:30,
			dir:90,
			url:'', text:''


		},
		//1
		{   sub:3,
			area:0,
			path:new paper.Path('M704.2,310c9.1-8.3,70.4-94.4,76.9-105.2c8.8-14.9,6.4-145.7-5.9-160.6c-12.6-14.5-93.4-71.5-101.3-71.2c-16,0.2-78,81.2-95.1,86.8 c-85.7,19-36.9,143,42.1,180c5.4,2.5,17.7,48,25.6,68C664.8,329.5,687.1,325.5,704.2,310C709.8,305.1,692.2,320.8,704.2,310z'),
			color:'#4D86A1',

			base:0,
			porc:0,
			xinit:151.41199552888756,
			yinit:30,
			dir:90,
			url:'', text:''

		},
		//2
		{
			sub:4,
			area:0,
			path:new paper.Path('M719.7,463c16.901,22.102,2.393,75.867,11.5,103.737c9.94,30.42,21.545,18.595,48.8,13.525 c65.731-12.228,66.927-117.708,77.993-172.73c3.844-19.112,21.951-69.586,10.454-87.857c-15.23-24.205-70.453-13.108-90.298-7.492 C722.037,328.062,680.307,411.117,719.7,463C727.5,473.2,711.5,452.2,719.7,463z'),
			color:'#77CCE0',

			base:0,
			porc:0,
			xinit:0,
			yinit:0,
			dir:0,
			url:'', text:''
		},
		//3
		{
			sub:6,
			area:0,
			path:new paper.Path('M637.5,605.4c-23.6-15.9-80-70.3-59.5-135.5c11.3-35.7,3.2-150.1-31.6-167.1c-4.3,13.5-42.1,72-47.4,124.8 c-1,9.7-25.1,38.7-22.8,47.6c2.2,8.8-28.4,29.8-28.7,38.7c-1.6,30.8,14.9,41.1,5.2,70.4c-4.5,13.5,30.7,34.1,42.1,45.6 c12.2,12.2,42.9,22.9,59.9,15.2C572.9,643,628.1,615.9,637.5,605.4z'),
			color:'#64A9B9',

			base:0,
			porc:0,
			xinit:0,
			yinit:0,
			dir:0,
			url:'', text:''
		},
		//4
		{
			sub:5,
			area:0,
			path:new paper.Path('M615.5,583.4c-23.6-15.9-80-70.3-59.5-135.5c11.3-35.7,3.2-150.1-31.6-167.1c-4.3,13.5-42.1,72-47.4,124.8 c-1,9.7-25.1,38.7-22.8,47.6c2.2,8.8-28.4,29.8-28.7,38.7c-1.6,30.8,14.9,41.1,5.2,70.4c-4.5,13.5,30.7,34.1,42.1,45.6c12.2,12.2,42.9,22.9,59.9,15.2 C550.9,621,606.1,593.9,615.5,583.4z'),
			color:'#28ADC0',

			base:0,
			porc:0,
			xinit:0,
			yinit:0,
			dir:0,
			url:'', text:''
		},
		//5
		{
			sub:2,
			area:0,
			path:new paper.Path('M700.65,358.517c0.523-60.843-69.893-25.02-47.269,32.969 c21.234,54.423-38.584,69.431-61.277,106.9C560.913,549.886,623.528,607.551,668,560.3 C712.569,502.889,700.068,426.226,700.65,358.517z'),
			color:'#97B8C6',

			base:1,
			porc:0,
			xinit:0,
			yinit:0,
			dir:0,
			url:'', text:''
		}



	]


	var num_data=Cat.length;
	var total=0;
	var i;

	cant_vacio=0;
	totalCantidad=0
	//todo reconocer valores no definidos en el objeto ingresado por json, para lo cual se agrega como 0
	//obtiene el area total
	for (i=0; i<num_data; i++){
		Cat[i]['text']=obj['data'][i]['nombre'];
		if(obj['data'][i]['cantidad']===0){
			cant_vacio=cant_vacio+1;
			Cat[i]['color']=colorVacio;

		}
		totalCantidad=totalCantidad+obj['data'][i]['cantidad'];
		if(Cat[i]['base']===1){
			//asigna el path base
			subCatBase=Cat[i]['path'];
			total_area=subCatBase.area;
			area_categorias[6].area=subCatBase.area;
			indexBase=i;
		}
		var sortIndex;
		sortIndex=Cat[i]['sub']-1;
		Cat[i]['url']=obj['data'][i]['url'];
		area_categorias[6].grupo.addChild(Cat[i]['path']);
		//total_area=total_area+area;


	}
	area_categorias[6].cantidad=totalCantidad;
	//todo: Validar que el cálculo para valores vacios está bien
	//Calcular porcentajes
	num_data=Cat.length;



	var restoPorcVacio=1-cant_vacio*0.1;
	for (i=0; i<num_data; i++){
		var x;
		var porc;
		var area;
		if(cant_vacio===Object.keys(obj['data']).length) {
			porc = 1 / cant_vacio;

		}
		else{
			if(obj['data'][i]['cantidad']===0){
				porc=0.025;
			}
			else{
				porc=(obj['data'][i]['cantidad']/totalCantidad)*restoPorcVacio;
			}

		}
		area=Math.ceil(porc*total_area);

		Cat[i]['porc']=porc;
		Cat[i]['area']=area;
		Cat[i]['text']=Cat[i]['text']+': '+obj['data'][i]['cantidad'];



	}

	//var restoPorcVacio=1-cant_vacio*0.1;
	var Cant10=0;
	var numCat10=0;
	for (i=0; i<num_data; i++){

		if (Cat[i]['porc']<0.015){
			Cant10=Cant10+(0.015-Cat[i]['porc'])
			Cat[i]['porc']=0.015
			numCat10=numCat10+1;
		}

	}

	numCat10=numSubCats-numCat10;


	for (i=0; i<num_data; i++){

		if (Cat[i]['porc']>0.015){
			if( ( Cat[i]['porc']-(Cant10/numCat10) )<0.015 ){
				Cant10=Cant10-(Cat[i]['porc']-0.015);
				Cat[i]['porc']=0.015;

			}
			else{
				Cat[i]['porc']=Cat[i]['porc']-Cant10/numCat10;
				Cant10=Cant10-Cant10/numCat10;

			}

			numCat10=numCat10-1;
		}

	}




	var result;
	i=30;
	var flagInicio=0;
	var varx=0;
	var porcentResult=0, porcentSubcat=0;
	Cat[indexBase]['path'].strokeWidth=2;
	Cat[indexBase]['path'].fillColor=Cat[indexBase]['color'];
	Cat[indexBase]['path'].strokeColor='white';
	//posición inicial



	while(varx<=numSubCats-1){
		//break;
		//  if(result){
		//  	result.remove();
		//  }
		switch(varx){
			case 0://1
				if(flagInicio===0){
					i=1;
					flagInicio=1;
				}
				Cat[varx]['path'].position=new paper.Point(641.4088007935298, 157.94304654202557+i);
				break;
			case 1://3
				if(flagInicio===0){
					i=1;
					flagInicio=1;
				}
				Cat[varx]['path'].position=new paper.Point(659.9974141947547, 147.92848027602892+i);
				break;
			case 2://4
				if(flagInicio===0){
					i=1;
					flagInicio=1;
				}
				Cat[varx]['path'].position=new paper.Point(788.4382344648609-i, 450.8831736851075);
				break;
			case 3://6
				if(flagInicio===0){
					i=1;
					flagInicio=1;
				}
				Cat[varx]['path'].position=new paper.Point(542.4448743769833+i, 475.2497086569522);
				break;
			case 4://5
				if(flagInicio===0){
					i=1;
					flagInicio=1;
				}
				Cat[varx]['path'].position=new paper.Point(520.4448743769833+i, 453.2497086569522);
				break;


		}

		//grafica Subcat1

		//debugger;
		result=inters(Cat,varx, indexBase);

		porcentResult=Math.abs(result.area/total_area);
		porcentSubcat=Math.abs(Cat[varx]['porc']);

		if( porcentResult>=(porcentSubcat-0.01) && porcentResult<=(porcentSubcat+0.01) ){
			//console.log("Cat[varx]['path'].area"+Cat[varx]['path'].area);
			result.select=true;
			result.fillColor=Cat[varx]['color'];
			result.strokeColor='white';
			result.strokeWidth=2;

			Cat[varx]['path']=result;
			//console.log(Cat[varx]['path'].area);
			eventCat(Cat[varx])
			//console.log(Cat[indexBase]['path'].area);
			result=Cat[indexBase]['path'].subtract(Cat[varx]['path']);
			Cat[indexBase]['path']=result;
			//console.log(Cat[indexBase]['path'].area);
			varx=varx+1;
			flagInicio=0;
			if(varx===indexBase){
				eventCat(Cat[varx]);
				varx=varx+1;

			}
			porcentResult=0;
			porcentSubcat=0;
			/*if(varx===3){
			 break;}*/


			//if(varx>2){
			// 	clearInterval(interval);
			// }
		}
		if(porcentResult<(porcentSubcat-0.01)){
			i=i+4;
			//debugger;
		}
		else if(porcentResult>(porcentSubcat+0.01)){
			//debugger;
			i=i-2
		}

	}
	//todo graficar bien los datos



	area_categorias[6].textPath=new paper.Path('M706,367.7c2.7,23.7,5.2,47.5,4.6,71.3c-0.6,23.1-5.8,45.6-12.8,67.5c-7.7,24.3-16,59.2-43,69.1');
	area_categorias[6].text=obj['categoria'];
	area_categorias[6].textPath.parent=area_categorias[6].grupo;
	var punto=new paper.Point(area_categorias[6].grupo.position._x-180,area_categorias[6].grupo.position._y+180);
	area_categorias[6].grupo.scale(0.8, punto);
	// var path = new paper.Path('M706,367.7c2.7,23.7,5.2,47.5,4.6,71.3c-0.6,23.1-5.8,45.6-12.8,67.5c-7.7,24.3-16,59.2-43,69.1');
	// pathfullySelected = true;
	// createAlignedText('      '+obj['categoria'], path, {fontSize: 18});




}

function Cat7(obj){
	var porcMinimo=0.1; //10%
	var colorVacio='#cccccc'
	var numSubCats=5;
	var areaBase;
	var orden=[];
	var total_area=0;
	var totalCantidad=0;
	var Cat=[];
	//var SubCatArray=[];
	var cant_vacio;
	var subCatBase;
	var indexBase;

	/*	orden[0]={sub:0, base:0};
	 orden[1]={sub:1, base:0};
	 orden[2]={sub:2, base:1};*/

	area_categorias[7]={cantidad:0,area:0, grupo:new paper.Group(), texto:'', textPath:null, scale:null};


	Cat=[
		//0
		{   sub:1,
			area:0,
			path:new paper.Path('M354.042,647.01c4.794-69.536-93.216-86.613-140.06-83.247c-71.546,5.142-130.023,14.739-142.519,94.799 c-11.619,74.44,114.139,52.291,154.963,50.724c34.105-1.309,77.11,13.183,108.974,2.514 C359.732,703.451,352.689,666.625,354.042,647.01z'),
			color:'#AEA095',

			base:0,
			porc:0,
			xinit:151.41199552888756,
			yinit:30,
			dir:90,
			url:'', text:''


		},
		//1
		{   sub:4,
			area:0,
			path:new paper.Path('M529.8,526.8c70.9-72,62-59.9,61.6-92.7c13.7-172.4-108.5-55.8-145.3-56.5c-117.3-41.4-94.9,108.5-100.8,116.7 c-4.2,5.6,59.1,17.8,65.3,21.1c6.1,3.6-7.4,19.2,43,44.6C505.8,586.3,518.7,548.6,529.8,526.8z'),
			color:'#C68684',

			base:0,
			porc:0,
			xinit:151.41199552888756,
			yinit:30,
			dir:90,
			url:'', text:''

		},
		//2
		{
			sub:5,
			area:0,
			path:new paper.Path('M617,674c28,31.9,61.9,44.7,102.1,44.5c181.7-25.9,191.6-148.3,125-170.8c-13-4.5-213.5,10.3-226.7,8.8c-15.6-1.6-34.5-9.5-34.9,9.9 C580.6,607,590.2,643.3,617,674C629.2,687.8,604.8,659.8,617,674z'),
			color:'#7C5A72',

			base:0,
			porc:0,
			xinit:0,
			yinit:0,
			dir:0,
			url:'', text:''
		},
		//3
		{
			sub:3,
			area:0,
			path:new paper.Path('M533,731.6c-15.3-2.2-27.9-12-41.8-17.8c-15.8-6.5-34.1-9.5-50.6-3.5c-36.2,9.9-8.6,67.6-120,180.3c17.9,37.5,79.3,41.8,111.4,29 c13-5.1,169.6-70.8,182.1-76.9c14.7-7.3-31.7-109.1-22.1-123.5C583.4,723.2,541.7,732.8,533,731.6 C517.7,729.4,538.8,732.4,533,731.6z'),
			color:'#91566A',

			base:0,
			porc:0,
			xinit:0,
			yinit:0,
			dir:0,
			url:'', text:''
		},
		//4
		{
			sub:2,
			area:0,
			path:new paper.Path('M582.9,627.4c0.7-1.6-1.2-23-1.5-25.4 c-1.9-16.5-13.5-28.7-29.1-33.3c-33.4-10-72.4-0.5-97.7,23.5c-14.4,13.7-23.9,31.8-39,44.7c-7.1,6.1-15.4,10.7-24.6,12.3 c-9.4,1.7-20.1,1-28.2,6.8c-12.1,8.7-9.9,27-1,37.2c11,12.5,29.6,13.4,45,13.8c35.9,0.9,76.5-1.9,109.5-17.1 c7.6-3.5,14.8-7.7,22.3-11.4c8.1-4.1,16.6-7.4,24.3-12.2C576.4,657.8,582.9,643,582.9,627.4'),
			color:'#C59E8F',

			base:1,
			porc:0,
			xinit:0,
			yinit:0,
			dir:0,
			url:'', text:''
		}



	]


	var num_data=Cat.length;
	var total=0;
	var i;

	cant_vacio=0;
	totalCantidad=0
	//todo reconocer valores no definidos en el objeto ingresado por json, para lo cual se agrega como 0
	//obtiene el area total
	for (i=0; i<num_data; i++){
		Cat[i]['text']=obj['data'][i]['nombre'];
		if(obj['data'][i]['cantidad']===0){
			cant_vacio=cant_vacio+1;
			Cat[i]['color']=colorVacio;

		}
		totalCantidad=totalCantidad+obj['data'][i]['cantidad'];
		if(Cat[i]['base']===1){
			//asigna el path base
			subCatBase=Cat[i]['path'];
			total_area=subCatBase.area;
			area_categorias[7].area=subCatBase.area;
			indexBase=i;
		}
		var sortIndex;
		sortIndex=Cat[i]['sub']-1;
		Cat[i]['url']=obj['data'][i]['url'];
		area_categorias[7].grupo.addChild(Cat[i]['path']);
		//total_area=total_area+area;


	}
	area_categorias[7].cantidad=totalCantidad;
	//todo: Validar que el cálculo para valores vacios está bien
	//Calcular porcentajes
	num_data=Cat.length;



	var restoPorcVacio=1-cant_vacio*0.1;
	for (i=0; i<num_data; i++){
		var x;
		var porc;
		var area;
		if(cant_vacio===Object.keys(obj['data']).length) {
			porc = 1 / cant_vacio;

		}
		else{
			if(obj['data'][i]['cantidad']===0){
				porc=0.025;
			}
			else{
				porc=(obj['data'][i]['cantidad']/totalCantidad)*restoPorcVacio;
			}

		}
		area=Math.ceil(porc*total_area);

		Cat[i]['porc']=porc;
		Cat[i]['area']=area;
		Cat[i]['text']=Cat[i]['text']+': '+obj['data'][i]['cantidad'];

	}




	var result;
	i=30;
	var flagInicio=0;
	var varx=0;
	var porcentResult=0, porcentSubcat=0;
	Cat[indexBase]['path'].strokeWidth=2;
	Cat[indexBase]['path'].fillColor=Cat[indexBase]['color'];
	Cat[indexBase]['path'].strokeColor='white';
	//posición inicial

/*			var ivarx=0;
	 for (ivarx=0; ivarx<numSubCats; ivarx++)
	 {
	 Cat[ivarx]['path'].strokeWidth=2;
	 Cat[ivarx]['path'].fillColor=Cat[ivarx]['color'];
	 Cat[ivarx]['path'].strokeColor='white';
	 console.log(ivarx+':');
	 console.log(Cat[ivarx]['path'].position);

	 }*/

	while(varx<=numSubCats-1){
		// break;
		//  if(result){
		//  	result.remove();
		//  }
		switch(varx){
			case 0://1
				if(flagInicio===0){
					i=1;
					flagInicio=1;
				}
				Cat[varx]['path'].position=new paper.Point(212.52841866085447+i, 638.4544181948404);
				break;
			case 1://4
				if(flagInicio===0){
					i=1;
					flagInicio=1;
				}
				Cat[varx]['path'].position=new paper.Point(468.7830679911315, 454.3418863854978+i);
				break;
			case 2://5
				if(flagInicio===0){
					i=1;
					flagInicio=1;
				}
				Cat[varx]['path'].position=new paper.Point(732.0554223130042-i, 632.6736216462702);
				break;
			case 3://3
				if(flagInicio===0){
					i=1;
					flagInicio=1;
				}
				Cat[varx]['path'].position=new paper.Point(468.80142788760986, 816.5827696735925-i);
				break;
			/*case 4://5
				if(flagInicio===0){
					i=1;
					flagInicio=1;
				}
				Cat[varx]['path'].position=new paper.Point(520.4448743769833+i, 453.2497086569522);
				break;
*/

		}

		//grafica Subcat1

		//debugger;
		result=inters(Cat,varx, indexBase);

		porcentResult=Math.abs(result.area/total_area);
		porcentSubcat=Math.abs(Cat[varx]['porc']);

		if( porcentResult>=(porcentSubcat-0.01) && porcentResult<=(porcentSubcat+0.01) ){
			//console.log("Cat[varx]['path'].area"+Cat[varx]['path'].area);
			result.select=true;
			result.fillColor=Cat[varx]['color'];
			result.strokeColor='white';
			result.strokeWidth=2;

			Cat[varx]['path']=result;
			//console.log(Cat[varx]['path'].area);
			eventCat(Cat[varx])
			//console.log(Cat[indexBase]['path'].area);
			result=Cat[indexBase]['path'].subtract(Cat[varx]['path']);
			Cat[indexBase]['path']=result;
			//console.log(Cat[indexBase]['path'].area);
			varx=varx+1;
			flagInicio=0;
			if(varx===indexBase){
				eventCat(Cat[varx]);
				varx=varx+1;

			}
			porcentResult=0;
			porcentSubcat=0;
			/*if(varx===3){
			 break;}*/


			//if(varx>2){
			// 	clearInterval(interval);
			// }
		}
		if(porcentResult<(porcentSubcat-0.01)){
			i=i+4;
			//debugger;
		}
		else if(porcentResult>(porcentSubcat+0.01)){
			//debugger;
			i=i-2
		}

	}
	//todo graficar bien los datos


	//area_categorias[7].textPath=new paper.Path('M365.3,714.5c11,12.5,29.6,13.4,45,13.8c35.9,0.9,76.5-1.9,109.5-17.1c7.6-3.5,14.8-7.7,22.3-11.4c8.1-4.1,16.6-7.4,24.3-12.2c13.5-8.4,20-23.3,20-38.9');
	area_categorias[7].textPath=new paper.Path('M286.23,728.399c17.534,14.438,47.183,15.477,71.731,15.939c57.225,1.04,121.943-2.195,174.546-19.75c12.115-4.042,23.592-8.893,35.547-13.167c12.911-4.735,26.461-8.547,38.735-14.091c21.519-9.818,31.88-26.911,31.88-44.929');

	area_categorias[7].text=obj['categoria'];
	area_categorias[7].textPath.parent=area_categorias[7].grupo;
	area_categorias[7].grupo.scale(0.8);

	// var path = new paper.Path('M365.3,714.5c11,12.5,29.6,13.4,45,13.8c35.9,0.9,76.5-1.9,109.5-17.1c7.6-3.5,14.8-7.7,22.3-11.4c8.1-4.1,16.6-7.4,24.3-12.2c13.5-8.4,20-23.3,20-38.9');
	// pathfullySelected = true;
	// createAlignedText('      '+obj['categoria'], path, {fontSize: 18});




}

function Cat8(obj){
	var porcMinimo=0.1; //10%
	var colorVacio='#cccccc'
	var numSubCats=4;
	var areaBase;
	var orden=[];
	var total_area=0;
	var totalCantidad=0;
	var Cat=[];
	//var SubCatArray=[];
	var cant_vacio;
	var subCatBase;
	var indexBase;

	/*	orden[0]={sub:0, base:0};
	 orden[1]={sub:1, base:0};
	 orden[2]={sub:2, base:1};*/


	area_categorias[8]={cantidad:0,area:0, grupo:new paper.Group(), texto:'', textPath:null, scale:null};

	Cat=[
		//0
		{   sub:1,
			area:0,
			path:new paper.Path('M634.5,452.6c-12-14.3-64.2-8.4-72.8,10.4c-10.1,22.3,2.3,52.1,22.2,65.5c8.5,5.8,22.2,9.9,32.6,7.8c9.4-2,57.1-1.6,57-8.2 c24.2-14.6,69.5-11.2,73.5-33c6.2-34.2-45.7-38.1-67-46.2C659.5,441,636.6,455,634.5,452.6z'),
			color:'#932C7D',

			base:0,
			porc:0,
			xinit:151.41199552888756,
			yinit:30,
			dir:90,
			url:'', text:''


		},
		//1
		{   sub:3,
			area:0,
			path:new paper.Path('M401.8,551.4c17.8-14.8,27.7-44.4,23.7-70.4c-5.3-35.3-19.4-32.9-43.6-30.3c-61.3-4.1-105.9,4-88.8,72.7 c15.9,26.4,84.1,48.3,111.2,25.7C403.5,549.9,397.9,554.4,401.8,551.4C419.6,536.7,395.5,556.4,401.8,551.4z'),
			color:'#932C7D',

			base:0,
			porc:0,
			xinit:151.41199552888756,
			yinit:30,
			dir:90,
			url:'', text:''

		},
		//2
		{
			sub:2,
			area:0,
			path:new paper.Path('M405.5,551.4c17.8-14.8,27.7-44.4,23.7-70.4c-5.3-35.3-19.4-32.9-43.6-30.3c-61.3-4.1-105.9,4-88.8,72.7 c15.9,26.4,84.1,48.3,111.2,25.7C407.2,549.9,401.6,554.4,405.5,551.4C423.3,536.7,399.2,556.4,405.5,551.4z'),
			color:'#B63089',

			base:0,
			porc:0,
			xinit:0,
			yinit:0,
			dir:0,
			url:'', text:''
		},
		//3
		{
			sub:4,
			area:0,
			path:new paper.Path('M557.8,494.5c-0.3-12.8-5.1-26-15.3-34.4 c-11.5-9.5-25.9-2.9-38,1.2c-14.5,4.8-30,3.1-44.9,5.6c-10.6,1.8-25.3,6.1-27.6,18.5c-1.3,7.2,3,12.6,7.7,17.5 c5.5,5.7,10.4,12,16,17.5c10.8,10.6,24.4,12.6,38.6,8.7c7.1-2,14-4.4,21.4-5.3c8.1-1,16.3-0.9,24.4-2.1c5.9-0.8,12.6-2.4,16.2-7.6 C560.2,508.3,557.9,501,557.8,494.5z'),
			color:'#F7ACE4',

			base:1,
			porc:0,
			xinit:0,
			yinit:0,
			dir:0,
			url:'', text:''
		}




	]


	var num_data=Cat.length;
	var total=0;
	var i;

	cant_vacio=0;
	totalCantidad=0
	//todo reconocer valores no definidos en el objeto ingresado por json, para lo cual se agrega como 0
	//obtiene el area total
	for (i=0; i<num_data; i++){
		Cat[i]['text']=obj['data'][i]['nombre'];
		if(obj['data'][i]['cantidad']===0){
			cant_vacio=cant_vacio+1;
			Cat[i]['color']=colorVacio;

		}
		totalCantidad=totalCantidad+obj['data'][i]['cantidad'];
		if(Cat[i]['base']===1){
			//asigna el path base
			subCatBase=Cat[i]['path'];
			total_area=subCatBase.area;
			area_categorias[8].area=subCatBase.area;
			indexBase=i;
		}
		var sortIndex;
		sortIndex=Cat[i]['sub']-1;
		Cat[i]['url']=obj['data'][i]['url'];
		area_categorias[8].grupo.addChild(Cat[i]['path']);
		//total_area=total_area+area;


	}
	area_categorias[8].cantidad=totalCantidad;
	//todo: Validar que el cálculo para valores vacios está bien
	//Calcular porcentajes
	num_data=Cat.length;



	var restoPorcVacio=1-cant_vacio*0.1;
	for (i=0; i<num_data; i++){
		var x;
		var porc;
		var area;
		if(cant_vacio===Object.keys(obj['data']).length) {
			porc = 1 / cant_vacio;

		}
		else{
			if(obj['data'][i]['cantidad']===0){
				porc=0.025;
			}
			else{
				porc=(obj['data'][i]['cantidad']/totalCantidad)*restoPorcVacio;
			}

		}
		area=Math.ceil(porc*total_area);

		Cat[i]['porc']=porc;
		Cat[i]['area']=area;
		Cat[i]['text']=Cat[i]['text']+': '+obj['data'][i]['cantidad'];

	}




	var result;
	i=30;
	var flagInicio=0;
	var varx=0;
	var porcentResult=0, porcentSubcat=0;
	Cat[indexBase]['path'].strokeWidth=2;
	Cat[indexBase]['path'].fillColor=Cat[indexBase]['color'];
	Cat[indexBase]['path'].strokeColor='white';
	//posición inicial



	while(varx<=numSubCats-1){
		//break;
		//  if(result){
		//  	result.remove();
		//  }
		switch(varx){
			case 0://1
				if(flagInicio===0){
					i=1;
					flagInicio=1;
				}
				Cat[varx]['path'].position=new paper.Point(652.8090778544715-i, 490.7528523575419);
				break;
			case 1://3
				if(flagInicio===0){
					i=1;
					flagInicio=1;
				}
				Cat[varx]['path'].position=new paper.Point(357.8988955958912+i, 503.9411481100519);
				break;
			case 2://2
				if(flagInicio===0){
					i=1;
					flagInicio=1;
				}
				Cat[varx]['path'].position=new paper.Point(361.5988955958911+i, 503.9411481100519);
				break;


		}

		//grafica Subcat1

		//debugger;
		result=inters(Cat,varx, indexBase);

		porcentResult=Math.abs(result.area/total_area);
		porcentSubcat=Math.abs(Cat[varx]['porc']);

		if( porcentResult>=(porcentSubcat-0.01) && porcentResult<=(porcentSubcat+0.01) ){
			//console.log("Cat[varx]['path'].area"+Cat[varx]['path'].area);
			result.select=true;
			result.fillColor=Cat[varx]['color'];
			result.strokeColor='white';
			result.strokeWidth=2;

			Cat[varx]['path']=result;
			//console.log(Cat[varx]['path'].area);
			eventCat(Cat[varx])
			//console.log(Cat[indexBase]['path'].area);
			result=Cat[indexBase]['path'].subtract(Cat[varx]['path']);
			Cat[indexBase]['path']=result;
			//console.log(Cat[indexBase]['path'].area);
			varx=varx+1;
			flagInicio=0;
			if(varx===indexBase){
				eventCat(Cat[varx]);
				varx=varx+1;

			}
			porcentResult=0;
			porcentSubcat=0;
			/*if(varx===3){
			 break;}*/


			//if(varx>2){
			// 	clearInterval(interval);
			// }
		}
		if(porcentResult<(porcentSubcat-0.01)){
			i=i+4;
			//debugger;
		}
		else if(porcentResult>(porcentSubcat+0.01)){
			//debugger;
			i=i-2
		}

	}
	//todo graficar bien los datos


	area_categorias[8].textPath=new paper.Path('M457,546.3c3.9-4.9,11.8,2.4,17.4-2.4c8.2-0.6,16.3,0.6,24.5,0.2c7.4-0.4,14.5-2.3,21.7-3.8 c14.4-3,28.2-6.1,39.8-3.1');
	area_categorias[8].text=obj['categoria'];
	area_categorias[8].textPath.parent=area_categorias[8].grupo;
	var punto=new paper.Point(area_categorias[8].grupo.position._x-300,area_categorias[8].grupo.position._y+100);
	area_categorias[8].grupo.scale(0.8, punto);

		// Set the shadow blur radius to 12:


	// var path = new paper.Path('M457,546.3c3.9-4.9,11.8,2.4,17.4-2.4c8.2-0.6,16.3,0.6,24.5,0.2c7.4-0.4,14.5-2.3,21.7-3.8 c14.4-3,28.2-6.1,39.8-3.1');
	// pathfullySelected = true;
	// createAlignedText('      '+obj['categoria'], path, {fontSize: 18});




}




var createAlignedText = function(str, path, style) {
	if (str && str.length > 0 && path) {
		// create PointText object for each glyph
		var glyphTexts = [];
		var i
		for (i = 0; i < str.length; i++) {
			glyphTexts[i] = createPointText(str.substring(i, i+1), style);
			glyphTexts[i].justification = "center";
		}
		// for each glyph find center xOffset
		var xOffsets = [0];
		for (i = 1; i < str.length; i++) {
			var pairText = createPointText(str.substring(i - 1, i + 1), style);
			pairText.remove();
			xOffsets[i] = xOffsets[i - 1] + pairText.bounds.width -
				glyphTexts[i - 1].bounds.width / 2 - glyphTexts[i].bounds.width / 2;
		}
		// set point for each glyph and rotate glyph aorund the point
		for (i = 0; i < str.length; i++) {
			var centerOffs = xOffsets[i];
			if (path.length < centerOffs) {
				if (path.closed) {
					centerOffs = centerOffs % path.length;
				}  else {
					centerOffs = undefined;
				}
			}
			if (centerOffs === undefined) {
				glyphTexts[i].remove();
			} else {
				var pathPoint = path.getPointAt(centerOffs);
				glyphTexts[i].point = pathPoint;
				var tan = path.getTangentAt(centerOffs);
				glyphTexts[i].rotate(tan.angle, pathPoint);
			}
		}
	}
}

// create a PointText object for a string and a style
var createPointText = function(str, style) {
	var text = new paper.PointText();
	text.content = str;
	if (style) {
		if (style.font) text.font = style.font;
		if (style.fontFamily) text.fontFamily = style.fontFamily;
		if (style.fontSize) text.fontSize = style.fontSize;
		if (style.fontWieght) text.fontWeight = style.fontWeight;
	}
	return text;
}




var selected;

