/*dar de Alta al usuario*/
function alta(id){

  $.get('/userDeAlta/'+id+'', function(response) {
        $.confirm({
          icon:'glyphicon glyphicon-ok text-green',
          title: 'Datos Actualizados',
          content: '<center><h3>'+response+'</h3></center>',
          confirmButtonClass: 'btn-success',
          confirmButton: 'Confirmar',
          cancelButton: false,
          confirm: function () {
            location.reload(true);
           } 
        });
  });
}
/*reincorporar*/
function activo(id){

  $.get('/userActivo/'+id+'', function(response) {
        $.confirm({
          icon:'glyphicon glyphicon-ok text-green',
          title: 'Datos Actualizados',
          content: '<center><h3>'+response+'</h3></center>',
          confirmButtonClass: 'btn-success',
          confirmButton: 'Confirmar',
          cancelButton: false,
          confirm: function () {
            location.reload(true);
           } 
        });
  });
}

function eliminar(id){

  $.confirm({
    title:"Eliminar Datos",
    icon:"fa fa-trash-o text-danger",
    content:"<center><h2>Seguro que quieres eliminar este Usuario</h2> <strong>al confirmar los datos ser&aacute;n eliminados por completo de la base de datos</strong></center>",
    confirmButton: 'Confirmar',
    confirmButtonClass: 'btn-danger',
    cancelButtonClass: 'btn-info',
    cancelButton: 'Cancelar',
    confirm: function () {
        $.get('/eliminarU/'+id+'', function(response) {
            $.dialog('<center><h2>'+response+'</h2></center>');
            location.reload(true);
        });
     },
    cancel: function () {
      
     }
  });

}

function cleanToken(){
  localStorage.removeItem('laravel_session');
  localStorage.removeItem('XSRF-TOKEN');

}