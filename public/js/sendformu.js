function sendButtonAlwayActive() {
  if (this.status) return;
  setInterval(() => {
    $("#sendbutton").attr('disabled', false);
  }, 100);
  this.status = true;
}

$(document).on("submit",".form_entrada",function(e){
// $('sendbutton').on("click", function(e) {
//funcion para atrapar los formularios y enviar los datos

        e.preventDefault();
        checkCaptcha();
        sendButtonAlwayActive();
        var formu=$(this);
        var quien=$(this).attr("id");
        var token = $("#token").val();
        /*campos de icheck que validar que sean true*/
        var terminos = $('.terminos').is(':checked') ;
        var id_perfil = $('.id_perfil').is(':checked') ;
        var id_nombre = $('.id_nombre').val() ;
        var id_telefono = $('.id_telefono').val() ;
     
        var id_palabras_claves  = $('.id_palabras_claves ').val() ;
        var id_correo  = $('.id_correo ').val() ;


        var id_area_LIC = $('.id_area_LIC').is(':checked') ;
        var id_status_gestion_laboral = $('.id_status_gestion_laboral').is(':checked') ;
        var id_status_form_docencia = $('.id_status_form_docencia').is(':checked') ;
        var id_status_invest_bio = $('.id_status_invest_bio').is(':checked') ;
        var captcha = $('#valcaptcha').val();
        var recaptchaRes = $("#recaptchaRes").val();

        console.log(`captcha: ${captcha}\nrecaptchaRes: ${recaptchaRes}`);

        // se declara el id del formulario con su ruta y el div de resultado
        if(quien=="form_usuario"){ var miurl="/newUsuario";  }
        // a condition with 900 carapters should never be written in a single line,
        // if a value is true, you can obmit '== true' because true === true is
        // redundant
        if ( id_perfil && id_nombre !='' && id_telefono !=''  !='' && id_palabras_claves !='' && id_correo !='' /*&& id_area_LIC*/ && id_status_gestion_laboral && 
             id_status_form_docencia && id_status_invest_bio && terminos && captcha != '' &&
             recaptchaRes != '' && recaptchaRes == 'true' && miurl) {
           $.ajax({
              url: miurl,
              type:"POST",
              headers:{'X-CSRF-TOKEN':token},

              data:formu.serialize(),

                beforeSend: function(){
                         $("#guardando").show();
                         $("#guardar").hide();
                      },
              success : function(response){
                   $("#guardando").hide();
                   $("#guardar").show();
                   $.confirm({
                      title: 'Respuesta',
                      icon:'fa fa-comment-o text-green',
                      content:'<center><h2>'+response+'</h2></center>',
                      confirmButton: 'Continuar',
                      cancelButton: false,
                      confirmButtonClass: 'btn-success',
                      animation: 'zoom', 
                      confirm: function(){
                        if (response != 'Verifica el recaptcha') {
                          location.reload(true);
                        }

                      }
                  });
              },
              error:function(){
                   $("#guardando").hide();
                   $("#guardar").show();

              }
          });
        }
          else{
  /*buscamos cual es vacio para mostrar mensaje*/
  if (!terminos) {$("#term_msj").empty().show().append('Debes aceptar los T&eacute;rminos'); anclaError('term_msj')}

  if (captcha == '') {$('#msj_captcha').empty().show().append('Debes ingresar el c&oacute;digo de valides');}

  if (recaptchaRes == '') {$('#msj_captcha').empty().show().append('Debes ingresar el c&oacute;digo de valides');}

  if (recaptchaRes == 'false') {$('#msj_captcha').empty().show().append('El c&oacute;digo de valides no es correcto');}

 if (id_palabras_claves=='') {$("#msj_id_palabras_claves").empty().show().append('Campo Requerido'); anclaError('palabras_claves')}
  else{$("#msj_id_palabras_claves").empty();}

 if (id_correo=='') {$("#msj_id_correo").empty().show().append('Campo Requerido'); anclaError('correo')}
  else{$("#msj_id_correo").empty();}

 if (id_telefono=='') {$("#msj_id_telefono").empty().show().append('Campo Requerido'); anclaError('msj_id_telefono')}
  else{$("#msj_id_telefono").empty();}


if (!id_status_invest_bio) {$("#msj_id_status_invest_bio").empty().show().append('Campo Requerido'); anclaError('status_invest_bio')}

if (!id_status_form_docencia) {$("#msj_id_status_form_docencia").empty().show().append('Campo Requerido'); anclaError('status_form_docencia')}

if (!id_status_gestion_laboral) {$("#msj_id_status_gestion_laboral").empty().show().append('Campo Requerido'); anclaError('status_gestion_laboral')}

//if (!id_area_LIC) {$("#msj_id_area_LIC").empty().show().append('Campo Requerido'); anclaError('area_LIC')}

if (!id_perfil) {$("#msj_perfil").empty().show().append('Campo Requerido'); anclaError('id_perfil_error')}

 if (id_nombre=='') {$("#msj_id_nombre").empty().show().append('Campo Requerido'); anclaError('nombre')}
  else{$("#msj_id_nombre").empty();}
 

  

            


          }
 

})

/*ocultar mensaje de requerido para cada radio button*/
$(document).ready(function() {

  validar('id_perfil','msj_perfil');

  validar('id_area_LIC','msj_id_area_LIC');
  validar('id_status_gestion_laboral','msj_id_status_gestion_laboral');
  validar('id_status_form_docencia','msj_id_status_form_docencia');
  validar('id_status_invest_bio','msj_id_status_invest_bio');
  validar('terminos','term_msj');


  /*cambiar status de ambito del usuario en actualizar datos*/


});
function msjclear(name){
  $("#"+name).empty();
}
function anclaError(id){
   $('body,html').stop(true,false).animate({        
        scrollTop: $('#'+id).offset().top
      },1000);
   $("#sendbutton").attr('disabled', true);
}

function validar(item, msj){
   $('.'+item).on('ifChecked', function(event){
       $('#'+msj).hide();
       $('#'+msj).empty();
       $("#sendbutton").attr('disabled', false);
    });
}


