$( document ).ready(()=>{
	 areasLicSelect()
   //no repetir formacion
    $('#norepeatformacion').keyup(function(event) {
            
            $.get("/norepeatformacion/"+event.target.value+"",function(response){
                    
                    for (i = 0; i < response.length; i++) {
                        
                        if (response[i].title) {

                            $("#msj_form").empty().append('Está Formación ya existe');
                            $('#first_name').parent().css('border-color','#a94442');
                            $("#sendform").attr('disabled', true);

                        }

                    }  
                     if(!$.isArray(response) || !response.length){ 
                            $("#msj_form").empty();
                            $('#first_name').parent().css('border-color','none');                          
                            $("#sendform").attr('disabled', false);
                            
                        }
                 });
            
        }); 
})

 $(document).on('submit', '.form_new', function(e) {
                
    e.preventDefault();
        var formu=$(this);
        var quien=$(this).attr("id");
        var token = $("#token").val();
    //se declara el id del formulario con su ruta 
    if(quien=="form_licen"){ var miurl="/newFormacion";}
      $.confirm({
      icon:'fa fa-comment-o text-green',
      title: 'Pregunta',
      content:'Seguro que quieres guardar "<b>'+$("#norepeatformacion").val()+'</b>" como Formación académica',
      confirmButtonClass: 'btn-success',
       confirmButton: 'Continuar',
      cancelButton: 'Volver',
      backgroundDismiss: true,
      
              confirm: function(){
                 $.ajax({
                  type: "POST",
                  url : miurl,
                  datatype:'json',
                  headers:{'X-CSRF-TOKEN':token},
                  data : formu.serialize(),
                  beforeSend: function(){
                    $("#guardarformacion").css("display","none");
                    $("#saveformacion").show();
                  },
                
                  success : function(response){
                    $("#saveformacion").hide();
                    $("#guardarformacion").css("display","block");  
                    $('#myModal').modal('hide');  
                    areasLicSelect()
                     $.dialog({
                      icon:'fa fa-thumbs-o-up text-green',
                        title: 'Correcto',
                        content: 'Se agregó la nueva licenciatura ya se encuentra en el listado de <b>"Formación académica de licenciatura"</b>',
                    });

                     $('#'+quien).trigger("reset");
                    }, //si ha ocurrido un error
                  error: function(response){  
                        $.confirm({
                          icon: 'fa fa-warning',title:'Hoops!',
                          content:"Ocurrió un error al cargar los datos",
                          confirmButton: 'Confirmar',cancelButton: false,
                          animation: 'zoom', confirmButtonClass: 'btn-info',
                        });
                  }
            });
              
          },
          cancel: function () {
              
          }
      
    });
});

//cargar listado de licenciaturas agrupadas por areas
 function areasLicSelect(){
   	$('.Licenciatura').empty();
 		var $select = $('.Licenciatura');
    $select.select2();
		$.ajax({ 
		  type: 'GET',
		  url: '/listdegrees',
		  dataType: 'json'
		}).then(function (data) {
			$select.select2({data: []})
		  $select.select2({data:data});
		  $select.trigger('change'); // notify JavaScript components of possible changes
		});
 } 
 
