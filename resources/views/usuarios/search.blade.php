@extends('layouts.app')
@section('htmlheader_title')
Mis datos
@endsection
@section('main-content')
@foreach($detalles as $dt)
<ul class="nav nav-tabs">
	<li class="active"><a data-toggle="tab" href="#data">Mis datos</a></li>
</ul>
<!-- Datos de usuario -->
<div class="tab-content">
	<div id="data" class="tab-pane fade in active">
		@include('usuarios.misdatos')
	</div>
</div>
@endforeach
@endsection