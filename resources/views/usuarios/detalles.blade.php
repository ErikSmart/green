@foreach($detalles as $dt)
<form id="form_update_usuario" class="form_actualizar" action="{{url('/actualizardatos')}}" method="post">
	<input type="hidden" name="_token" value="{{csrf_token()}}" id="token">
	<input type="hidden" name="idUser" value="{{$dt->id}}" id="idUser">
	<div class="panel-default">
		<div class="panel panel-body">
			<div class="col-md-6">
				<div class="form-group">
					<label>Nombre Completo</label>
					<input type="text" name="nombreCompleto" value="{{$dt->nombreCompleto}}" class="form-control" >
				</div>
				<div class="form-group">
					<label>Relación con actual con la Facultad *</label>
					@foreach($perfil as $p)
					
					@if($dt->id_perfil == $p->id)
					<div class="form-group">
						<input type="radio" name="id_perfil"  value="{{$p->id}}" checked="">&nbsp;&nbsp;{{$p->descripcion}}
					</div>
					@else
					<div class="form-group">
						<input type="radio" name="id_perfil"  value="{{$p->id}}" >&nbsp;&nbsp;{{$p->descripcion}}
					</div>
					@endif
					@endforeach
				</div>
				<div class="form-group">
					<label> FORMACIÓN ACADÉMICA EN LA FACULTAD DE BIOÉTICA DE LA UNIVERSIDAD ANÁHUAC Y STATUS (INCOMPLETA, COMPLETA, o TITULADO) )</label>
					{{-- @foreach($statusF as $sf)
					@if($dt->id_status_formacion == $sf->id)
					<div class="form-group">
						<input type="radio" name="id_status_formacion"  value="{{$sf->id}}" checked="" >&nbsp;&nbsp;{{$sf->descripcion}}
					</div>
					@else
					<div class="form-group">
						<input type="radio" name="id_status_formacion"  value="{{$sf->id}}"  >&nbsp;&nbsp;{{$sf->descripcion}}
					</div>
					@endif
					@endforeach --}}
					<input type="text" class="form-control id_perfil" name="relacion" value="{{$dt->relacion}}" >
				</div>
				<div class="form-group">
					<label> Formación Académica de LICENCIATURA</label>
					
					<select class="Licenciatura form-control" name="formAcademicLIC" style="width:  100%">
						@foreach($areasLic as $licDe)
						<optgroup label="{{$licDe->acronimo}}">
							@foreach($degree->where('areasLic',$licDe->id)->where('otros',0) as $dee)
								@if($licDe->id != 8 )
									@if($dt->formAcademicLIC == $dee->id)
										<option value="{{$dee->id}}" selected="true">{{$dee->title}}</option>
									@else
										<option value="{{$dee->id}}">{{$dee->title}}</option>
									@endif
								@endif
							@endforeach
							@foreach($degree->where('otros',1) as $dee)
								@if($licDe->id == 8 )
									@if($dt->formAcademicLIC == $dee->id)
										<option value="{{$dee->id}}" selected="true">{{$dee->title}}</option>
									@else
										<option value="{{$dee->id}}">{{$dee->title}}</option>
									@endif
								@endif
							@endforeach
						</optgroup>
						@endforeach
					</select>
				</div>
				<div class="form-group">
					<label>Identifique el ÁREA a la que corresponde su LICENCIATURA de base</label>
					@foreach($areasLic as $alic)
					@if($alic->id == $dt->id_area_LIC)
					<div class="form-group">
						<input type="radio" name="id_area_LIC"  value="{{$alic->id}}" checked="" >&nbsp;&nbsp;{{$alic->nombreArea}}
					</div>
					@else
					<div class="form-group">
						<input type="radio" name="id_area_LIC"  value="{{$alic->id}}" >&nbsp;&nbsp;{{$alic->nombreArea}}
					</div>
					@endif
					@endforeach
				</div>
				<div class="form-group">
					<label>Formación Académica de MAESTRÍA (A parte de la de Bioética)</label>
					<input type="text"  name="formdeMaestria" value="{{$dt->formdeMaestria}}" class="form-control" >
				</div>
				<div class="form-group">
					<label>Formación Académica de ESPECIALIDAD (A parte de la de Bioética)</label>
					<input type="text"  name="formEspecialidad" value="{{$dt->formEspecialidad}}" class="form-control" >
				</div>
				<div class="form-group">
					<label>Formación Académica de DOCTORADO (A parte de la de Bioética)</label>
					<input type="text" name="formDoctorado"  value="{{$dt->formDoctorado}}" class="form-control" >
				</div>
				<div class="form-group">
					<label>Otra formación de POSGRADO (A parte de la mencionada antes) </label>
					<textarea name="formPostGrado" class="form-control" style="max-width: 100%">{{$dt->formPostGrado}}</textarea>
					
				</div>
				<div class="form-group">
					<label>Ámbito Gestión/Laboral en Bioética-STATUS</label>
					@foreach($statusL as $sl)
					@if($sl->id == $dt->id_status_gestion_laboral)
					<div class="form-group">
						<input type="radio" name="id_status_gestion_laboral"  value="{{$sl->id}}" checked="" >&nbsp;&nbsp;{{$sl->descripcion}}
					</div>
					@else
					<div class="form-group">
						<input type="radio" name="id_status_gestion_laboral"  value="{{$sl->id}}" >&nbsp;&nbsp;{{$sl->descripcion}}
					</div>
					@endif
					@endforeach
				</div>
				<div class="form-group">
					<input type="hidden" name="idrespuestas1" value="{{$dt->id_respuestas_gestion_laboral}}">
					<label>Ámbito Gestión/Laboral -Bioética -PROYECTOS o ACTIVIDADES</label>
					<div class="form-group">
						<input type="text" name="rl1" value="{{$dt->rl1}}" class="form-control" >
					</div>
					<div class="form-group">
						<input type="text" name="rl2" value="{{$dt->rl2}}" class="form-control" >
						
					</div>
					<div class="form-group">
						<input type="text" name="rl3" value="{{$dt->rl3}}" class="form-control" >
					</div>
				</div>
				<div class="form-groupo">
					<input type="hidden" name="idrespuestas2" value="{{$dt->id_respuestas_form_docencia}}">
					<label>Ámbito de Formación/Docencia en Bioética- TEMAS/ ÁREA </label>
					<div class="form-group">
						<input type="text" name="rd1" value="{{$dt->rd1}}" class="form-control" >
					</div>
					<div class="form-group">
						<input type="text" name="rd2" value="{{$dt->rd2}}" class="form-control" >
						
					</div>
					<div class="form-group">
						<input type="text" name="rd3" value="{{$dt->rd3}}" class="form-control" >
					</div>
				</div>
				
				<div class="form-group">
					<label> Ámbito de Formación/Docencia en Bioética - STATUS  </label>
					@foreach($statusL as $sL)
					@if($sL->id == $dt->id_status_form_docencia)
					<div class="form-group">
						<input type="radio" name="id_status_form_docencia"  value="{{$sL->id}}" checked="">&nbsp;&nbsp;{{$sL->descripcion}}
					</div>
					@else
					<div class="form-group">
						<input type="radio" name="id_status_form_docencia"  value="{{$sL->id}}"  >&nbsp;&nbsp;{{$sL->descripcion}}
					</div>
					@endif
					@endforeach
				</div>
				<div class="form-group">
					<label for="">Ámbito de Investigación en Bioética - STATUS </label>
					@foreach($statusL as $sL)
					@if($sL->id == $dt->id_status_invest_bio)
					<div class="form-group">
						<input type="radio" name="id_status_invest_bio"  value="{{$sL->id}}" checked="">&nbsp;&nbsp;{{$sL->descripcion}}
					</div>
					@else
					<div class="form-group">
						<input type="radio" name="id_status_invest_bio"  value="{{$sL->id}}"  >&nbsp;&nbsp;{{$sL->descripcion}}
					</div>
					@endif
					@endforeach
				</div>
				<div class="form-group">
					<label>Ámbito de Investigación en Bioética - LÍNEAS DE INVESTIGACIÓN / TEMAS (Campo tipo TextArea)</label>
					<div class="form-group">
						<textarea name="investigacion_temas" cols="30" rows="5"  style="max-width: 100%" class="form-control">{{$dt->investigacion_temas}}</textarea>
					</div>
				</div>
				
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label>T&eacute;lefono</label>
					<div class="form-group">
						<input name="telefono"  class="form-control" value="{{$dt->telefono}}" >
					</div>
					<label>T&eacute;lefono Secundario</label>
					<div class="form-group">
						<input name="telefono2"  class="form-control" value="{{$dt->telefono2}}" >
					</div>
					<label>Correo</label>
					<div class="form-group">
						<input type="email" class="form-control" name="correo"  value="{{$dt->correo}}" autocomplete="off" id="searchMail" required="">
						<p id="msj_mailT" style="color: #a94442"></p>
					</div>
					<label>Correo Secundario</label>
					<div class="form-group">
						<input name="correo2"  class="form-control" value="{{$dt->correo2}}" >
					</div>
				</div>
				
				<div class="form-group">
					<label> Ámbito Gestión/Laboral -Áreas de la Bioética</label>
					
						@foreach($selectA1 as $sl)
						@if($sl->cheked == 1)
						@if($sl->nombre == 'Otro')
						<div class="form-group">
							<label>
								<input type="checkbox" name="gestion_laboral_areas[]"  value="{{$sl->id}}" checked="">&nbsp;&nbsp;{{$sl->nombre}}
								
								</label><input type="text" name="gestion_laboral" value="{{$dt->gestion_laboral}}" autocomplete="off">
							</div>
							@else
							<div class="form-group">
								<input type="checkbox" name="gestion_laboral_areas[]"  class="ambito" data-idA="{{$sl->id}}" value="{{$sl->id}}" checked="">&nbsp;&nbsp;{{$sl->nombre}}
								
							</div>
							@endif
							@else
							@if($sl->nombre == 'Otro')
							    <div class="form-group">
								<label>
									<input type="checkbox" name="gestion_laboral_areas[]"  value="{{$sl->id}}" >&nbsp;&nbsp;{{$sl->nombre}}
									
									</label><input type="text" name="gestion_laboral" value="{{$dt->gestion_laboral}}" autocomplete="off">
								</div>
								@else
								<div class="form-group">
									
									<input type="checkbox" name="gestion_laboral_areas[]" id="" class="ambito" data-idA="{{$sl->id}}"  value="{{$sl->id}}"  >&nbsp;&nbsp;{{$sl->nombre}}
								</div>
								@endif
								@endif
								
								@endforeach
							
						</div>
						
						
						
						<div class="form-group">
							<label>Ámbito de Formación/Docencia- Área de la Bioética  </label>
							
							@foreach($selectA2 as $sl)
							@if($sl->cheked == 1)
							@if($sl->nombre == 'Otro')
							<div class="form-group">
								<label>
									<input type="checkbox" name="gestion_docencia_areas[]"  value="{{$sl->id}}" checked="">&nbsp;&nbsp;{{$sl->nombre}}
									
									</label><input type="text" name="formacion_docencia" value="{{$dt->formacion_docencia}}" autocomplete="off">
								</div>
								@else
								<div class="form-group">
									
									<input type="checkbox" name="gestion_docencia_areas[]"  class="ambito" data-idA="{{$sl->id}}" value="{{$sl->id}}" checked="">&nbsp;&nbsp;{{$sl->nombre}}
								</div>
								@endif
								@else
								@if($sl->nombre == 'Otro')
                                <div class="form-group">

								<label>
									<input type="checkbox" name="gestion_docencia_areas[]"  value="{{$sl->id}}" >&nbsp;&nbsp;{{$sl->nombre}}
									
									</label><input type="text" name="formacion_docencia" value="{{$dt->formacion_docencia}}" autocomplete="off">
								</div>
								@else
                                <div class="form-group">
								
									<input type="checkbox" name="gestion_docencia_areas[]" id="" class="ambito" data-idA="{{$sl->id}}"  value="{{$sl->id}}"  >&nbsp;&nbsp;{{$sl->nombre}}
								</div>
								@endif
								@endif
								@endforeach
								
							</div>
							<div class="form-group">
								<label>Ámbito de Investigación - Área de la Bioética</label>
								
						@foreach($selectA3 as $sl)
							@if($sl->cheked == 1)
								@if($sl->nombre == 'Otro')
	                                <div class="form-group">
									<label>
										<input type="checkbox" name="gestion_inves_areas[]"  value="{{$sl->id}}" checked="">&nbsp;&nbsp;{{$sl->nombre}}
										
										</label><input type="text" name="temas_area" value="{{$dt->temas_area}}" autocomplete="off">
									</div>
								@else
	                                <div class="form-group">
									
										<input type="checkbox" name="gestion_inves_areas[]"  class="ambito" data-idA="{{$sl->id}}" value="{{$sl->id}}" checked="" >&nbsp;&nbsp;{{$sl->nombre}}
									</div>
								@endif
							@else
								@if($sl->nombre == 'Otro')
	                                <div class="form-group">

									<label>
										<input type="checkbox" name="gestion_inves_areas[]"  value="{{$sl->id}}" >&nbsp;&nbsp;{{$sl->nombre}}
										
										</label><input type="text" name="temas_area" value="{{$dt->temas_area}}" autocomplete="off">
									</div>
								@else
									<div class="form-group">
										<input type="checkbox" name="gestion_inves_areas[]" id="" class="ambito" data-idA="{{$sl->id}}"  value="{{$sl->id}}"  >&nbsp;&nbsp;{{$sl->nombre}}
									</div>
								@endif
							@endif
						@endforeach
			</div>
							<div class="form-group">
								
								<div class="s2-example">
									<label>Palabras Claves</label>
									<input type="text" class="form-control" value="{{$dt->palabras_claves}}" name="palabras_claves" required="">
									
								</div>
							</div>
						</div>
						<div class="col-md-12" id="guardando" style="display: none;"><center><label> Guardando Datos..</label><i class="fa fa-spinner fa-spin fa-2x fa-fw"></i></center></div>
						<div class="col-md-12">
							
							<button type="submit" class="btn btn-info btn-block">Actualizar Datos</button>
							<hr>
						</div>
					</div>
				</div>
			</form>
			@endforeach
			<!-- iCheck -->
			<script src="{{ asset('/plugins/iCheck/icheck.min.js') }}" type="text/javascript"></script>
			<script>
			$(function () {
			$('input').iCheck({
			checkboxClass: 'icheckbox_square-blue',
			radioClass: 'iradio_square-blue',
			increaseArea: '20%' // optional
			});
			});
				$('.Licenciatura').select2();
			$(".js-example-tokenizer").select2({
			tags: true,
			tokenSeparators: [',', ' ']
			})
			</script>
			<script src="{{ asset('/plugins/select2/select2.min.js') }}"></script>