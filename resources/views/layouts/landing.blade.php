<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Bioética</title>
        <link rel="shortcut icon" href="{{ asset('/img/logo.gif')}}" />
        <!-- Bootstrap core CSS -->
        <link href="{{ asset('/css/bootstrap.css') }}" rel="stylesheet">
       
         <!-- select2 -->
        <link rel="stylesheet" href="{{ asset('/plugins/select2/select2.min.css')}}">
        <link rel="stylesheet" href="{{ asset('/js/slick/slick-theme.css')}}">
        <link rel="stylesheet" href="{{ asset('/css/jquery-confirm.css')}}">
        <link rel="stylesheet" href="{{ asset('/css/secondFooter.css')}}">
        <link rel="stylesheet" href="{{ asset('/css/secondFooter.css')}}">
        <link rel="stylesheet" href="{{ asset('/css/headerHidden.css')}}">
        <link rel="stylesheet" href="{{ asset('/css/headerMenu.css')}}">
        <link rel="stylesheet" href="{{ asset('/js/slick/slick.css')}}">
        <link rel="stylesheet" href="{{ asset('/css/main.css')}}">
        <link rel="stylesheet" href="{{ asset('/css/menu.css')}}">
        <link rel="stylesheet" href="{{ asset('/css/mapa.css')}}">
        <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Raleway:400,300,700' rel='stylesheet' type='text/css'>
        <!-- Font Awesome Icons -->
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        
        <!-- iCheck -->
        <link href="{{ asset('/plugins/iCheck/square/blue.css') }}" rel="stylesheet" type="text/css" />
       
        @include('cuerpo.header')
    </head>
    
    <body data-spy="scroll" data-offset="0" data-target="#navigation" id="capaRespuesta">

        <div class="text-center preloadDiv"  id="preloadDiv">
            <i class="fa fa-spinner fa-pulse fa-3x fa-fw text-orange" style="font-size: 4em"></i>
        </div>
        
       
        <div class="container-fluid" style="padding-left: 0">
        <nav class="contenerdorbotones col-sm-3 menuShow hidden-md hidden-xs" style="padding: 0">
        <a href="{{url('/mapa')}}" class="boton-navegacion " @if(isset($Mactive) && !empty($Mactive))style="{{$Mactive}}"@endif  title="Encuentra un profecional en bioética">MAPA</a>
        <a href="{{url('/quienes-somos')}}" class="boton-navegacion "@if(isset($Qactive) && !empty($Qactive))style="{{$Qactive}}"@endif  title="Quiénes somos los de área de bioética">QUIÉNES<br>SOMOS</a>
        <a href="{{url('/areas-de-bioetica')}}" class="boton-navegacion "@if(isset($Bactive) && !empty($Bactive))style="{{$Bactive}}"@endif  title="Áreas disponibles del la bioética">ÁREAS DE <br>LA BIÓETICA</a>
        <a href="{{url('/unete')}}" class="boton-navegacion " @if(isset($Uactive) && !empty($Uactive))style="{{$Uactive}}"@endif  title="Formulario registrate en bioética">ÚNETE</a>
        </nav>
                
            @yield('content')
        </div>
        <!-- footer -->
           @include('cuerpo.footer')
           @include('cuerpo.footerSecond')
        <!-- end footer -->
        
  
        <!-- jQuery 2.1.4 -->
    <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
    <script type="text/javascript">
        window.___gcfg = {lang: 'es-419'};
        (function() {
        var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
        po.src = 'https://apis.google.com/js/platform.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
        })();
        </script>
        <div id="fb-root"></div>
        <script>
        window.fbAsyncInit = function() {
        FB.init({
        appId      : '114567602489443',
        xfbml      : true,
        version    : 'v2.9'
        });
        FB.AppEvents.logPageView();
        };
        (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/es_LA/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="{{ asset('/js/push.min.js')}}"></script>
    <script src="{{ asset('/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <!-- iCheck -->
    <script src="{{ asset('/plugins/iCheck/icheck.min.js') }}" type="text/javascript"></script>
    <!-- selct2 -->
    <script src="{{ asset('/plugins/select2/select2.min.js') }}"></script>
    <!-- validaciones de boostrap -->
    <script src="{{ asset('/js/formularioUsuario.js') }}"></script>
    <script src="{{ asset('/js/bootstrapValidator.min.js') }}"></script>
    <script src="{{ asset('/js/sendformu.js')}}"></script>
    <!-- buscador de palabras  -->
    <script src="{{ asset('/js/busqueda.js')}}"></script>
    <!-- header menu  -->
    <script src="{{ asset('/js/headerMenu.js')}}"></script>
    <!-- dar de alta o reincoporar el usuario -->
    <script src="{{ asset('/js/accionesUser.js')}}"></script>
    <script src="{{ asset('/js/jquery-confirm.js')}}"></script>
    <script src="{{ asset('/js/slick/slick.js')}}"></script>
    <script src="{{ asset('/js/captcha.js')}}"></script>
    
    <script src="{{ asset('/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
    <script src="{{ asset('/js/historialCookies.js')}}"></script>
    <script src="{{ asset('/js/mapa/paper/paper-full.min.js')}}"></script>
    <script src="{{ asset('/js/mapa/powertip/jquery.powertip.js')}}"></script>
    <script src="{{ asset('/js/mapa/paper-core.js')}}"></script>
    <script src="{{ asset('/js/mapa/dibujarmapa.js')}}"></script>
    
 
    @notmobile 
           <script src="{{ asset('/js/mapa/mapa_web5.js')}}"></script>
      
        @endnotmobile
     
        @mobile
           <script src="{{ asset('/js/mapa/mapa2.js')}}"></script>

        @endmobile
   

   
    <script src="{{ asset('/js/sendMail.js')}}"></script>
    <script src="{{ asset('/js/inicial.js')}}"></script>    
    @yield('js')
      </body>
</html>
        
 
