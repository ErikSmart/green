<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
        <li><a href="{{url('/')}}" style="padding: 0;margin:0;border-left:0"><img src="{{asset('/img/logo/logo-anahuac.png')}}" class="img-responsive" style="width: 230px"></a></li>
            <!-- Optionally, you can add icons to the links -->
            @if(Auth::user()->perfil_id != 1)
             <li class="header"><center>Panel Personal</center></li>
            <li class="active listLogo"><a href="{{ url('/misdatos') }}"><i class='fa fa-file-text-o'></i> <span>Mis datos</span></a></li>   
            @else
            <li class="header"><center>Panel Administrador</center></li>
            <li @if(isset($Hactive) && !empty($Hactive))class="{{$Hactive}} listLogo"@endif><a href="{{ url('home') }}"><i class='fa fa-list-ol'></i> <span>Listado de Usuarios</span></a></li>   
            <li @if(isset($Dactive) && !empty($Dactive))class="{{$Dactive}} listLogo"@endif><a href="{{ url('historials') }}"><i class='fa fa-list-alt'></i> <span>Historial</span></a></li> 
            <li @if(isset($Lactive) && !empty($Lactive))class="{{$Lactive}} listLogo"@endif><a href="{{ url('licenciaturas') }}"><i class='fa fa-university'></i> <span>Listado Licenciaturas</span></a></li>   
            <li><a href="#" onclick="configTimes()"><i class='fa fa-clock-o'></i> <span>Configurar Hora</span></a></li>   
            @endif       
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
