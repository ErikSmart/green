<!-- jQuery 2.1.4 -->
<script src="{{ asset('/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
<!-- Bootstrap 3.3.2 JS -->
<script src="{{ asset('/js/bootstrap.min.js') }}" type="text/javascript"></script>
<!-- iCheck -->
<script src="{{ asset('/plugins/iCheck/icheck.min.js') }}" type="text/javascript"></script>
<!-- selct2 -->
<script src="{{ asset('/plugins/select2/select2.min.js') }}"></script>
<!-- validaciones de boostrap -->
<script src="{{ asset('/js/formularioUsuario.js') }}"></script>
<script src="{{ asset('/js/bootstrapValidator.min.js') }}"></script>

<script src="{{ asset('/js/jquery-confirm.js')}}"></script>
<script src="{{asset('/js/sendformu.js')}}"></script>


  <script src="/js/busqueda.js"></script>
<!-- buscador de palabras  -->
<script>
function myFunction() {

    var input, filter, ul, li, a, i;

    input = document.getElementById("myInput");

    filter = input.value.toUpperCase();

    ul = document.getElementById("myUL");

    li = ul.getElementsByTagName("li");

    for (i = 0; i < li.length; i++) {

        a = li[i].getElementsByTagName("a")[0];
        
        if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";

        }
    }
}

</script>