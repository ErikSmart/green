<!-- Main Header -->
<header class="main-header">
    <!-- Logo -->
    <a href="{{ url('/') }}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>R</b>B</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Bioética</b> </span>
    </a>
    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">{{ trans('adminlte_lang::message.togglenav') }}</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                @if(Auth::user()->perfil_id == 1)
                <!-- Notifications Menu -->
                <li class="dropdown notifications-menu">
                    <!-- Menu toggle button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-bell-o"></i>
                        <span class="label label-danger" id="cantRN"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header" id="cantRT"></li>
                        <li>
                            <ul class="menu" id="newRegistros">
                                <!-- Inner Menu: contains the notifications -->
                            </ul>
                        </li>
                         
                    </ul>
                </li>
                <li class="dropdown tasks-menu licO">
                    <a href="#" class="dropdown-toggle licA" data-toggle="dropdown" aria-expanded="true">
                        <i class="fa fa-graduation-cap"></i>
                        <span class="label label-success"  id="LicN"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header"  id="LicNT"></li>
                        <li>
                            <!-- inner menu: contains the actual data -->
                            <ul class="menu" id="newLice">
                            </ul>
                        </li>
                        <li class="footer">
                            <a href="{{url('/licenciaturas')}}">Ver todas las Lic.</a>
                        </li>
                    </ul>
                </li>
@endif

@if (Auth::guest())
<li><a href="{{ url('/register') }}">{{ trans('adminlte_lang::message.register') }}</a></li>
<li><a href="{{ url('/login') }}">{{ trans('adminlte_lang::message.login') }}</a></li>
@else
<!-- User Account Menu -->
<li class="dropdown user user-menu">
    <!-- Menu Toggle Button -->
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <!-- The user image in the navbar
        <img src="asset('/img/user2-160x160.jpg')" class="user-image" alt="User Image"/>-->
        <!-- hidden-xs hides the username on small devices so only the image appears. -->
        <span>{{ Auth::user()->name }}</span>
    </a>
    <ul class="dropdown-menu">
        
        <li class="user-header">
            <!-- The user image in the menu
            <img src="asset('/img/user2-160x160.jpg')" class="img-circle" alt="User Image" />-->
            <p style="margin-top: 20%;">
                
                <small style="font-size: 20px">
                <i class="fa fa-calendar" style="float:left;"></i>
                <i class="fa fa-clock-o " style="float:right;"></i>
                <b id="hora"></b>
                </small>
            </p>
        </li>
        
        <!-- Menu Footer-->
        <li class="user-footer">
            <div class="pull-left">
                <a href="{{ asset('/perfil_usuario/'.Auth::user()->id)}}" class="btn btn-default btn-float" onclick="perfil();"><i class="fa fa-user"></i> Perfil</a>
            </div>
            <div class="pull-right">
                <a href="{{ url('/logout') }}" class="btn btn-default btn-flat" onclick="cleanToken();"><i class="fa fa-sign-out"></i> {{ trans('adminlte_lang::message.signout') }}</a>
            </div>
        </li>
    </ul>
</li>
@endif
<!-- Control Sidebar Toggle Button
<li>
    <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
</li>-->
</ul>
</div>
</nav>
</header>