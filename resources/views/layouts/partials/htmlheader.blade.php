<head>
    <meta charset="UTF-8">
    <title>Bióetica</title>
    <!--      <link rel="shortcut icon" href="/img/logo.gif" /> -->
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.4 -->
    <link href="{{ asset('/css/bootstrap.css') }}" rel="stylesheet" type="text/css" />
         <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="{{ asset('/css/AdminLTE.css') }}" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link href="{{ asset('/css/skins/skin-yellow.css') }}" rel="stylesheet" type="text/css" />
    <!-- iCheck -->
    <link href="{{ asset('/plugins/iCheck/square/blue.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/plugins/datatables/jquery.dataTables.css') }}" rel="stylesheet" type="text/css" />
    <!-- cargador css  -->
    <link href="{{ asset('/plugins/pace/pace.css ')}}" rel="stylesheet"/>
    <!--   jquery-confirm -->
    <link href="{{ asset('/css/jquery-confirm.css ')}}" rel="stylesheet"/>
    <!-- Ionicons -->
    <link href="{{ asset('/fonts/ionicons.min.css') }}" rel="stylesheet" type="text/css" />
     <!-- Font Awesome Icons -->
    <link href="{{ asset('/fonts/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="{{ asset('/plugins/select2/select2.min.css') }}"  rel="stylesheet">
</head>
