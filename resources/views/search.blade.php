@extends('layouts.landing')
@section('content')
<style>

body { background-color: transparent;
background-image: url(/img/fondos/imagen-fondo-filtros-anahuac.jpg);
background-repeat: no-repeat;
padding-top: 0;}
html {background-color: transparent;}

#myInput {
font-size: 16px;
color: #000;
border: 1px solid #ddd;
height: 46px;
background: rgba(255,255,255, 0.70);
border: 0;
}
#myInputIcon{
background-position: 95% 12px;
background-repeat: no-repeat;
}
#myUL {
list-style-type: none;
padding: 0;
margin: 0;
padding-top: 12px;
}
.list-group-item {
position: relative;
display: block;
padding: 10px 15px;
margin-bottom: -1px;
background-color: #ffffff;

background: rgba(255,255,255, 0.70);
border: 0;
}
#myUL li a.header {
background-color: #e2e2e2;
cursor: default;
border: 0;
}
#myUL li a:hover:not(.header) {
background-color: #eee;
}
/* color a quien busco */
#u680 {
background-color:rgba(224, 172, 83, 0.70);
color:#fff;
opacity: 0.8;
filter: alpha(opacity=60);
border: 0;
}
/* colo ambito que me interesa */
#u681{
background-color: rgba(128, 36, 56, 0.70);
color:#fff;
opacity: 0.8;
filter: alpha(opacity=60);
border: 0;
}
/* color que area de la bioetica busco */
#u682{
background-color: rgba(128,141,84, 0.70);
color:#fff;
opacity: 0.8;
filter: alpha(opacity=60);
border: 0;
}
#mapaIr{
background-color: #CE6E22;
color: #fff;
}
.search{
padding: 6px 12px;
font-size: 14px;
font-weight: normal;
line-height: 1;
color: #000;
text-align: center;
background-color: rgba(0, 0, 0, 0.60);
border: 0;
border-radius: 0;

}
.capaRespuesta{
background-color: rgba(195,195,195, 0.70);
}
</style>
<div class="Fondo">
    <div id="capaRespuesta">
    <div id="preloadDiv" class="text-center" style="position:fixed;top:40%;left:0;right:0;padding-bottom:0;z-index:100;">
    
    <i class="fa fa-spinner fa-pulse fa-3x fa-fw" style="font-size: 50px"></i>
    
</div>
<div class="container">
    <br>
    <div class="col-md-9 col-md-offset-2" style="background: #000;margin-bottom: 20px;" >
        <center><h3 style="color: #fff;"><b>MAPA</b></h3>
        <hr style=" width:70%;"></center>
    </div>
</div>
<div class="container-fluid" style="display: none;" id="contentSection">
    <form id="filtrosdebusqueda" class="filtrosdebusqueda" method="post">
        <input type="hidden" name="_token" id="token"  value="{{csrf_token()}}">
        <div class="col-md-2 col-md-offset-1">
            <div class="panel-group">
                <div class="panel panel-default" id="u680">
                    <a data-toggle="collapse" href="#collapse1" id="dataTogle" class="panel-collapse collapse in" aria-expanded="true" >
                        <div class="panel-heading" style="background: #000; ">
                            <h4 class="panel-title" style="color: white">
                            <center style="padding: 8px" >¿A QUÍEN BUSCO?</center>
                            </h4>
                        </div></a>
                        <div id="collapse1"  class="panel-collapse collapse in" aria-expanded="true" >
                            @foreach($perfil as $p)
                            <div class="panel-body" id="u680">
                                <div class="col-md-9">
                                    <center><b >{{$p->descripcion}}</b></center>
                                </div>
                                <div class="col-md-3"><input type="checkbox"  name="perfil[]"  value="{{$p->id}}" ></div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-md-2">
                <div class="panel-group">
                    <div class="panel panel-default" id="u681">
                        <a data-toggle="collapse" href="#collapse2" id="dataTogle2" class="panel-collapse collapse in" aria-expanded="true" >
                            <div class="panel-heading" style="background: #000; ">
                                <h4 class="panel-title" style="color: white">
                                <center>¿ÁMBITO QUE ME INTERESA?</center>
                                </h4>
                            </div></a>
                            <div id="collapse2"  class="panel-collapse collapse in" aria-expanded="true" >
                                @foreach($ambitos as $a)
                                <div class="panel-body">
                                    <div class="col-md-9">
                                        <center><b >{{$a->descripcion}}</b></center>
                                    </div>
                                    <div class="col-md-3">
                                        <input type="checkbox"  name="ambito[]" value="{{$a->id}}" >
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="panel-group">
                        <div class="panel panel-default" id="u682">
                            <a data-toggle="collapse" href="#collapse3" id="dataTogle3" class="panel-collapse collapse in" aria-expanded="true" >
                                <div class="panel-heading" style="background: #000; ">
                                    <h4 class="panel-title" style="color: white">
                                    <center>¿QUÉ ÁREA DE LA BIÓETICA BUSCO?</center>
                                    </h4>
                                </div>
                            </a>
                                <div id="ocultar">
                                    <div id="collapse3" class="panel-collapse collapse in" aria-expanded="true" >
                                    @foreach($areas as $ar)
                                    <div class="panel-body" >
                                        <div class="col-md-9">
                                            <center><b style="font-size: 14px;">{{$ar->nombre}}</b></center>
                                        </div>
                                        <div class="col-md-3">
                                            <input type="checkbox" name="areas[]" value="{{$ar->nombre}}" >
                                        </div>
                                    </div>
                                    @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        
                        <div class="input-group" style="margin-top: 5px;">
                            <input type="text" id="myInput" onkeyup="myFunction()" placeholder="REPRODUCCIÓN ASISTIDA" title="Busqueda rapida" name="key" class="form-control" autocomplete="off">
                            <span class="input-group-addon search"><img src="/img/search.png" id="myInputIcon"></span>
                        </div>
                        
                        <ul id="myUL" class="list-group">
                 
                            <div class="list-group-item"   style="display: none" id="loading"><center><b>Buscando</b> <i class="fa fa-spinner fa-spin  fa-fw"></i></center></div>
                            @foreach($listCookies as $k => $name)
                            @if($name != 'XSRF-TOKEN' && $name != 'laravel_session')
                            <li class="list-group-item">
                                <a href="/ver/{{str_replace('_',' ',$name)}}/{{$_COOKIE[$name]}}" style="color: #000">{{str_replace('_',' ',$name)}} </a>
                                <span class="badge" style="padding:0;background-color: transparent;color: #000;font-size: 16px" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fa fa-close btn" onclick="eliminarCookie('{{$name}}');" style="padding:0;background-color: transparent;color: #000;font-size: 16px"></i></span>
                            </li>
                            
                            @endif
                            @endforeach
                            @if($numCookie > 0)
                            <li class="list-group-item"><a href=""  id="limpiarCookies" style="color: #000"><p align="right" style="margin: 0">Limpiar&nbsp;&nbsp;<i class="fa fa-trash"></i></p></a></li>
                            @endif
                        </ul>
                        
                    </div>
                    <div class="col-md-1">
                        
                        <button type="submit" class="btn btn-float btn-lg " id="mapaIr" style="border-radius: 0;margin-top: 5px;">IR</button>
                        
                    </div>
                </form>
            </div>
            <div class="container" style="display: none;" id="resultados">
                
                <div class="col-md-8">
                    <div class="row">
                        <br>
                        <div class="col-md-12" >
                            <div class="panel panel-default">
                                <!-- Default panel contents -->
                                <div class="panel-heading" style="background-color:#000;color: #fff"><center>LISTA DE RESULTADOS</center></div>
                                
                                <!-- Default panel contents -->
                                <div class="panel-heading" style="background:#C3C2C2;color: #000"><center>FILTROS APLICADOS</center></div>
                                
                                <div class="panel-body" id="datosSearch">
                                    <div class="nohaydatos"></div>
                                    <div class="result" > </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-1"><br>
                    <button id="mostrar" class="btn" style="background: transparent;float: left;position: relative;top:-27px;padding-bottom: 0"><i class="fa fa-close fa-3x"></i> </button>
                </div>
                <div class="col-md-3">
                    <br>
                    <table>
                        @foreach($areaslic as $li)
                        <tr>
                            <th><a style="float: right;font-size: 16px;font-size: normal;color: #000;margin-bottom: 10px">{{$li->acronimo}} <i class="fa fa-circle" style="color: #{{$li->color}};"></i></th></a>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
</div>
</div>

@endsection