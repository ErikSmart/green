<link type="text/css" rel="stylesheet" href="{{asset('/plugins/select2/select2.css')}}">
@if(isset($lic)&&!empty($lic))

	<div class="row">
	<form id="form_updateLic" class="form updateLIC" action="{{url('/updateLic')}}" method="post">
		
		<input type="hidden" name="_token" value="{{csrf_token()}}" id="token"/>
		<input type="hidden" name="id" value="{{$lic[0]->id}}" required />
		<div class="text-center">
			<strong>Selecciona en que Grupo quieres que se visualice la Formación</strong>
		</div>	
		
		<div class="container" style="width: 100%;">	
			<div class="form-group">
				<div class="col-sm-6">
				  	<fieldset>
						@foreach($areasLic as $alic)
						<div class="form-group">
							@if( $lic[0]->areasLic == $alic->id)
							<input type="radio" name="areasLic" class="area" value="{{$alic->id}}" @if( $lic[0]->mapView ==1) disabled="true" @endif data-color="{{$alic->color}}" checked="">&nbsp;&nbsp;{{$alic->nombreArea}}
							@elseif( $lic[0]->areasLic != $alic->id)
							<input type="radio" name="areasLic" class="area" value="{{$alic->id}}" @if( $lic[0]->mapView ==1) disabled="true" @endif data-color="{{$alic->color}}" >&nbsp;&nbsp;{{$alic->nombreArea}}
							@endif
						</div>
						@endforeach
					</fieldset>
				</div>
		
			</div>
		</div> 
		
		<div class="container" style="width: 100%;">
			<div class="col-sm-12">
				<div class="form-group">
					<label>Nombre Formación Lic.</label>
					<input type="text" name="title" class="form-control titleLic"  value="{{$lic[0]->title}}" required />
				</div>
			</div>
			<div class="col-sm-12">
				<label>Color</label>
				<div class="input-group">
					<input type="text" name="color" class="form-control color" id="codColor"  value="{{$lic[0]->color}}" required="" />
					<span class="input-group-addon" style="padding: 0">
					<div class="btn-group dropup">
             
                 <button class="btn dropdown-toggle" data-toggle="dropdown" id="backColor" style="background:{{$lic[0]->color}}; ">
					    <span style="padding-left: 2rem;padding-right: 2rem;color:white;"><i class="fa fa-paint-brush fa-lg" aria-hidden="true"></i></span>
					  </button>
					  <ul class="dropdown-menu" style="padding: 10px;">
					    @foreach($areasLic as $alic)
						<div class="form-group">
							
							<input type="radio" name="colorCheck" class="area"  data-color="{{$alic->color}}" checked="">&nbsp;&nbsp;Cambiar<div style="background:#{{$alic->color}};width: 20px;height: 20px;float: right;"></div>
							
							
						</div>
						@endforeach
					  </ul>
              </div>
					</span>
				</div>
				<br>
			</div>
		</div>
		<div class="container" style="width: 100%;">
		@if( $lic[0]->mapView == 0) 

		<div class="col-sm-12 text-center">
			<label>Agrupar en Sección OTROS </label>
			<center>
				<div class="input-group">
					@if($lic[0]->otros == 1)
					<input type="radio" name="otros" value="1" checked=""> SI &nbsp;
					<input type="radio" name="otros" value="0" > NO
					@else
					<input type="radio" name="otros" value="1" > SI &nbsp;
					<input type="radio" name="otros" value="0" checked=""> NO
					@endif
				</div>
			</center>
			<br><br>
		</div>

		@endif
			
			
			<div class="col-sm-12">
				
				<center id="guardando" style="display: none;">
				<p>Guardando... <i class="fa fa-spinner fa-spin fa-2x fa-fw"></i></p>
				</center>
				<button type="submit" class="btn btn-block btn-success ink-reaction" id="guardar">Guardar cambios</button>
			</div>
		</div>
	</form>
</div>

<script src="{{asset('/plugins/select2/select2.js')}}"></script>
<script>
	$('[data-toggle="tooltip"]').tooltip();
	$(function () {
		$('input').iCheck({
			checkboxClass: 'icheckbox_square-blue',
			radioClass: 'iradio_square-blue',
			increaseArea: '20%' // optional
		});
	});

	$(".area").on('ifClicked', function() {
	    color = $(this).attr('data-color');
	    $("#codColor").val('#'+color);
	    $("#backColor").css('background-color', '#'+color);
		//alert('#'+color);
	});
	
</script>
@endif