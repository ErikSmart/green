<div id="validarCodigo">
    <div class="form-group">
        <label>Ingresa el C&oacute;digo que se te envi&oacute; al correo</label>
        <input type="text" class="form-control" placeholder="Codigo" id="Codigo" autocomplete="off" oncopy="return false;" onpaste="return false;" oncut="return false;">
    </div>
</div>
<div id="ingresarCodigo" style="display: none;">
    <form id="password_restar" action="{{ url('/password/restar') }}" class="form_perfil" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" id="idusuario" name="user">
        <div class="col-md-12">
            <div class="form-group">
                <label>Nueva Contraseña</label>
                <input type="password" class="form-control" name="password" placeholder="****************" id="newpassword">
            </div>
            <p id="msj_pass1" style="color:#a94442"></p>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label>Repetir Contraseña</label>
                <input type="password" class="form-control" name="password_confirmation"  placeholder="****************" id="password_confirmation">
            </div>
            <p id="msj_pass" style="color:#a94442"></p>
        </div>
        <div class="col-md-12">
            <button class="btn btn-success btn-block" id="subContra">Actualizar</button>
        </div>
        <div class="col-xs-2">
        </div>
    </div>
</form>
</div>
<script>
$("#Codigo").keyup(function(event) {
    codigo = $("#Codigo").val();
    $.get('/verifcarCod/'+codigo+'', function(response) {
        for (i = 0; i < response.length; i++) {      
            if (response[i].codigo) {
                $("#validarCodigo").hide();
                $("#ingresarCodigo").show();
                $("#idusuario").val(response[i].id);
                reset(2);
                $("#form_resetar").show(); 
               $("#correoenviado").hide();
            }
        }
    });
});
 $('#newpassword').keyup(function(event) {
        var pass1 =$('#newpassword').val();

      if(pass1.length < 6) {  
          $("#msj_pass1").empty().append("El m&iacute;nimo es 6 car&aacute;cteres");  
          $("#subContra").attr('disabled', true);
          
        } else{
          $("#msj_pass1").empty();  
          $("#subContra").attr('disabled', false);

        }
    });
    $('#password_confirmation').keyup(function(event) {
        var pass1 =$('#newpassword').val();
        var pass2 =$('#password_confirmation').val();

        if(pass2.length < 6) {  
          $("#msj_pass").empty().append("El m&iacute;nimo es 6 car&aacute;cteres");  
            
        } else{
          $("#msj_pass").empty();  

        }

        if (pass1 == pass2) {
          $("#subContra").attr('disabled', false);
        $("#msj_pass").empty();

        }else{
          $("#subContra").attr('disabled', true);
        $("#msj_pass").empty().append('La contraseña no coincide');
        }
      });


$(document).on("submit",".form_perfil",function(e){
//funcion para atrapar los formularios y enviar los datos
e.preventDefault();
         
var formu=$(this);
var quien=$(this).attr("id");//quien es el formulario de donde provienen los datos
//se declara el id del formulario con su ruta 
if(quien=="password_restar"){ var miurl="/password/restar";  var pass1 = $('#newpassword').val(); var pass2 = $('#password_confirmation').val();}
  if (pass1 != '' && pass2 != '' && pass1 == pass2 ) {
      $.ajax({
          type: "POST",
          url : miurl,
          data : formu.serialize(),
          success : function(response){
               if (response == 1) {
                  $.confirm({
                      icon:'glyphicon glyphicon-ok text-green',
                      title: 'Datos Actualizados',
                      content: '<center><h3>La actualizaci&oacute;n de tus datos se realizo correctamente</h3></center>',
                      confirmButtonClass: 'btn-success',
                      confirmButton: 'Confirmar',
                      cancelButton: false,
                      confirm: function () {
                        location.reload(true);
                       } 
                    });
               }
            }, //si ha ocurrido un error
          error: function(response){  
                $.confirm({
                  icon: 'fa fa-warning',
                  title:'Hoops!',
                  content:'Erro al cargar datos',
                  confirmButton: 'Confirmar',
                  cancelButton: false,
                  animation: 'zoom', confirmButtonClass: 'btn-info',
                });
          }
    });
  }else{
    $("#subContra").attr('disabled', true);
    $.dialog({title:false,content:'<cener><h2>Completar campos</h2></center>'});
  }
})

</script>