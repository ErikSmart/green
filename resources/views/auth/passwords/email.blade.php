@extends('layouts.auth')
@section('htmlheader_title')
    Password recovery
@endsection
@section('content')
<body class="login-page">
    <div class="login-box">
        <div class="login-logo">
            <a href="{{ url('/') }}"><b>Bioética</b></a>
        </div><!-- /.login-logo -->

        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> {{ trans('adminlte_lang::message.someproblems') }}<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="login-box-body">
            <div id="form_resetar" >
                <p class="login-box-msg">Envio de Código</p>
                <form id="for_passwordReset" class="reset_pass" action="{{ url('/passwordReset') }}" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div>
                        <div class="form-group has-feedback">
                        <input type="email" class="form-control" placeholder="Ingresa tu Email primario" name="email" value="{{ old('email') }}" required="" autocomplete="off" id="validEmail" />
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    </div>
                    <p id="msj_mailTPS" style="color:#a94442"></p>
                    </div>
                    
                    <div class="row">
                      
                        <div class="col-xs-12">
                            <button type="submit" class="btn btn-primary btn-block btn-flat" id="searchMail" disabled="true">Enviar el c&oacute;digo para restablecer la contrase&ntilde;a</button>
                        </div><!-- /.col -->
                  
                    </div>
                </form>
            </div>

            <div id="correoenviado" style="display: none;"><center><b>Enviando..</b><img src="https://digitalsynopsis.com/wp-content/uploads/2015/10/gif-icons-menu-transition-animations-sent.gif" alt="" width="150"></center></div>
        </div><!-- /.login-box-body -->
    </div>

@include('layouts.partials.scripts_auth')

<script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });
        $(document).ready(function() {
             $('#validEmail').keyup(function(event) {
                    if (event.target.value == '') {
                        $("#searchMail").attr('disabled', true);
                        $("#msj_mailTPS").empty();
                    }
                        $.get("/mail/"+event.target.value+"/Usuario",function(response){
                                
                        for (i = 0; i < response.length; i++) {
                            
                            if (response[i].correo) {
                                $("#msj_mailTPS").empty();
                                $("#searchMail").attr('disabled', false);
                            }
                            if(!response.length){
                                $("#msj_mailTPS").empty().append('Correo inexistente');  
                                $("#searchMail").attr('disabled', true);
                            }
                        }  
                       
                     }); 
                    
                   
                    
                }); 
        });
        $(document).on("submit",".reset_pass",function(e){
        //funcion para atrapar los formularios y enviar los datos

                e.preventDefault();
                var formu=$(this);
                var quien=$(this).attr("id");   
                if(quien=="for_passwordReset"){ var miurl="/passwordReset";  }
                 $.ajax({
                    url: miurl,
                    type:"POST",
                    data:formu.serialize(),
                    beforeSend: function(){
                     $("#correoenviado").show();
                     $("#form_resetar").hide();
                    },
                    success : function(response,data){
                        id = response.data[0].id;
                        $("#idusuario").val(id);

                        view(1);
                        $("#form_resetar").show(); 
                          $("#correoenviado").hide();
                      },
                      error:function(){
                          $("#form_resetar").show(); 
                          $("#correoenviado").hide();
                      }
                  });
        })

        function view(cod){
                
              if (cod == 1) { var time ='cancel|120000'; var cancelar='$("#form_resetar").show();$("#correoenviado").hide();alert("Se excedió el limite de tiempo para ingresar el codigo")';}
            if (cod == 2) {var time =false; var cancelar=location.reload(true);}
            $.confirm({
                title: 'Validar Código',
                content:'url:/passReset',
                animation: 'zoom',
                confirmButton: false,
                cancelButton: 'Cancelar',
                autoClose: time,
                cancel:function(){
                    cancelar
                }
            }); 

             
           
           
        }


    </script>
</body>

@endsection
