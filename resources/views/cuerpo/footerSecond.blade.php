<hr class="yellow-line">
<footer class="footer">
  <div class="container">
    <div class="row">
      <div class="col-md-4 col-sm-4">
        <!-- Por Catalán -->
        <div>
          <div id="block-footerlogo" class="block block-block-content">
            <div class="content">
              <div>
                <p>
                  <a href="http://pegaso.anahuac.mx/radio/" target="_blank"><img alt="" src="{{ asset('img/redio-img.png') }}"></a>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-4 col-sm-4 bodre-right bodre-left footer-col">
        <div>
          <div id="block-campusnorthe" class="block block-block-content">
            <div class="content">
              <div>
                <h4>CAMPUS NORTE</h4>
                <p>Av. Universidad Anáhuac 46, Col. Lomas Anáhuac</p>
                <p>Huixquilucan, Estado de México, C.P. 52786.</p>
                <p>Conmutador: <strong><a href="tel:+525556270210">+52 (55) 5627 0210</a></strong></p>
                <p>Del interior sin costo: <strong><a href="tel:+525556270210">+52 (55) 5627 0210</a></strong></p>
                <p><a href="http://www.anahuac.mx/mexico/Avisodeprivacidad/proteccion-de-datos-personales-campus-norte" style="color:#F76F0B; "><strong>Aviso de privacidad.</strong></a></p>
                <p>&nbsp;</p>
                <div class="row">
                  <div class="col-md-4 col-sm-4"><span><strong>SÍGUENOS EN:</strong> </span></div>
                  <div class="col-md-8 col-sm-8">
                    <div class="social-icons">
                      <div>
                        <ul>
                          <li>
                          <a class="addthis_button_facebook" href="https://www.facebook.com/UniversidadAnahuac/"><img alt="Facebook"  src="{{ asset('img/facebook.png') }}"></a></li>
                          <li>
                          <a class="addthis_button_twitter" href="https://twitter.com/anahuac"><img alt="Twitter"   src="{{ asset('img/twitter.png') }}"></a>
                          </li>
                          <li>
                          <a class="addthis_button_instagram" href="https://www.instagram.com/uanahuacnorte/"><img alt="Instagram"  src="{{ asset('img/instagram.png') }}"></a>
                          </li>
                          <li
                          ><a class="addthis_button_youtube" href="https://www.youtube.com/universidadanahuac"><img alt="Youtube"  src="{{ asset('img/youtube.png') }}"></a>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-4 col-sm-4 footer-col">
        <div>
          <div id="block-campussur" class="block block-block-content">
            <div class="content">
              <div>
                <h4>CAMPUS SUR</h4>
                <p>Av. de las Torres 131, Col. Olivar de los Padres</p>
                <p>Ciudad de México. C.P. 01780</p>
                <p>Conmutador: <strong><a href="tel:52555628-8800">+52 (55) 5628 8800</a></strong></p>
                <p><a href="http://www.anahuac.mx/mexico/Avisodeprivacidad/proteccion-de-datos-personales-campus-sur" onclick="ga(&#39;uam.send&#39;, &#39;event&#39;, &#39;Footer&#39;, &#39;click&#39;, &#39;Aviso de privacidad sur&#39;);" style="color:#F76F0B; "><strong>Aviso de privacidad.</strong></a></p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <div class="row">
                  <div class="col-md-4 col-sm-4"><span><strong>SÍGUENOS EN:</strong> </span></div>
                  <div class="col-md-8 col-sm-4">
                    <div class="social-icons">
                      <div class="addthis_toolbox addthis_default_style addthis_32x32_style addthis_custom_sharing">
                        <ul>
                          <li>
                          <a class="addthis_button_facebook" href="https://www.facebook.com/AnahuacSur/"><img alt="Facebook"  src="{{ asset('img/facebook.png') }} "></a>
                          </li>
                          <li>
                          <a class="addthis_button_twitter" href="https://twitter.com/AnahuacSur"><img alt="Twitter"  src="{{ asset('img/twitter_0.png') }}"></a>
                          </li>
                          <li>
                          <a class="addthis_button_instagram" href="https://www.instagram.com/uanahuacsur/"><img alt="Instagram"  src="{{ asset('img/instagram.png') }}">
                          </a></li>
                          <li>
                          <a class="addthis_button_youtube" href="https://www.youtube.com/universidadanahuac"><img alt="Youtube"  src="{{ asset('img/youtube.png') }}">
                          </a></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row" style="margin-top:50px;">
      <div class="col-lg-12 col-sm-12 bottom-footer">
        <div>
          <div id="block-footercopyright" class="block block-block-content">
            <div class="content">
              <div>
                <div class="logos-footer">
                  <img alt="Fimpes" src="{{ asset('img/fimpes_0.png') }}" class="align-center">&nbsp;
                  <img alt="ANUIES" src="{{ asset('img/anuies_0.png') }}" class="align-center">
                  <img alt="SEP" class="align-center" src="{{ asset('img/sep_0.png') }}">&nbsp;
                  <img alt="Empresa Socialmente Responsable" src="{{ asset('img/esr.png') }}" class="align-center">
                  <img alt="Empresa Familiarmente Responsable" src="{{ asset('img/efr.png') }}" class="align-center">
                  <p>&nbsp;</p>
                  <h3>Universidad Anáhuac 2017. Todos los Derechos Reservados.</h3>
                  <p>&nbsp;</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>