		
@if($actual[0]->status == 'Activo')
<div class="container-fluid">

		<div class="col-xs-12"  style="background: black;">
			<p style="color: white;font-size: 18px;margin-bottom: 0;padding: 10px;">{{$actual[0]->nombreCompleto}}</p>
		</div>
		<div class="col-xs-12" style="background: #fff;">
			<table width="100">
				<tr>
					<th style="padding-top: 15px">&nbsp;&nbsp;ACTIVO</th>
					<td><i class="fa fa-check" style="font-size: 25px;color: #B87708;position: relative;"></i></td>
				</tr>
				<tr>
					<th>&nbsp;&nbsp;INACTIVO</th>
					<td><i class="fa fa-minus" style="font-size: 25px;color: #81ADB9;position: relative;padding-bottom: 15px;top: 8px;"></i></td>
				</tr>
			</table>
		</div>
		<!-- Gestion -->
		<div class="col-xs-12" style="background: #424242;">
			<p style="color: #fff;padding-bottom: 5px;color: white;font-size: 18px;margin-bottom: 0">GESTIÓN</p>
		</div>
		<div class="col-xs-12" style="background: #fff;">
			<table>
				<tr>
					<td>@if(!empty($actual[0]->rgestion))
						@if($actual[0]->sg == 'Activo')
						<i class="fa fa-check" style="font-size: 25px;color: #B87708;padding-right: 6px;"></i>
						@elseif($actual[0]->sg == 'Inactivo')
						<i class="fa fa-minus" style="font-size: 25px;color: #81ADB9;padding-right: 6px;"></i>
						@else
						<i class="fa fa-times" style="font-size: 25px;color: transparent;padding-right: 6px;"></i>
						@endif
						@endif
						
					</td>
					<th style="padding-bottom: 15px;"><p style="margin-bottom: -5px;">{!!$actual[0]->rgestion!!}</p></th>
				</tr>
			</table>
		</div>
		<!-- end Gestion -->
		<!-- DOCENCIA -->
		<div class="col-xs-12" style="background: #424242;">
			<p style="color: #fff;padding-bottom: 5px;color: white;font-size: 18px;margin-bottom: 0">DOCENCIA</p>
		</div>
		<div class="col-xs-12" style="background: #fff;">
			<table>
				<tr>
					<td>
						@if(!empty($actual[0]->rdocencia))
						@if($actual[0]->sd == 'Activo')
						<i class="fa fa-check" style="font-size: 25px;color: #B87708;padding-right: 6px;"></i>
						@elseif($actual[0]->sd == 'Inactivo')
						<i class="fa fa-minus" style="font-size: 25px;color: #81ADB9;padding-right: 6px;"></i>
						@else
						<i class="fa fa-times" style="font-size: 25px;color: transparent;padding-right: 6px;"></i>
				
						@endif
					@endif</td>
					<th style="padding-bottom: 15px;">{!!$actual[0]->rdocencia!!}</th>
				</tr>
			</table>
		</div>
		<!-- end DOCENCIA -->
		<!-- INVESTIGACIÓN -->
		<div class="col-xs-12" style="background: #424242;">
			<p style="color: #fff;padding-bottom: 5px;color: white;font-size: 18px;margin-bottom: 0">INVESTIGACIÓN</p>
		</div>
		<div class="col-xs-12" style="background: #fff;">
			<table>
				<tr>
					<td >
						@if(!empty($actual[0]->investigacion_temas))
						@if($actual[0]->si == 'Activo')
						<i class="fa fa-check" style="font-size: 25px;color: #B87708;padding-right: 6px;"></i>
						@elseif($actual[0]->si == 'Inactivo')
						<i class="fa fa-minus" style="font-size: 25px;color: #81ADB9;padding-right: 6px;"></i>
						@else
						<i class="fa fa-times" style="font-size: 25px;color: transparent;padding-right: 6px;"></i>
			
						@endif
						@endif
					</td>
					<th style="padding-bottom: 15px;">{{$actual[0]->investigacion_temas}}</th>
				</tr>
			</table>
		</div>
		<!-- end INVESTIGACIÓN -->
		<div class="col-xs-12" style="background: {{$actual[0]->color}};">
			<p style="color: #fff;padding-bottom: 5px;color: white;font-size: 18px;margin-bottom: 0">PERFIL DE INFORMACI&Oacute;N</p>
		</div>
		<div class="col-xs-12" style="background: #fff;background: #424242;color: #fff;padding: 0">
			<table style="width: 100%;" class="table-striped">
				<tr>
					<th>
						<p style="padding-left: 20px;margin:0">{{$actual[0]->formAcademicLic}}</p>
					</th>
				</tr>
				<tr>
					<th >
						<p style="padding-left: 20px;margin:0">{{$actual[0]->formdeMaestria}}</p>
					</th>
				</tr>
				<tr>
					<th>
						<p style="padding-left: 20px;margin:0">{{$actual[0]->formEspecialidad}}</p>
					</th>
				</tr>
				<tr>
					<th >
						<p style="padding-left: 20px;margin:0">{{$actual[0]->formDoctorado}}</p>
					</th>
				</tr>
				<tr>
					<th>
						<p style="padding-left: 20px;margin:0">{{$actual[0]->formPostGrado}}</p>
					</th>
				</tr>
			</table>
		</div>
		<!-- contacto -->
		<div class="col-xs-12" style="background: {{$actual[0]->color}};">
			<p style="color: #fff;padding-bottom: 5px;color: white;font-size: 18px;margin-bottom: 0">INFORMACIÓN DE VINCULACIÓN Y CONTACTO</p>
			<br>
			<center>
			<img src="/img/movil.png" width="25">
			<p style="margin-bottom: 25px;color: white;font-weight: 700">{{$actual[0]->telefono}}</p>
			<img src="/img/mail.png" alt="">
			<p style="margin-bottom: 25px;color: white;font-weight: 700">{{$actual[0]->email}}</p>
						<div class="divSocial">
					
					<div class="fb-share-button"
						data-href="{{url('ver', [$actual[0]->nombreCompleto,$actual[0]->idV])}}"
						data-layout="button_count">
					</div>
					<a
						href="https://twitter.com/share"
						class="twitter-share-button"
						data-show-count="false"
					data-url="{{url('ver', [$actual[0]->nombreCompleto,$actual[0]->idV])}}">Tweet</a>
					<div class="g-plus" 
					data-action="share" 
					data-href="{{url('ver', [$actual[0]->nombreCompleto,$actual[0]->idV])}}">
					</div>
				</div>
			</center>
		</div>
</div>
@else
<div class="col-sm-12 col-xs-12">
	<h1 align="center" style="position: relative;top: 40%">Usuario temporalmente dado de baja <i class="fa fa-thumbs-o-down" aria-hidden="true"></i></h1>
</div>

@endif
@foreach($datos as $re)
	@if($actual[0]->nombreCompleto != $re->nombreCompleto && $re->status == 'Activo')
<div class="container-fluid">
	
		<div class="col-xs-12"  style="background: black;">
			<p style="color: white;font-size: 18px;margin-bottom: 0;padding: 10px;">{{$re->nombreCompleto}}</p>
		</div>
		<div class="col-xs-12" style="background: #fff;">
			<table width="100">
				<tr>
					<th style="padding-top: 15px">&nbsp;&nbsp;ACTIVO</th>
					<td><i class="fa fa-check" style="font-size: 25px;color: #B87708;position: relative;"></i></td>
				</tr>
				<tr>
					<th>&nbsp;&nbsp;INACTIVO</th>
					<td><i class="fa fa-minus" style="font-size: 25px;color: #81ADB9;position: relative;padding-bottom: 15px;top: 8px;"></i></td>
				</tr>
			</table>
		</div>
		<!-- Gestion -->
		<div class="col-xs-12" style="background: #424242;">
			<p style="color: #fff;padding-bottom: 5px;color: white;font-size: 18px;margin-bottom: 0">GESTIÓN</p>
		</div>
		<div class="col-xs-12" style="background: #fff;">
			<table>
				<tr>
					<td>@if(!empty($re->rgestion))
						@if($re->sg == 'Activo')
						<i class="fa fa-check" style="font-size: 25px;color: #B87708;padding-right: 6px;"></i>
						@elseif($re->sg == 'Inactivo')
						<i class="fa fa-minus" style="font-size: 25px;color: #81ADB9;padding-right: 6px;"></i>
						@else
						<i class="fa fa-times" style="font-size: 25px;color: transparent;padding-right: 6px;"></i>

						@endif
						@endif
						
					</td>
					<th style="padding-bottom: 15px"><p style="margin-bottom: -5px">{!!$re->rgestion!!}</p></th>
				</tr>
			</table>
		</div>
		<!-- end Gestion -->
		<!-- DOCENCIA -->
		<div class="col-xs-12" style="background: #424242;">
			<p style="color: #fff;padding-bottom: 5px;color: white;font-size: 18px;margin-bottom: 0">DOCENCIA</p>
		</div>
		<div class="col-xs-12" style="background: #fff;">
			<table>
				<tr>
					<td>
						@if(!empty($re->rdocencia))
						@if($re->sd == 'Activo')
						<i class="fa fa-check" style="font-size: 25px;color: #B87708;padding-right: 6px;"></i>
						@elseif($re->sd == 'Inactivo')
						<i class="fa fa-minus" style="font-size: 25px;color: #81ADB9;padding-right: 6px;"></i>
						@else
						<i class="fa fa-times" style="font-size: 25px;color: transparent;padding-right: 6px;"></i>

						@endif
					@endif</td>
					<th style="padding-bottom: 15px;">{!!$re->rdocencia!!}</th>
				</tr>
			</table>
		</div>
		<!-- end DOCENCIA -->
		<!-- INVESTIGACIÓN -->
		<div class="col-xs-12" style="background: #424242;">
			<p style="color: #fff;padding-bottom: 5px;color: white;font-size: 18px;margin-bottom: 0">INVESTIGACIÓN</p>
		</div>
		<div class="col-xs-12" style="background: #fff;">
			<table>
				<tr>
					<td >
						@if(!empty($re->investigacion_temas))
						@if($re->si == 'Activo')
						<i class="fa fa-check" style="font-size: 25px;color: #B87708;padding-right: 6px;"></i>
						@elseif($re->si == 'Inactivo')
						<i class="fa fa-minus" style="font-size: 25px;color: #81ADB9;padding-right: 6px;"></i>
						@else
						<i class="fa fa-times" style="font-size: 25px;color: transparent;padding-right: 6px;"></i>
				
						@endif
					@endif</td>
					<th style="padding-bottom: 15px;">{{$re->investigacion_temas}}</th>
				</tr>
			</table>
		</div>
		<!-- end INVESTIGACIÓN -->
		<div class="col-xs-12" style="background: {{$re->color}};">
			<p style="color: #fff;padding-bottom: 5px;color: white;font-size: 18px;margin-bottom: 0">PERFIL DE INFORMACI&Oacute;N</p>
		</div>
		<div class="col-xs-12" style="background: #fff;background: #424242;color: #fff;padding: 0">
			<table style="width: 100%;" class="table-striped">
				<tr>
					<th>
						<p style="padding-left: 20px;margin:0">{{$re->formAcademicLic}}</p>
					</th>
				</tr>
				<tr>
					<th >
						<p style="padding-left: 20px;margin:0">{{$re->formdeMaestria}}</p>
					</th>
				</tr>
				<tr>
					<th>
						<p style="padding-left: 20px;margin:0">{{$re->formEspecialidad}}</p>
					</th>
				</tr>
				<tr>
					<th >
						<p style="padding-left: 20px;margin:0">{{$re->formDoctorado}}</p>
					</th>
				</tr>
				<tr>
					<th>
						<p style="padding-left: 20px;margin:0">{{$re->formPostGrado}}</p>
					</th>
				</tr>
			</table>
		</div>
		<!-- contacto -->
		<div class="col-xs-12" style="background: {{$re->color}};">
			<p style="color: #fff;padding-bottom: 5px;color: white;font-size: 18px;margin-bottom: 0">INFORMACIÓN DE VINCULACIÓN Y CONTACTO</p>
			<br>
			<center>
			<img src="/img/movil.png" width="25">
			<p style="margin-bottom: 25px;color: white;font-weight: 700">{{$re->telefono}}</p>
			<img src="/img/mail.png" alt="">
			<p style="margin-bottom: 25px;color: white;font-weight: 700">{{$re->email}}</p>
			<div class="divSocial">
					
					<div class="fb-share-button"
						data-href="{{url('ver', [$re->nombreCompleto,$re->idV])}}"
						data-layout="button_count">
					</div>
					<a
						href="https://twitter.com/share"
						class="twitter-share-button"
						data-show-count="false"
					data-url="{{url('ver', [$re->nombreCompleto,$re->idV])}}">Tweet</a>
					<div class="g-plus" 
					data-action="share" 
					data-href="{{url('ver', [$re->nombreCompleto,$re->idV])}}">
					</div>
				</div>
			</center>
		</div>
		</div>
		@elseif($actual[0]->nombreCompleto != $re->nombreCompleto && $re->status == 'Inactivo')
		<div class="col-sm-12 col-xs-12">
			<h1 align="center" style="position: relative;top: 40%">Usuario temporalmente dado de baja <i class="fa fa-thumbs-o-down" aria-hidden="true"></i></h1>
		</div>
		@endif

		@endforeach

		
