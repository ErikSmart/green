@extends('layouts.landing')
@section('content')
<link rel="stylesheet" href="{{asset('/css/inicio.css')}}">
<style>
	.menuShow{display: none;}
</style>
@include('cuerpo.footerRelativo')
<body class="show-resultados">
	<div class="col-lg-8 col-md-10 col-lg-offset-2 col-md-offset-1 " style="padding-top: 7%;padding-bottom: 6%;padding-right: 1px;">
		
		<div class="col-xs-12">
		
			@if(count($datos) > 1)
			<center class="hidden-lg"><b style="color:black">Desliza para ver mas</b>&nbsp;<img src="http://icon-icons.com/icons2/631/PNG/512/swipe-right-with-three-fingers-of-a-hand-stroke-gesture-symbol_icon-icons.com_57966.png" width="50" height="50"></center>
			@endif
		
			<a href="{{url('/mapa')}}"><i class="fa fa-close fa-2x volvermapa"></i></a>
		</div>
		@if(isset($actual) && !empty($actual))
		<div class="col-lg-12  col-sm-12 col-xs-12 hidden-sm hidden-xs center slider" style="padding: 0">
			
			@include('cuerpo.slidershow')
		</div>
		<div class="col-lg-12  col-sm-12 col-xs-12 hidden-lg hidden-md center slider" style="padding: 0">

			@include('cuerpo.showdetails')
		</div>
		@else
		<div style="height: 360px;">
		<h1 align="center" style="position: relative;top: 40%">Datos de usuario no encontrados <i class="fa fa-warning" aria-hidden="true"></i></h1>
		</div>
		@endif
	</div>
</body>
@endsection