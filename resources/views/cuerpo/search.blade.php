@extends('layouts.landing')
@section('content')
<link rel="stylesheet"  href="{{asset('/css/search.css')}}">
<link rel="stylesheet"  href="{{asset('/js/mapa/jquery_svg/jquery-svg.css')}}" />
<link rel="stylesheet"  href="{{asset('/js/mapa/jquery_svg/jquery.svg.css')}}" />
<link rel="stylesheet"  href="{{asset('/js/mapa/powertip/jquery.powertip.css')}}" />
@include('cuerpo.footerRelativo')
<div class="row Fondo">
    <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 col-lg-offset-0 col-sm-offset-1"   id="contentSection" style="margin-top: 5px;">
        <div class="filstrosDelante">
                <form id="filtrosdebusqueda" class="filtrosdebusqueda" method="post">
                    <input type="hidden" name="_token" id="token"  value="{{csrf_token()}}">
                    <div class="col-lg-3 col-md-3 col-sm-10" style="padding-right: 1px;padding-left: 1px;z-index: 50;">
                        <div class="panel-group">
                            <div class="panel panel-default" id="u680">
                                <a data-toggle="collapse" href="#collapse1" id="dataTogle"  class="panel-collapse collapse in" aria-expanded="true">
                                    <div class="panel-heading" style="background: #000; ">
                                        <h4 class="panel-title" style="color: white">
                                        <center style="padding: 8px" >¿A QUÍEN BUSCO?</center>
                                        </h4>
                                    </div>
                                </a>
                                <div id="collapse1"  class="panel-collapse collapse" aria-expanded="false" >
                                    @foreach($perfil as $p)
                                    <div class=" panel-body cambiar listbusco">
                                        <div class="col-lg-9 col-md-10 col-sm-10 col-xs-10">
                                            <center><b >{{$p->descripcion}}</b></center>
                                        </div>
                                        <div class="col-lg-3 col-md-2 col-sm-2 col-xs-1">
                                        <input type="checkbox"  name="perfil[]"  value="{{$p->id}}" ></div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-lg-3 col-md-3 col-sm-10" style="z-index: 50;padding-right: 0;padding-left: 1px;">
                        <div class="panel-group">
                            <div class="panel panel-default" id="u681">
                                <a data-toggle="collapse" href="#collapse2" id="dataTogle2"  class="panel-collapse collapse in" aria-expanded="true" >
                                    <div class="panel-heading" style="background: #000; ">
                                        <h4 class="panel-title" style="color: white">
                                        <center style="padding: 8px" >¿ÁMBITO QUE ME INTERESA?</center>
                                        </h4>
                                    </div>
                                </a>
                                <div id="collapse2"   class="panel-collapse collapse" aria-expanded="false" >
                                    @foreach($ambitos as $a)
                                    <div class="panel-body cambiar listambito">
                                        <div class="col-lg-9 col-md-10 col-sm-10 col-xs-10">
                                            <center><b >{{$a->descripcion}}</b></center>
                                        </div>
                                        <div class="col-lg-3 col-md-2 col-sm-2 col-xs-1">
                                            <input type="checkbox"  name="ambito[]" value="{{$a->descripcion}}" >
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-10" style="padding-right: 1px;padding-left: 1px;z-index: 50;">
                        <div class="panel-group">
                            <div class="panel panel-default" id="u682">
                                <a data-toggle="collapse" href="#collapse3" id="dataTogle3" class="panel-collapse collapse in" aria-expanded="true" >
                                    <div class="panel-heading" style="background: #000; ">
                                        <h4 class="panel-title" style="color: white">
                                        <center style="padding: 8px" >¿QUÉ ÁREA DE LA BIÓETICA BUSCO?</center>
                                        </h4>
                                    </div>
                                </a>
                                
                                <div id="collapse3"  class="panel-collapse collapse" aria-expanded="false" >
                                    @foreach($areas as $ar)
                                    <div class="panel-body cambiar listarea">
                                        <div class="col-lg-9 col-md-10 col-sm-10 col-xs-10">
                                            <center><b style="font-size: 14px;">{{$ar->nombre}}</b></center>
                                        </div>
                                        <div class="col-lg-3 col-md-2 col-sm-2 col-xs-1">
                                            <input type="checkbox" name="areas[]" value="{{$ar->id}}" >
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-lg-3 col-md-3 col-sm-10 " style="padding-right: 0;padding-left: 0">
                        
                        <div class="col-lg-12 col-sm-12 col-xs-12" style="padding-right: 0;padding-left: 0">
                            <div class="col-sm-11" style="padding-right: 0;">
                                <div class="input-group" style="margin-top: 5px;">
                                    <input type="text" id="myInput" onkeyup="myFunction()" placeholder="REPRODUCCIÓN ASISTIDA" title="Busqueda rapida" name="key" class="form-control" autocomplete="off">
                                    <span class="input-group-addon search"><img src="/img/search.png" id="myInputIcon"></span>
                                </div>
                            </div>
                            <div class="col-lg-1 col-sm-1 col-xs-12 botonIr" style=" z-index: 60;">
                                <button type="submit" class="btn btn-float btn-lg " id="mapaIr" style="border-radius: 0;margin-top: 5px;">IR</button>
                            </div>
                            <!-- listado de cookies -->
                            <div class="col-lg-12 col-xs-12 listCookie">
                                <ul id="myUL" class="list-group listacookies" style="width: 105%;" style="padding-right: 0;padding-left: 0">
                       
                                    <div class="list-group-item"   style="display: none" id="loading">
                                        <center><b>Buscando</b><i class="fa fa-spinner fa-spin  fa-fw"></i></center>
                                    </div>
                     
                                </ul>
                            </div>
                            <!-- end listado cookies -->
                        </div>
                        
                    </div>
                </form>
        </div>
    </div>
    <div class="row"  style="margin-top: 4em; margin-right: 0">
        <div class="col-lg-7 col-xs-12"> 
            <!-- lista de colores -->
            <div id="listadoResult">
                <div class="row">
                     <div class="col-lg-12 col-sm-10 col-lg-offset-0 col-sm-offset-1">
                        <button  class="btn btn-ocultarResult ShowMapa"><i class="fa fa-close fa-3x"></i></button>
                    </div> 
                    <div class="col-lg-12 col-sm-10 col-lg-offset-0 col-sm-offset-1 listColor">
                        <div class="panel panel-default">
                            <!-- Default panel contents -->
                            <div class="panel-heading" style="background-color:#000;color: #fff"><center>LISTA DE RESULTADOS</center></div>
                            
                            <!-- Default panel contents -->
                            <div class="panel-heading" style="background:#C3C2C2;color: #000"><center>FILTROS APLICADOS</center></div>
                            
                            <div class="panel-body" id="datosSearch" >
                                <div class="nohaydatos"></div>
                                <div class="result" ></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- lista de colores -->
            <div id="graficaResult">
                 @mobile
                  @include('cuerpo.mapa.mapaPhone')
                  @elsemobile
                  <div style="margin-top: 4em;">
                      <canvas id="myCanvas"></canvas>
                    <div id='tooltip' class="volar ocultar">Nada</div> 
                  </div>
                 
                  @endmobile
                
            </div> 
        </div>
        <div class="col-lg-3 col-sm-4 col-xs-4  col-lg-offset-0  col-sm-offset-4 col-xs-offset-0">
            @include('cuerpo.mapa.mapaPC')
        </div>  
    </div>
</div>
@endsection