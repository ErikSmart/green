@extends('layouts.landing')<!--se pone nombre_de_carpeta.archivo-->
@section('content')
<link rel="stylesheet" href="{{asset('/css/inicio.css')}}">
@include('cuerpo.footerRelativo')
<body class="unete">
    <div class="container">
        <br>
        <form id="form_usuario" class="form_entrada" action="{{url('/newUsuario')}}" method="post" >
            <input type="hidden" name="_token" value="{{csrf_token()}}" id="token">
            <div class="col-lg-8 col-lg-offset-2 col-md-10">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <center><h3><b>RED DE VINCULACIÓN DE EGRESADOS FACULTAD DE BIOÉTICA UNIVERSIDAD ANÁHUAC</b></h3></center>
                        
                        <div class="form-group" id="nombre">
                            <label>Nombre completo *</label>
                            <input type="text" class="form-control id_nombre" name="nombreCompleto" onkeyup="msjclear('msj_id_nombre');" autocomplete="off">
                           <p style="display: none;color: #a94442" id="msj_id_nombre"></p>

                        </div>
                        
                        <label>Relación con actual con la facultad *</label>
                        <div id="id_perfil_error"> </div>
                            @foreach($perfil as $p)
                            <div class="form-group ">
                                <input type="radio" name="id_perfil"  value="{{$p->id}}" class="id_perfil" >&nbsp;&nbsp;{{$p->descripcion}}
                            </div>
                            @endforeach 
                            <p style="display: none;color: #a94442" id="msj_perfil"></p> 
                       
                        
                        <div class="form-group">
                            <label>Formación académica en la facultad de bioética de la universidad Anáhuac y status (incompleta, completa, o titulado) </label>
                    
                              {{--    @foreach($statusF as $sf)
                            <div class="form-group">
                                <input type="radio" name="id_status_formacion"  value="{{$sf->id}}" class="id_status_formacion" >&nbsp;&nbsp;{{$sf->descripcion}}
                            </div>
                            @endforeach --}}
                            <input type="text" class="form-control id_status_formacion" name="relacion"  >                            
                        </div>

                        <div class="row">
                        <br>
                        <div class="col-lg-9 col-xs-12" style="padding-right:0 ">

                           <b>Si tu licenciatura no se encuentra en la lista pulsa el botón agregar</b>      
                       </div>
                       <div class="col-lg-3 col-xs-12">
                           <button type="button" class="btn-primary btn-block btn-sm" data-toggle="modal" data-target="#myModal" >Agregar</button> 
                       </div>
                            <div class="col-lg-12">
                            <br>
                                <div class="form-group">
                                    <label>Formación académica de licenciatura</label>
                                    <select class="Licenciatura form-control" name="formAcademicLIC">
                                      
                                    </select>                         
                                </div>
                           </div>
                           
                        </div>
                       {{--  <div class="form-group">
                            <label>Identifique el área a la que corresponde su licenciatura de base</label>
                            <br>
                            <small><b>Seleccione el área con que MÁS IDENTIFIQUE su formación/licenciatura de base</b></small>
                            <div id="area_LIC"> </div>
                                @foreach($areasLic as $alic)
                                <div class="form-group">
                                    
                                    <input type="radio" name="id_area_LIC" class="id_area_LIC" value="{{$alic->id}}" >&nbsp;&nbsp;{{$alic->nombreArea}}
                                </div>
                                @endforeach
                           
                            <p style="display: none;color: #a94442" id="msj_id_area_LIC"></p>
                            
                        </div> --}}
                        <div class="form-group">
                            <label>Formación académica de maestría (a parte de la de bioética)</label>
                            <input type="text" class="form-control" name="formdeMaestria" placeholder="Coloque el nombre de su maestría ejemplo: “Maestría en medicina”" autocomplete="off">
                        </div>
                        <div class="form-group">
                            <label>Formación académica de especialidad (a parte de la de bioética)</label>
                            <input type="text" class="form-control" name="formEspecialidad" placeholder="Si usted tiene alguna Especialidad Médica o Académica ejemplo: “Especialidad en medicina”" autocomplete="off">
                        </div>
                        <div class="form-group">
                            <label> Formación académica de doctorado (a parte de la de bioética)</label>
                            <input type="text" class="form-control" name="formDoctorado" placeholder="Coloque el nombre de su doctorado ejemplo: “Doctorado en medicina”" autocomplete="off">
                        </div>
                        <div class="form-group">
                            <label>Otra formación de posgrado (a parte de la mencionada antes)</label>
                            <br>
                            <small><b> Si lo desea, coloque algún(os) otro(s) grado(s) académico(s) con los que cuenta</b></small>
                            <textarea  id="" cols="30" rows="10" name="formPostGrado" class="form-control" style="max-width: 100%;max-height: 200px" ></textarea>
                            
                        </div>
                        <div class="form-group">
                            <label>Ámbito gestión/laboral en bioética-status</label>
                            <div id="status_gestion_laboral"></div>
                            @foreach($statusL as $sL)
                            
                                <div class="form-group">
                                    
                                    <input type="radio" name="id_status_gestion_laboral"  class="id_status_gestion_laboral" value="{{$sL->id}}" >&nbsp;&nbsp;{{$sL->descripcion}}
                                </div>
                            
                            @endforeach
                            
                            <p style="display: none;color: #a94442" id="msj_id_status_gestion_laboral"></p>
                        </div>
                        <div class="form-group">
                            <label>Ámbito gestión/laboral -bioética -proyectos o actividades</label><br>
                            <small>
                            <b>Si contestó "ACTIVO" o "INACTIVO" en la pregunta anterior, usted trabaja o ha trabajado en alguna área relacionada con la bioética, o se desempeña en algún cargo en el que practique sus conocimientos de ésta, (sólo trabajo, no investigación). Coloque su(s) labore(s) o activadad(es) junto con la(s) institución(es) en la(s) que labora o ha laborado, a continuación (MÁXIMO 3 respuestas</b>
                            </small>
                            <div class="form-group">
                                <br>
                                <input type="text" class="form-control" name="respuesta1-1" placeholder="Tu respuesta" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="respuesta2-1" placeholder="Tu respuesta" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="respuesta3-1" placeholder="Tu respuesta" autocomplete="off">
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Ámbito gestión/laboral -áreas de la bioética</label>
                            @foreach($ambitos as $am)
                        
                                @if($am->nombre == 'Otro')
                                <div class="checkbox">
                                 <label>
                                    <input type="checkbox" name="gestion_laboral_areas[]"  value="{{$am->id}}">&nbsp;&nbsp;{{$am->nombre}}
                                    
                                </label><input type="text" name="gestion_laboral" autocomplete="off">
                                </div>
                                @else
                                <div class="checkbox">
                                 <label>
                                    <input type="checkbox" name="gestion_laboral_areas[]"  value="{{$am->id}}">&nbsp;&nbsp;{{$am->nombre}}
                                   
                                </label>
                                </div>
                                @endif
                          
                            @endforeach

                        </div>
                        <div class="form-group">
                            <label>Ámbito de formación/docencia en bioética – status</label>
                            <br>
                            <small>
                            <b>STATUS ACTUAL: ACTIVO= si desempeña en cargos de docente o en formación en Bioética. // INACTIVO= Actualmente no se desarrolla en ese ámbito de la Bioética PERO lo ha hecho antes. // INEXISTENTE= NUNCA se ha desempeñado en este ámbito de la Bioética.</b>
                            </small>
                            <div id="status_form_docencia">
                                @foreach($statusL as $sL)
                            <div class="form-group">
                                <input type="radio" name="id_status_form_docencia"  class="id_status_form_docencia" value="{{$sL->id}}" >&nbsp;&nbsp;{{$sL->descripcion}}
                            </div>
                            
                            @endforeach
                            </div>
                            <p style="display: none;color: #a94442" id="msj_id_status_form_docencia"></p>
                        </div>
                        <div class="form-group">
                            <label>Ámbito de formación/docencia en bioética- temas/ área</label>
                            <br>
                            <small><b>Si usted contestó "ACTIVO" o "INACTIVO", se desempeña o se desempeñó en la formación o de la docencia sobre algún tema de Bioética, o que pudiera tener relación. Coloque los tema(s) o clase(s) que imparte(ió), o áreas en las que desempeña (ó) e institución(es) en la(s) que realiza(ó) estas actividades, a continuación (Máximo 3 respuestas).</b></small>
                            <div class="form-group">
                                <br>
                                <input type="text" class="form-control" name="respuesta1-2" placeholder="Tu respuesta" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="respuesta2-2" placeholder="Tu respuesta" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="respuesta3-2" placeholder="Tu respuesta" autocomplete="off">
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Ámbito de formación/docencia- área de la bioética</label>
                            <br>
                            <small><b>SI usted contestó la pregunta anterior, seleccione por favor, una o varias de las “Áreas de la Bioética” que se relacionan con las actividades en el área de la Formación o de la Docencia en las que desarrolla o ha desarrollado</b></small>
                            @foreach($ambitos as $am)
                             @if($am->nombre == 'Otro')
                             <div class="checkbox">
                                 <label>
                                    <input type="checkbox" name="gestion_docencia_areas[]"  value="{{$am->id}}">&nbsp;&nbsp;{{$am->nombre}}
                                    
                                </label><input type="text" name="formacion_docencia" autocomplete="off">
                                </div>
                                @else
                                <div class="checkbox">
                                 <label>
                                    <input type="checkbox" name="gestion_docencia_areas[]"  value="{{$am->id}}">&nbsp;&nbsp;{{$am->nombre}}
                              
                                </label>
                                </div>
                            @endif
                            @endforeach
                        </div>
                        <div class="form-group">
                            <label>Ámbito de investigación en bioética – status</label>
                            <br>
                            <small><b>SELECCIONE una de las siguientes con respecto a su actividad en investigación: STATUS ACTUAL: ACTIVO= se desarrolla actualmente como investigador en Bioética. // INACTIVO= cuenta con investigaciones previas, pero no en desarrollo. // INEXISTENTE=no cuenta con investigaciones previas o en curso en el ámbito de la Bioética.</b></small>
                            <div id="status_invest_bio"></div>
                                   @foreach($statusL as $sL)
                            <div class="form-group">
                                
                                <input type="radio" name="id_status_invest_bio" class="id_status_invest_bio"  value="{{$sL->id}}" >&nbsp;&nbsp;{{$sL->descripcion}}
                            </div>
                            
                            @endforeach
                            
                            <p style="display: none;color: #a94442" id="msj_id_status_invest_bio"></p>
                        </div>
                        <div class="form-group">
                            <label>Ámbito de investigación en bioética - líneas de investigación / temas</label><br>
                            <small><b>Si usted contestó "ACTIVO" o "INACTIVO", se desempeña como investigador en algún área de la Bioética, o cuenta con investigaciones anteriores en esta área. Coloque los tema(s) e institución(es) en la(s) que realiza estas actividades, a continuación</b></small>
                            <textarea  id="" cols="30" rows="10" name="investigacion_temas" class="form-control" style="max-width: 100%;max-height: 200px" ></textarea>
                        </div>
                        <div class="form-group">
                            <label>Ámbito de investigación - área de la bioética</label>
                            <br>
                            <small><b>SI usted contestó la pregunta anterior, seleccione por favor, una o varias de las “Áreas de la Bioética” que se relacionan con las actividades en la investigación en las que desarrolla o ha desarrollado:</b></small>
                            @foreach($ambitos as $am)
                             @if($am->nombre == 'Otro')
                                <div class="checkbox">
                                 <label>
                                    <input type="checkbox" name="gestion_inves_areas[]"  value="{{$am->id}}">&nbsp;&nbsp;{{$am->nombre}}
                                   
                                </label>
                                 <input type="text" name="temas_area" autocomplete="off">
                                </div>
                                @else
                                <div class="checkbox">
                                 <label>
                                    <input type="checkbox" name="gestion_inves_areas[]"  value="{{$am->id}}">&nbsp;&nbsp;{{$am->nombre}}
                                  
                                </label>
                                </div>
                            @endif
                            @endforeach
                             
                        </div>
                        <div class="form-group">
                            <label>Medios de contacto</label>
                            <br>
                            <small><b>Esto es para que nos brinde el/los medios de contacto que quiere ofrecer al público para que le contacten. Pueden ser correo y teléfono, o dos correos, o dos teléfonos (2 formas de contacto máximo).</b></small>
                            
                        </div>
                        <div class="form-group">
                            <label>Teléfono *</label>
                            <input type="text" class="form-control id_telefono" name="telefono" id="telefono" onkeyup="msjclear('msj_id_telefono');" autocomplete="off" required="">
                             <p style="display: none;color: #a94442" id="msj_id_telefono"></p>
                        </div>
                        <div class="form-group">
                            <label>Teléfono Secundario</label>
                            <input type="text" class="form-control" name="telefono2" autocomplete="off">
                        </div>
                        <div class="form-group" id="correo">
                            <label>Correo  *</label>
                            <input type="email" class="form-control id_correo" name="correo" autocomplete="off" onkeyup="msjclear('msj_id_correo');" id="searchMail" oncopy="return false;" onpaste="return false;" oncut="return false;">

                        </div>
                        <p style="display: none;color: #a94442" id="msj_id_correo"></p>
                        <p id="msj_mailT" style="color: #a94442"></p>
                        
                        <div class="form-group">
                            <label>Correo Secundario </label>
                            <input type="email" class="form-control" name="correo2" autocomplete="off">
                        </div>
                        
                        <div class="form-group" id="palabras_claves">
                            <label>Palabras clave</label>
                            <br>
                            <small><b>A continuación coloque las palabras clave por las cuales a usted le gustaría ser encontrado en los filtros de búsqueda de la página. Estas palabras tienen que tener relación a sus trabajos en el campo laboral, docencia o investigación. Pueden ser palabras compuestas. Ejemplo: "neuroética", "derechos humanos", "pedagogía"</b></small>
                            <div class="s2-example">
                                <br>
                                <input type="text" name="palabras_claves"  class="form-control id_palabras_claves" onkeyup="msjclear('msj_id_palabras_claves');" required="">
                        <p style="display: none;color: #a94442" id="msj_id_palabras_claves"></p>
                                
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <strong style="text-align: justify;padding-left: 10px;">AVISO DE PRIVASIDAD:</strong>
                            <br>
                            <p style="font-size: 12px;text-align: justify;padding-left: 10px;padding-right: 10px;font-weight: 700;line-height: initial;">
                                Manifiesto mi consentimiento bajo información para que sean utilizados mis datos personales y/o sensibles, por la Facultad de Bióetica de la Universidad Anáhuac, proporcionados expresa y libremente, para los efectos legales correspondientes, de conformidad con la Ley de Protección de Datos en Posesión de Particulares, bajo principios de licitud, consentimiento, información, calidad, finalidad, lealtad, proporcionalidad y responsabilidad, previsto en la Ley. Con la finalidad de lograr la visibilidad y vinculación buscada, los datos que proporcione, serán visibles para cualquier persona en internet, es por esto que me hago responsable de la veracidad de estos y proporcino únicamente datos que desee exponer en la red: <br><br>
                                Articulo 16.- El aviso de privacidad deberá contener, al menos, la siguiente información:
                                I.- La identidad y domicilio del responsable que los recaba;
                                II.- Las finalidades del tratamiento de datos; <br>
                                II.- Las opciones y medios que el responsable ofrezca a los titulares para limpiar el uso o divulgación de datos; <br>
                                IV.- Los medios para ejercer los derechos de acceso, rectificación u oposición, de conformodidad con lo dispuesto en esta ley; <br>
                                V.- En su caso, las transferencias de datos que se efectúen, y. <br>
                                VI.- El procedimiento y medio por el cual el responsable comunicará a los titulares de cambios al aviso de privacidad con lo previsto en esta Ley. En el caso de datos personales sensibles, el aviso de privacidad deberá señalar expresamente que se trata de este tipo de datos.
                            </p>
                            <p align="right" ><b id="term_msj" style="display: none;color: #a94442;margin-right: 10px;font-weight: 100"></b>  Aceptar <input type="checkbox" name="terminos" class="terminos"></p>
                        </div>
                        <div class="col-lg-3"></div>
                        <div class="form-group col-lg-6 col-lg-offset-0 col-md-4 col-md-offset-4 col-sm-5 col-sm-offset-4 col-xs-12" style="padding: 0">
                            <table>
                                <tr>
                                    <td><center><i class="fa fa-refresh btn btn-xs" data-toggle="tooltip" title="Otro" id="loadCaptcha"></i></center></td>
                                </tr>
                                <tr>
                                    <td><center><div class="imgcaptcha"></div></center></td>
                                </tr>
                            </table>
                            <input type="hidden" id="captchaVal">
                            <input type="hidden" id="recaptchaRes" name="recaptcha">
                            <input type="text" name="captcha" id="valcaptcha" maxlength="10"  style="text-align: center; margin-top: 10px;width: 80%;height: 20px;padding-top: 0;" >
                            <a class="btn btn-xs " id="sendCaptcha" style="top: -1px;position: relative;">validar</a><br>
                            <b id="msj_captcha" style="display: none;color: #a94442;margin-right: 10px;font-weight: 100"></b>
                        </div>
                        <div class="col-md-12" id="guardando" style="display: none;"><center><label> Guardando Datos..</label><i class="fa fa-spinner fa-spin fa-2x fa-fw"></i></center></div>
                        <div class="form-group" id="guardar">
                            <button type="submit" class="btn btn-block btn-success" id="sendbutton" style="padding: 19px 0px;">ENVIAR&nbsp;&nbsp;<i class="fa fa-send"></i></button>
                        </div>
                    </div>
                    
                </div>
            </div>
        </form>
    </div>
      <!-- Modal agregar formación-->
      <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-md">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title text-center">Nueva Formación <i class="fa fa-graduation-cap"></i></h4>
            </div>
            <form id="form_licen" class="form_new" action="{{url('/newFormacion')}}" method="post">
            <input type="hidden" name="_token" value="{{csrf_token()}}" id="token">
                <div class="modal-body">
                        <div class="form-group">
                            <label>Identifique el área a la que corresponde su licenciatura</label>
                            <br>
                               {{--  @foreach($otros as $alic)
                                <div class="form-group">
                                    <input type="hidden" name="id_tooltip_carrera" value="{{$alic->id_tooltip_carrera}}">
                                    <input type="hidden" name="color" value="{{$alic->color}}">
                                    <input type="radio" name="area"  value="{{$alic->areasLic}}" required="">&nbsp;&nbsp;{{$alic->title}}
                                </div>                             
                                @endforeach --}}
                                 @foreach($areasLic as $alic)
                                <div class="form-group">
                                    <input type="radio" name="area" class="id_area_LIC" value="{{$alic->id}}" required="" >&nbsp;&nbsp;{{$alic->nombreArea}}
                                </div>
                                @endforeach
                        </div>                            
                        
                  <input type="text" class="form-control" id="norepeatformacion" name="title" placeholder="Ingresa Nombre de Formación" autocomplete="off" required=""> 
                  <p style="color: #a94442" id="msj_form"></p>
                </div>
                <div class="modal-footer">
                  <div class="col-md-12" id="saveformacion" style="display: none;"><center><label> Guardando Datos..</label><i class="fa fa-spinner fa-spin fa-2x fa-fw"></i></center></div>
                        <div class="form-group" id="guardarformacion">
                            <button type="submit" class="btn btn-block btn-success" id="sendform" style="padding: 19px 0px;">Guardar</button>
                        </div>
                </div>
            </form>
          </div>
        </div>
      </div>
      <!--  end Modal -->
</div>
</body>
@endsection
@section('js')
<script src="{{ asset('/js/mainUnete.js')}}"></script>
@endsection