@extends('layouts.landing')
@section('content')
<link rel="stylesheet" href="{{asset('/css/inicio.css')}}">
@include('cuerpo.footerRelativo')
<body class="quienes-somos">
	<div class="col-lg-9 col-md-8 contenido">
	<article class="container">
		<div class="row">
			<div class="col-sm-11 contenido-titular">
				<h4 class="bajarh4">CONTACTO ÁREAS DE LA BIOÉTICA</h4>
				<center><hr class="linea"></center>
				<h5 class="subirh5">Facultad de bioética</h5>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-11 todoeltexto " style="padding-bottom: 10%">
				<form id="f_sendmail" class="form-horizontal sendMail" action="{{url('/enviocorreo')}}" method="post" autocomplete="off">
					<br>
					<input type="hidden" name="_token" value="{{csrf_token()}}" id="token">
					<div class="col-md-1"></div>
					<div class="col-md-10">
						<div class="form-group">
							<label for="nombre" class="col-sm-2 control-label">Nombre</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="nombre" placeholder="Nombre" id="name">
							</div>
						</div>
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label">Correo</label>
							<div class="col-sm-10">
								<input type="email" class="form-control" name="correo" placeholder="Correo electrónico" id="correo">
							</div>
						</div>
						<div class="form-group">
							<label for="asunto" class="col-sm-2 control-label">Asunto</label>
							<div class="col-sm-10">
								<textarea class="form-control" rows="10"name="mensaje" id="mensaje"  style="max-width: 100%"></textarea>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
								<button id="enviarcorreo" type="submit" class="boton-naranja" style="letter-spacing: 3px;">Enviar</button>
								
								<div style="display: none;" id="correoenviado">
									<center><b>Enviando..</b><img src="https://digitalsynopsis.com/wp-content/uploads/2015/10/gif-icons-menu-transition-animations-sent.gif" alt="" width="150"></center>
								</div>
							</div>
						</div>
					</div>
				</form>
				
			</div>
		</div>
	</article>
</div>
</body>
@endsection