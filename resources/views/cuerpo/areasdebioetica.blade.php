@extends('layouts.landing')
@section('content')
<link rel="stylesheet" href="{{asset('/css/inicio.css')}}">

@include('cuerpo.footerRelativo')
  <body class="areas-de-bioetica-fondo">
	<div class="col-lg-9 col-md-8 contenido">
	<article class="container">
		<div class="row">
			<div class="col-md-11 contenido-titular">
				<h4 class="bajarh4">ÁREAS DE LA BIOÉTICA</h4>
				<center><hr class="linea"></center>
				<h5 class="subirh5">GUÍA TEMAS</h5>
			</div>
		</div>
		<div class="row">
			<div class="col-md-11 todoeltexto ">
				<br>
				<p class="parrafos">Te presentamos una guiá con “Áreas de la Bioetica”, que contiene “TEMAS” frecuentemente estudiados y discutidos en la bioética</p>
				<p class="parrafos">En algunos casos hemos listado subtemas, también en ocasiones existen temas y subtemas que pueden ser abordados por diferentes áreas, Sin embargo nos remitimos a esta aclaración y hemos omitido repeticiones.</p>
				<p class="parrafos">*NOTA: Si tienes algún comentario, pregunta o sugerencia, puedes enviarlo al buzón, en el apartado de “OTROS”.</p>
				<p class="parrafos"><strong>ÁREAS DE LA BIOÉTICA</strong></p>
				<picture class="container">
					<div class="contenido-imagenes">
						<div class="row">
							<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12  quitarespaciadoimagenes">
								<img src="/img/iconos/1.jpg" alt="alumnos bioetica"
								onmouseover="this.src='/img/textos/1.png';"
								onmouseout="this.src='/img/iconos/1.jpg';"  class="img-responsive" style="cursor: pointer;">
							</div>
							<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 quitarespaciadoimagenes">
								<img src="/img/iconos/2.jpg" alt="alumnos bioetica"
								onmouseover="this.src='/img/textos/2.png';"
								onmouseout="this.src='/img/iconos/2.jpg';" class="img-responsive" style="cursor: pointer;">
							</div>
							<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 quitarespaciadoimagenes">
								<img src="/img/iconos/3.jpg" alt="alumnos bioetica"
								onmouseover="this.src='/img/textos/3.png';"
								onmouseout="this.src='/img/iconos/3.jpg';" class="img-responsive" style="cursor: pointer;">
							</div>
							<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 quitarespaciadoimagenes">
								<img src="/img/iconos/4.jpg" alt="alumnos bioetica"
								onmouseover="this.src='/img/textos/4.png';"
								onmouseout="this.src='/img/iconos/4.jpg';" class="img-responsive" style="cursor: pointer;">
							</div>
						</div>
						<div class="row">
							<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 quitarespaciadoimagenes">
								<img src="/img/iconos/5.jpg" alt="alumnos bioetica"
								onmouseover="this.src='/img/textos/5.png';"
								onmouseout="this.src='/img/iconos/5.jpg';" class="img-responsive" style="cursor: pointer;">
							</div>
							<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 quitarespaciadoimagenes">
								<img src="/img/iconos/6.jpg" alt="alumnos bioetica"
								onmouseover="this.src='/img/textos/6.png';"
								onmouseout="this.src='/img/iconos/6.jpg';" class="img-responsive" style="cursor: pointer;">
							</div>
							<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 quitarespaciadoimagenes">
								<img src="/img/iconos/7.jpg" alt="alumnos bioetica"
								onmouseover="this.src='/img/textos/7.png';"
								onmouseout="this.src='/img/iconos/7.jpg';" class="img-responsive" style="cursor: pointer;">
							</div>
							<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 quitarespaciadoimagenes">
								<img src="/img/iconos/8.jpg" alt="alumnos bioetica"
								onmouseover="this.src='/img/textos/8.png';"
								onmouseout="this.src='/img/iconos/8.jpg';" class="img-responsive" style="cursor: pointer;">
							</div>
						</div>
						<div class="row">
							<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 quitarespaciadoimagenes">
								<img src="/img/iconos/9.jpg" alt="alumnos bioetica"
								onmouseover="this.src='/img/textos/9.png';"
								onmouseout="this.src='/img/iconos/9.jpg';" class="img-responsive" style="cursor: pointer;">
							</div>
							<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 quitarespaciadoimagenes">
								<img src="/img/iconos/10.jpg" alt="alumnos bioetica"
								onmouseover="this.src='/img/textos/10.png';"
								onmouseout="this.src='/img/iconos/10.jpg';" class="img-responsive" style="cursor: pointer;">
							</div>
							<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 quitarespaciadoimagenes">
								<img src="/img/iconos/11.jpg" alt="alumnos bioetica"
								onmouseover="this.src='/img/textos/11.png';"
								onmouseout="this.src='/img/iconos/11.jpg';" class="img-responsive" style="cursor: pointer;">
							</div>
							<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 quitarespaciadoimagenes">
								<img src="/img/iconos/12.jpg" alt="alumnos bioetica"
								onmouseover="this.src='/img/textos/12.png';"
								onmouseout="this.src='/img/iconos/12.jpg';" class="img-responsive" style="cursor: pointer;">
							</div>
						</div>
					</div>
					
					<div class="container ">
					
						<div class="col-lg-6 col-lg-offset-6 col-md-8 col-md-offset-4 col-sm-9 col-sm-offset-1 col-xs-12">
							<div class="cajadeareas">
							<div class="row">
							<div class="col-lg-2 col-md-2 col-sm-2"><img src="{{('/img/content/comentario.png')}}" alt="" width="60" height="60">
							</div>
							<div class="col-lg-4 col-md-5 col-sm-5"><strong style="font-size: 13px;position: relative;top: 5px" >OTROS TEMAS <br> DE LA BIOÉTICA</strong></div>
							</div>
							<p class="parrafos" >* Enviá tus sugerencias si no encuentras un tema que esté contenido dentro de unas de las áreas</p>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-2"></div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-10" style="position: relative;top:-18px;padding-right: 0;padding-left: 0">
							
								<a href="{{url('/contactanos')}}" class="boton-naranja parrafos">CONTACTÁNOS</a>
							
							</div>
						</div>

						</div>					
						
					</div>
					<br>
				</div>
			</div>
		</article>
	</div>
</body>
	@endsection