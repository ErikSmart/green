<header class="header">
  <div class="container">
    <div class="row margin20 logo-areas">
      <!-- Inicio logo -->
      <div class="col-lg-3 col-sm-3 log-izq">
        <div>
          <div id="block-logo" class="block block-block-content ">
            <div class="content">
              <div>
                <p><a href="http://www.anahuac.mx/mexico"><img alt="Universidad Anáhuac México"   src="{{ asset('img/logo_0.png')}}"></a></p>
                <input type="hidden" name="_token" value="{{csrf_token()}}" id="token">
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Fin logo -->
      <!-- Inicio Slogan -->
      <div class="col-lg-6 col-sm-6 frase-arriba">
        <div>
          <div id="block-headercenter" class="block block-block-content ">
            <div class="content">
              <div>
                <h3>Somos Anáhuac México. Líderes de Acción Positiva.</h3>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Fin slogan -->
      <div class="col-lg-3 col-sm-3 idiomas-der"></div>
    </div>
  </div>
</header>
<!-- Fin encabezado -->
<!-- Por Catalán -->
<!-- Inicio del menú principal -->
<nav class="navbar navbar-default navbar-static-top navbar-yellow">
    <div class="navbar-header">
      <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
    </div>
    <div class="navbar-collapse collapse">
      <div>

        <nav role="navigation" aria-labelledby="block-mainnavigation-menu" id="block-mainnavigation">
          <h2 class="visually-hidden" id="block-mainnavigation-menu">Main navigation</h2>
          <ul class="nav navbar-nav">
            <li>
              <a href="http://www.anahuac.mx/mexico/"  class="is-active">Inicio</a>
            </li>
            <li class="has-child">
              <a href="http://www.anahuac.mx/mexico/" target=""  class="is-active dropdown-toggle" data-toggle="dropdown">NUESTRA UNIVERSIDAd</a>
              <ul class="dropdown-menu" >
                <li>
                  <a href="http://www.anahuac.mx/mexico/mision" target="_self" title="Misión">Misión</a>
                </li>
                <li>
                  <a href="http://www.anahuac.mx/mexico/historia" target="" title="Historia">Historia</a>
                </li>
                <li>
                  <a href="http://www.anahuac.mx/mexico/Modelo-Educativo-Anahuac-de-Formacion-Integral" target="" title="Modelo Anáhuac">Modelo Educativo Anáhuac</a>
                </li>
                <li>
                  <a href="http://www.anahuac.mx/mexico/ObjetivosFormativos" target="" title="Objetivos Formativos">Objetivos Formativos</a>
                </li>
                <li>
                  <a href="http://www.anahuac.mx/mexico/calidad-academica-acreditada" target="" title="Calidad Académica">Calidad Académica Acreditada</a>
                </li>
                <li>
                  <a href="http://www.anahuac.mx/mexico/lineas-estrategicas" target="" title="Líneas Estratégicas">Líneas Estratégicas</a>
                </li>
                <li>
                  <a href="http://anahuac.mx/mexico/files/2017/ManualdeImagenInstitucional.pdf" target="_blank" title="Manual de Imagen Institucional">Manual de Imagen Institucional</a>
                </li>
                <li>
                  <a href="http://www.anahuac.mx/mexico/NuestrosCampus" target="" title="Nuestros Campus">Nuestros Campus</a>
                </li>
                <li>
                  <a href="http://www.anahuac.mx/mexico/Directorio" target="" title="Directorio de áreas de la Universidad Anáhuac México">Directorio</a>
                </li>
                <li>
                  <a href="http://redanahuac.mx/rua/" target="_blank" title="Red de Universidades Anáhuac">Red de Universidades Anáhuac</a>
                </li>
              </ul>

            </li>
            <li class="has-child">
              <a href="http://pegaso.anahuac.mx/preuniversitarios/oferta-educativa" target="_blank" class="dropdown-toggle" data-toggle="dropdown">LICENCIATURAS</a>
              <ul class="dropdown-menu" >
                <li>
                  <a href="http://www.anahuac.mx/mexico/preuniversitarios/" target="_blank" title="inicio">Conoce más...</a>
                </li>
                <li>
                  <a href="http://www.anahuac.mx/mexico/preuniversitarios/proceso-de-admision" target="_blank" title="Admisiones">Admisiones</a>
                </li>
                <li>
                  <a href="http://www.anahuac.mx/mexico/preuniversitarios/oferta-educativa" target="_blank" title="Oferta Académica">Oferta Académica</a>
                </li>
                <li>
                  <a href="http://www.anahuac.mx/mexico/preuniversitarios/cuotas" target="_blank" title="Costos">Costos</a>
                </li>
                <li>
                  <a href="http://www.anahuac.mx/mexico/preuniversitarios/financiamiento" target="_blank" title="Becas y Financiamiento">Becas y Financiamiento</a>
                </li>
                <li>
                  <a href="http://ww2.anahuac.mx/foraneos/" target="_blank" title="Alumnos Foráneos">Alumnos Foráneos</a>
                </li>
              </ul>

            </li>
            <li class="has-child">
              <a href="http://www.anahuac.mx/mexico/posgrados/" target="_blank" class="dropdown-toggle" data-toggle="dropdown">POSGRADOS</a>
              <ul class="dropdown-menu" >
                <li>
                  <a href="http://www.anahuac.mx/mexico/posgrados/" target="_blank">Conoce más...</a>
                </li>
                <li>
                  <a href="http://www.anahuac.mx/mexico/posgrados/admisiones" target="_blank" title="Admisiones">Admisiones</a>
                </li>
                <li>
                  <a href="http://www.anahuac.mx/mexico/posgrados/escuelas-y-facultades" target="_blank" title="Oferta Académica">Oferta Académica</a>
                </li>
                <li>
                  <a href="http://www.anahuac.mx/mexico/posgrados/costos-y-financiamiento" target="_blank" title="Costos">Costos y Financiamiento</a>
                </li>
                <li>
                  <a href="http://www.anahuac.mx/mexico/posgrados/convenios-de-descuento" target="_blank" title="Becas y Financiamiento">Becas y Financiamiento</a>
                </li>
              </ul>

            </li>
            <li class="has-child">
              <a href="http://www.anahuac.mx/mexico/educacioncontinua/" target="_blank" title="EDUCACIÓN CONTINUA" class="dropdown-toggle" data-toggle="dropdown">EDUCACIÓN CONTINUA</a>
              <ul class="dropdown-menu" >
                <li>
                  <a href="http://www.anahuac.mx/mexico/educacioncontinua/" target="_blank">Conoce más...</a>
                </li>
                <li>
                  <a href="http://www.anahuac.mx/mexico/educacioncontinua/admisiones" target="_blank" title="Admisiones">Admisiones</a>
                </li>
                <li>
                  <a href="http://www.anahuac.mx/mexico/educacioncontinua/programas" target="_blank" title="Oferta Académica">Oferta Académica</a>
                </li>
                <li>
                  <a href="http://www.anahuac.mx/mexico/educacioncontinua/costos-y-procesos-de-pago" target="_blank" title="Costos">Costos y Proceso de Pago</a>
                </li>
                <li>
                  <a href="http://www.anahuac.mx/mexico/educacioncontinua/convenios-de-descuento" target="_blank" title="Becas y Financiamiento">Becas y Financiamiento</a>
                </li>
              </ul>

            </li>
            <li>
              <a href="http://www.anahuac.mx/mexico/VidaUniversitaria" target="" >VIDA UNIVERSITARIA</a>
            </li>
            <li>
              <a href="http://www.anahuac.mx/mexico/EscuelasyFacultades" target="" >ESCUELAS Y FACULTADES</a>
            </li>
          </ul>
        </nav>
      </div>
    </div>
</nav>
<!-- Fin del menú principal -->

<!-- Inicio menú secundario -->
<nav class="navbar second-nav-bar navbar-static-top navbar-yellow" id="navYellow">
  <div class="container">
    <div class="navbar-header">
      <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
    </div>
    <div class="navbar-collapse collapse">

      <div>
        <nav role="navigation" aria-labelledby="block-secondarymenu-menu" id="block-secondarymenu">
          <h2 class="visually-hidden" id="block-secondarymenu-menu">Secondary Menu</h2>

          <ul class="nav navbar-nav">
            <li class="has-child">
              <a href="http://ww2.anahuac.mx/rectoria/" target="_blank" title="OFICINA DE RECTORÍA" class="dropdown-toggle" data-toggle="dropdown">OFICINA DE RECTORÍA</a>
              <ul class="dropdown-menu" >
                <li>
                  <a href="http://ww2.anahuac.mx/rectoria/rector/mensaje-del-rector" target="_blank" title="Mensaje del Rector">Mensaje del Rector</a>
                </li>
                <li>
                  <a href="http://ww2.anahuac.mx/rectoria/rector/semblanza" target="_blank" title="Semblanza">Semblanza</a>
                </li>
                <li>
                  <a href="http://ww2.anahuac.mx/rectoria/" target="_blank" title="Discursos">Discursos</a>
                </li>
                <li>
                  <a href="http://www.anahuac.mx/mexico/2016/Rector&#39;s-annual-report.pdf" target="_blank" title="Informe del 2016">Informe 2016</a>
                </li>
              </ul>

            </li>
            <li>
              <a href="http://online.anahuac.mx/" target="_self" title="Oferta maestrías en línea">MAESTRÍAS EN LÍNEA</a>
            </li>
            <li>
              <a href="http://www.anahuac.mx/mexico/ServiciosUniversitarios" target="" title="SERVICIOS UNIVERSITARIOS">SERVICIOS UNIVERSITARIOS</a>
            </li>
            <li>
              <a href="http://pegaso.anahuac.mx/egresados" target="_blank" title="EGRESADOS">EGRESADOS</a>
            </li>
            <li>
              <a href="http://www.anahuac.mx/mexico/noticias" target="" title="NOTICIAS" >NOTICIAS</a>
            </li>
          </ul>
        </nav>
      </div>
    </div>
  </div>
</nav>
<div>