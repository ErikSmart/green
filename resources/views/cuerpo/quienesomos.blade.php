@extends('layouts.landing')
@section('content')
<link rel="stylesheet" href="{{asset('/css/inicio.css')}}">

@include('cuerpo.footerRelativo')
<body class="quienes-somos">
	
<div class="col-lg-9 col-md-10 col-sm-12 contenido ">
	<article class="container">
		<div class="row">
			<div class="col-sm-11 col-xs-12 contenido-titular">
				<h4 class="bajarh4">RED DE VINCULACIÓN DE LA FACULTAD DE BIÓETICA</h4>
				<center><hr class="linea"></center>
				<h5 class="subirh5">UNIVERSIDAD ANAHUAC MÉXICO NORTE</h5>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-11 col-xs-12 todoeltexto ">
				<br>
				<p class="parrafos">La formación de esta red tiene como objetivo mantener información actualizada accesible para todo el que la necesite sobre quiénes, qué hacen y dónde hacen Bioética las personas que forman parte o formaron parte de la Facultad de Bioética de la Universidad Anáhuac México Norte. Ademas esperamos que esto sirva como un medio de contacto y formación de vínculos entre sus miembros y con personas externas.</p>
				<p class="parrafos"><strong>MIEMBROS DE LA FACULTAD</strong></p>
				<picture class="container">
					<div class="row">
						<div class="col-md-4 col-sm-6 col-xs-12 img-quienes">
							<img src="{{asset('/img/content/alumnos-texto-bioetica.png')}}" alt="alumnos-bioetica"  width="239" height="221"  onmouseover="this.src='{{asset("/img/content/alumnos-bioetica.png")}}';"
							onmouseout="this.src='{{asset("/img/content/alumnos-texto-bioetica.png")}}';" style="cursor:pointer;">
						</div>
						<div class="col-md-4 col-sm-6 col-xs-12 img-quienes">
							<img src="{{asset('/img/content/academicos-investigadores-texto-bioetica.png')}}" alt="investigadores bioética"  width="239" height="221"
							onmouseover="this.src='{{asset("/img/content/academicos-investigadores-bioetica.png")}}';" onmouseout="this.src='{{asset("/img/content/academicos-investigadores-texto-bioetica.png")}}';" style="cursor:pointer;">
						</div>
						<div class="col-md-4 col-sm-6 col-sm-offset-3 col-md-offset-0 col-xs-12 img-quienes" >
							<img src="{{asset('/img/content/egredados-texto-bioetica.png')}}" alt="Egresados bioética"  width="239" height="221"
							onmouseover="this.src='{{asset("/img/content/egredados-bioetica.png")}}';"
							onmouseout="this.src='{{asset("/img/content/egredados-texto-bioetica.png")}}';" style="cursor:pointer;">
						</div>
					</div>
				</picture>
				<br>
				<div class="boton-naranjaflexbox">
					<a href="{{url('/unete')}}" class="boton-naranja">ÚNETE</a>
				</div>
				<br>
				<br>
				<p class="parrafos"><strong>VÍNCULOS </strong></p>
				<p class="parrafos">Estas son instituciones a las que diversos miembros de nuestra facultad pertenecen</p>
				<img src="{{asset('img/content/hospital-general-de-mexico.png')}}" alt="hospital genera de mexico" width="128">
				<img src="{{asset('img/content/universidad-paramericana.png')}}" alt="Universidad paramericana" height="64">
				<p class="parrafos">Más información en la página oficial de la facultad de bioética Anahuac México <br> nortepegaso.anahuc.mx/bioetica</p>
			</div>
		</div>
	</article>
</div>
</body>
@endsection