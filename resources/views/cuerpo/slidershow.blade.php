@if($actual[0]->status == 'Activo')
<div class="table-responsive">
	<table  class="tablitashow slick-slide slick-current slick-active" border="0" style="width: 100%;"   id="contenlist" >
		<thead valign="top">
			<tr>
				<th style="min-width: 153px;background: #fff;">
					<table>
						<tr >
							<th style="padding-top: 15px">&nbsp;&nbsp;ACTIVO</th>
							<td><i class="fa fa-check" style="font-size: 25px;color: #B87708;position: relative;left: 100%;padding-right: 15px"></i></td>
						</tr>
						<tr>
							<th>&nbsp;&nbsp;INACTIVO</th>
							<td><i class="fa fa-minus" style="font-size: 25px;color: #81ADB9;position: relative;left: 100%;padding-right: 15px"></i></td>
							
						</tr>
					</table>
				</th>
				<th colspan="4" style="margin-bottom: 10px"><p style="margin-left: 5px;padding: 10px;background-color: black;color: white;">{{$actual[0]->nombreCompleto}}</p></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td><p style="background: #424242;color: #fff;padding-left: 10px;padding-bottom: 5px">GESTIÓN</p></td>
				
				<td colspan="3" style="padding-left: 10px; background: white;width: 592px">
					<table>
						<tr>
							<td>@if(!empty($actual[0]->rgestion))
								@if($actual[0]->sg == 'Activo')
								<i class="fa fa-check" style="font-size: 25px;color: #B87708;"></i>
								@elseif($actual[0]->sg == 'Inactivo')
								<i class="fa fa-minus" style="font-size: 25px;color: #81ADB9;"></i>
								@else
								<i class="fa fa-times" style="font-size: 25px;color: transparent;padding-right: 6px;"></i>
								@endif
								@endif
								
							</td>
							<th style="padding-left: 5px;padding-bottom: 15px"><p style="width: 480px;">{!!$actual[0]->rgestion!!}</p></th>
						</tr>
					</table>
				</td>
				<td ROWSPAN=5 style="background-color: {{$actual[0]->color}};color: #fff; word-wrap: break-word;"><p style="font-size: 14px;font-weight: 500;padding-left: 10px">INFORMACIÓN DE VINCULACIÓN Y CONTACTO</p>
				<center>
				<img src="/img/movil.png" width="25">
				<p style="margin-bottom: 25px;">{{$actual[0]->telefono}} <br> {{$actual[0]->telefono2}}</p>
				<img src="/img/mail.png" alt="">
				<p style="margin-bottom: 25px;">{{$actual[0]->email}} <br> {{$actual[0]->email2}}</p>
				<div class="divSocial">
					
					<div class="fb-share-button"
						data-href="{{url('ver', [$actual[0]->nombreCompleto,$actual[0]->idV])}}"
						data-layout="button_count">
					</div>
					<a
						href="https://twitter.com/share"
						class="twitter-share-button"
						data-show-count="false"
					data-url="{{url('ver', [$actual[0]->nombreCompleto,$actual[0]->idV])}}">Tweet</a>
					<div class="g-plus" 
					data-action="share" 
					data-href="{{url('ver', [$actual[0]->nombreCompleto,$actual[0]->idV])}}">
					</div>
				</div>
				</center>
			</td>
		</tr>
		<tr>
			<td style="padding-bottom: 10px;"><p style="background: #424242;color: #fff;padding-left: 10px;padding-bottom: 5px">DOCENCIA</p></td>
			<td colspan="3" style="background: white;">
				<table>
					<tr>
						<td style="padding-left: 10px;">
							@if(!empty($actual[0]->rdocencia))
							@if($actual[0]->sd == 'Activo')
							<i class="fa fa-check" style="font-size: 25px;color: #B87708;"></i>
							@elseif($actual[0]->sd == 'Inactivo')
							<i class="fa fa-minus" style="font-size: 25px;color: #81ADB9;"></i>
							@else
							<i class="fa fa-times" style="font-size: 25px;color: transparent;padding-right: 6px;"></i>
							@endif
							@endif
						</td>
						<th style="padding-left: 5px;padding-bottom: 15px;">{!!$actual[0]->rdocencia!!}</th>
					</tr>
				</table>
			</td>
		</tr>
		<tr><td style="padding-bottom: 10px"><p style="background: #424242;color: #fff;padding-left: 10px;padding-bottom: 5px">INVESTIGACIÓN</p></td>
		<td colspan="3" style="background: white;">
			<table>
				<tr>
					<td style="padding-left: 10px;">
						@if(!empty($actual[0]->investigacion_temas))
						@if($actual[0]->si == 'Activo')
						<i class="fa fa-check" style="font-size: 25px;color: #B87708;"></i>
						@elseif($actual[0]->si == 'Inactivo')
						<i class="fa fa-minus" style="font-size: 25px;color: #81ADB9;"></i>
						@else
						<i class="fa fa-times" style="font-size: 25px;color: transparent;padding-right: 6px;"></i>
						@endif
					@endif</td>
					<th style="padding-left: 5px">{{$actual[0]->investigacion_temas}}</th>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<th style="padding-right: 10px;padding-bottom: 15px;background: {{$actual[0]->color}};">
			<p style="color: #fff;padding-left: 5px;padding-bottom: 5px">PERFIL DE INFORMACI&Oacute;N</p>
		</th>
		
		<td colspan="3" style="background: #424242;color: #fff;">
			<table style="width: 100%;" class="table-striped">
				
				<tr>
					<th>
						<p style="padding-left: 20px;margin:0">{{$actual[0]->formAcademicLic}}</p>
					</th>
				</tr>
				<tr>
					<th>
						<p style="padding-left: 20px;margin:0">{{$actual[0]->formdeMaestria}}</p>
					</th>
				</tr>
				<tr>
					<th>
						<p style="padding-left: 20px;margin:0">{{$actual[0]->formEspecialidad}}</p>
					</th>
				</tr>
				<tr>
					<th>
						<p style="padding-left: 20px;margin:0">{{$actual[0]->formDoctorado}}</p>
					</th>
				</tr>
				<tr>
					<th>
						<p style="padding-left: 20px;margin:0">{{$actual[0]->formPostGrado}}</p>
					</th>
				</tr>
			</table>
		</td>
	</tbody>
</table>
<meta name="keywords" content="{{$actual[0]->palabras_claves}}"/>
</div>
@else
<div style="height: 448px">
<h1 align="center" style="position: relative;top: 40%">Usuario temporalmente dado de baja <i class="fa fa-thumbs-o-down" aria-hidden="true"></i></h1>
</div>
@endif
@foreach($datos as $re)
@if($actual[0]->nombreCompleto != $re->nombreCompleto && $re->status == 'Activo')
<div class="table-responsive">
<table  class="tablitashow" border="0" style="width: 100%"  >
	<thead valign="top">
		<tr>
			<th style="min-width: 153px;background: #fff;">
				<table>
					<tr >
						<th style="padding-top: 15px">&nbsp;&nbsp;ACTIVO</th>
						<td><i class="fa fa-check" style="font-size: 25px;color: #B87708;position: relative;left: 100%;padding-right: 15px"></i></td>
					</tr>
					<tr>
						<th>&nbsp;&nbsp;INACTIVO</th>
						<td><i class="fa fa-minus" style="font-size: 25px;color: #81ADB9;position: relative;left: 100%;padding-right: 15px"></i></td>
						
					</tr>
				</table>
			</th>
			<th colspan="4" style="margin-bottom: 10px"><p style="margin-left: 5px;padding: 10px;background-color: black;color: white;">{{$re->nombreCompleto}}</p></th>
		</tr>
	</thead>
	<TBODY valign="top">
		<tr><td><p style="background: #424242;color: #fff;padding-left: 10px;padding-bottom: 5px">GESTIÓN</p></td>
		<td colspan="3" style="padding-left: 10px; background: white;width: 592px">
			<table>
				<tr>
					<td>	@if(!empty($re->rgestion))
						@if($re->sg == 'Activo')
						<i class="fa fa-check" style="font-size: 25px;color: #B87708;"></i>
						@elseif($re->sg == 'Inactivo')
						<i class="fa fa-minus" style="font-size: 25px;color: #81ADB9;"></i>
						@else
						<i class="fa fa-times" style="font-size: 25px;color: transparent;padding-right: 6px;"></i>
						@endif
						@endif
					</td>
					<th style="padding-left: 5px;max-width: 900px;padding-bottom: 15px"><p style="width: 480px;">{!!$re->rgestion!!}</p></th>
				</tr>
			</table>
			</td><td ROWSPAN=5 style="background-color: {{$re->color}};color: #fff; word-wrap: break-word;"><p style="font-size: 14px;font-weight: 500;padding-left: 10px">INFORMACIÓN DE VINCULACIÓN Y CONTACTO</p>
			<center>
			<img src="/img/movil.png" width="25">
			<p style="margin-bottom: 25px;">{{$re->telefono}}</p>
			<img src="/img/mail.png" alt="">
			<p style="margin-bottom: 25px;">{{$re->email}}</p>
			<div class="divSocial">
				
				<div class="fb-share-button"
					data-href="{{url('ver', [$re->nombreCompleto,$re->idV])}}"
					data-layout="button_count">
				</div>
				<a
					href="https://twitter.com/share"
					class="twitter-share-button"
					data-show-count="false"
					data-url="{{url('ver', [$re->nombreCompleto,$re->idV])}}">Tweet</a>
				<div class="g-plus" 
					data-action="share" 
					data-href="{{url('ver', [$re->nombreCompleto,$re->idV])}}"></div>
			</div>
			</center>
		</center></td></tr>
		<tr><td style="padding-bottom: 10px;"><p style="background: #424242;color: #fff;padding-left: 10px;padding-bottom: 5px">DOCENCIA</p></td>
		<td colspan="3" style="background: white;">
			<table>
				<tr >
					<td style="padding-left: 10px;">
						@if(!empty($re->rdocencia))
						@if($re->sd == 'Activo')
						<i class="fa fa-check" style="font-size: 25px;color: #B87708;"></i>
						@elseif($re->sd == 'Inactivo')
						<i class="fa fa-minus" style="font-size: 25px;color: #81ADB9;"></i>
						@else
						<i class="fa fa-times" style="font-size: 25px;color: transparent;padding-right: 6px;"></i>
						@endif
						@endif
					</td>
					<th style="padding-left: 5px;padding-bottom: 15px;">{!!$re->rdocencia!!}</th>
				</tr>
			</table>
		</td></tr>
		<tr><td style="padding-bottom: 10px"><p style="background: #424242;color: #fff;padding-left: 10px;padding-bottom: 5px">INVESTIGACIÓN</p></td>
		<td colspan="3" style="background: white;">
			<table>
				<tr>
					<td style="padding-left: 10px;">
						@if(!empty($re->investigacion_temas))
						@if($re->si == 'Activo')
						<i class="fa fa-check" style="font-size: 25px;color: #B87708;"></i>
						@elseif($re->si == 'Inactivo')
						<i class="fa fa-minus" style="font-size: 25px;color: #81ADB9;"></i>
						@else
						<i class="fa fa-times" style="font-size: 25px;color: transparent;padding-right: 6px;"></i>
						@endif
						@endif
					</td>
					<th style="padding-left: 5px">{{$re->investigacion_temas}}</th>
				</tr>
			</table>
		</td>
	</tr>
	<tr><th style="padding-right: 10px;padding-bottom: 15px;background: {{$re->color}};"><p style="color: #fff;padding-left: 5px;padding-bottom: 5px">PERFIL DE INFORMACI&Oacute;N</p></th>
	<th colspan="3" style="background: #424242;color: #fff;">
		<table  class="table-striped" style="width: 100%;">
			
			<tr>
				<th>
					<p style="padding-left: 20px;margin:0">{{$re->formAcademicLic}}</p>
				</th>
			</tr>
			<tr>
				<th >
					<p style="padding-left: 20px;margin:0">{{$re->formdeMaestria}}</p>
				</th>
			</tr>
			<tr>
				<th>
					<p style="padding-left: 20px;margin:0">{{$re->formEspecialidad}}</p>
				</th>
			</tr>
			<tr>
				<th >
					<p style="padding-left: 20px;margin:0">{{$re->formDoctorado}}</p>
				</th>
			</tr>
			<tr>
				<th>
					<p style="padding-left: 20px;margin:0">{{$re->formPostGrado}}</p>
				</th>
			</tr>
		</table>
	</th>
</TBODY>
</table>
<meta name="keywords" content="{{$re->palabras_claves}}"/>
</div>
@elseif($actual[0]->nombreCompleto != $re->nombreCompleto && $re->status == 'Inactivo')
<div style="height: 448px">
<h1 align="center" style="position: relative;top: 40%">Usuario temporalmente dado de baja <i class="fa fa-thumbs-o-down" aria-hidden="true"></i></h1>
</div>
@endif
@endforeach