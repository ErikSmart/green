-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 12-03-2017 a las 06:14:46
-- Versión del servidor: 10.1.9-MariaDB
-- Versión de PHP: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bioetica`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ambitos`
--

CREATE TABLE `ambitos` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `ambitos`
--

INSERT INTO `ambitos` (`id`, `nombre`, `created_at`, `updated_at`) VALUES
(1, 'Fundamentos de la Ética y Bioética', NULL, NULL),
(2, 'Bioética al Inicio de la Vida', NULL, NULL),
(3, 'Bioética al Final de la Vida', NULL, NULL),
(4, 'Bioética, Reproducción Humana y Sexualidad', NULL, NULL),
(5, 'Bioética Clínica', NULL, NULL),
(6, 'Bioética y la Industria Farmacéutica', NULL, NULL),
(7, 'Bioética y Tecnlogía', NULL, NULL),
(8, 'Bioética en la Investigación Clínica y Básica', NULL, NULL),
(9, 'Bioética Equidad y Justicia', NULL, NULL),
(10, 'Bioética y Educación', NULL, NULL),
(11, 'Bioética y Medio Ambiente', NULL, NULL),
(12, 'Arte, Ética y Bioética', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `areaslic`
--

CREATE TABLE `areaslic` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombreArea` text COLLATE utf8_unicode_ci NOT NULL,
  `acronimo` text COLLATE utf8_unicode_ci NOT NULL,
  `color` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `areaslic`
--

INSERT INTO `areaslic` (`id`, `nombreArea`, `acronimo`, `color`, `created_at`, `updated_at`) VALUES
(1, 'Ciencias de la Salud. (medicina, nutrición,psicología, enfermería, rehabilitación, etc.)', 'C. DE LA SALUD', '765AA3', NULL, NULL),
(2, 'Ciencias Sociales y Humanidades (filosofía, historia, derecho, etc.)', 'C. SOCIALES Y HUMANIDADES', '76BBA5', NULL, NULL),
(3, 'Ciencias Económicas (administración, economía, contaduría, actuaría, etc.)', 'C. ECONÓMICAS', 'C37E2B', NULL, NULL),
(4, 'Ciencias Naturales (biología, química, física, geografía, astronomía, etc.)', 'C. NATURALES', 'A5B81E', NULL, NULL),
(5, 'Ciencias Religiosas (teología, filosofía, etc.)', 'C. NATURALES', 'E2C273', NULL, NULL),
(6, 'Ciencias Tecnológicas (Biotecnología, Ing. Industrial, Ing. Mecánica, Ing. Informática, etc.)', 'C. TECNOLÓGICAS', '1F227E', NULL, NULL),
(7, 'Arte y Cultura (Danza, Teatro, Fotografía, Diseño)', 'ARTE Y CULTURA', '7F2338', NULL, NULL),
(8, 'Otra área', 'OTROS', '9F3889', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_05_27_203930_add_deleted_to_users_table', 1),
('2017_02_13_165723_create_referentials_table', 1),
('2017_02_13_170210_create_vinculacions_table', 1),
('2017_02_13_170211_create_ambitos_table', 1),
('2017_02_13_171657_create_respuestas_table', 1),
('2017_02_14_024154_create_vinculacions_ambitos_table', 1),
('2017_02_14_041059_areasLic', 1),
('2017_02_26_032715_add_deleted_vinculacions_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `referentials`
--

CREATE TABLE `referentials` (
  `id` int(10) UNSIGNED NOT NULL,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `identificador` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `referentials`
--

INSERT INTO `referentials` (`id`, `descripcion`, `identificador`, `created_at`, `updated_at`) VALUES
(1, 'SuperAdmin', 'perfil', NULL, NULL),
(2, 'Alumno', 'perfil', NULL, NULL),
(3, 'Profesor', 'perfil', NULL, NULL),
(4, 'Egresado', 'perfil', NULL, NULL),
(5, 'Incompleta', 'statusF', NULL, NULL),
(6, 'Completa', 'statusF', NULL, NULL),
(7, 'Titulado', 'statusF', NULL, NULL),
(8, 'Activo', 'statusL', NULL, NULL),
(9, 'Inactivo', 'statusL', NULL, NULL),
(10, 'Inexistente', 'statusL', NULL, NULL),
(11, 'Gestión', 'ambito', NULL, NULL),
(12, 'Docencia', 'ambito', NULL, NULL),
(13, 'Investigación', 'ambito', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `respuestas`
--

CREATE TABLE `respuestas` (
  `id` int(10) UNSIGNED NOT NULL,
  `respuesta1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `respuesta2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `respuesta3` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `respuestas`
--

INSERT INTO `respuestas` (`id`, `respuesta1`, `respuesta2`, `respuesta3`, `created_at`, `updated_at`) VALUES
(1, 'Cambios en la percepción de la identidad de la persona humana pertinencia de las publicaciones en bioética principios de la bioética personalista	', '', '', NULL, NULL),
(2, 'Fundamentos de la Bioética Bioética al Final de la Vida Temas selectos de Bioética', '', '', NULL, NULL),
(3, 'Cambios en la percepción de la identidad de la persona humana pertinencia de las publicaciones en bioética principios de la bioética personalista	', '', '', NULL, NULL),
(4, 'Fundamentos de la Bioética Bioética al Final de la Vida Temas selectos de Bioética', '', '', NULL, NULL),
(5, '', '', '', NULL, NULL),
(6, '', '', '', NULL, NULL),
(7, '', '', '', NULL, NULL),
(8, '', '', '', NULL, NULL),
(9, '', '', '', NULL, NULL),
(10, '', '', '', NULL, NULL),
(11, '', '', '', NULL, NULL),
(12, '', '', '', NULL, NULL),
(13, 'Proyecto "Mapa de Conocimiento y Vinculación de Egresado de la Facultad de Bioética de la Universidad Anáhuac México Norte" en Universidad Anáhuac México Norte	', '', '', NULL, NULL),
(14, 'Clases de Ética y Bioética en Pregrado en Universidad Anáhuac Sur	', '', '', NULL, NULL),
(15, 'Proyecto "Mapa de Conocimiento y Vinculación de Egresado de la Facultad de Bioética de la Universidad Anáhuac México Norte" en Universidad Anáhuac México Norte	', '', '', NULL, NULL),
(16, 'Clases de Ética y Bioética en Pregrado en Universidad Anáhuac Sur	', '', '', NULL, NULL),
(17, 'Trabajo de planta en la Facultad de Bioética	', '', '', NULL, NULL),
(18, 'Profesor de la Maestría en Bioética y profesor de los programas de extensión de la Faucltad	', '', '', NULL, NULL),
(19, 'Elaboración de un proyecto de NOM.', '', '', NULL, NULL),
(20, 'Curso "Medicina Humanista" de la Universidad Pontificia, área de Bioética	', '', '', NULL, NULL),
(21, 'Elaboración de un proyecto de NOM.', '', '', NULL, NULL),
(22, 'Curso "Medicina Humanista" de la Universidad Pontificia, área de Bioética	', '', '', NULL, NULL),
(23, 'Sistematizar el proceso de titulación de alumnos del Doctorado en Bioética para alcanzar altos indices de eficiencia terminal y titulación. Gestión del proyecto de llevar el Doctorado en Bioética a la ciudad de Morelia, Michoacán.	', '', '', NULL, NULL),
(24, 'Metodología de la Investigación-Doctorado en Bioética Análisis e Interpretación de Datos Estadísticos-Doctorado en Bioética Seminario de Investigación Documental en Bioética-Maestría en Bioética Diseño de la Investigación-Maestría en Bioética Seminario de', '', '', NULL, NULL),
(25, 'Sistematizar el proceso de titulación de alumnos del Doctorado en Bioética para alcanzar altos indices de eficiencia terminal y titulación. Gestión del proyecto de llevar el Doctorado en Bioética a la ciudad de Morelia, Michoacán.	', '', '', NULL, NULL),
(26, 'Metodología de la Investigación-Doctorado en Bioética Análisis e Interpretación de Datos Estadísticos-Doctorado en Bioética Seminario de Investigación Documental en Bioética-Maestría en Bioética Diseño de la Investigación-Maestría en Bioética Seminario de', '', '', NULL, NULL),
(27, '', '', '', NULL, NULL),
(28, '', '', '', NULL, NULL),
(29, '', '', '', NULL, NULL),
(30, '', '', '', NULL, NULL),
(31, 'Sexualidad, educación, ética, bioética, teología del cuerpo, antropología de la sexualidad , fertilidad	', '', '', NULL, NULL),
(32, 'Etica y bioética Bioética General', 'Bioética Aplicada  Sexualidad y Reproducción', 'Antropología de la sexualidad Sexualidad y Reproducción Teología del cuerpo	', NULL, NULL),
(33, 'Sexualidad, educación, ética, bioética, teología del cuerpo, antropología de la sexualidad , fertilidad	', '', '', NULL, NULL),
(34, 'Etica y bioética Bioética General', 'Bioética Aplicada  Sexualidad y Reproducción', 'Antropología de la sexualidad Sexualidad y Reproducción Teología del cuerpo	', NULL, NULL),
(35, 'Formación, Registro y Funcionamiento del Comité Hospitalario de Bioética del ISSSTE estatal Zacatecas	', '', '', NULL, NULL),
(36, 'Bioética y Biotecnología Bioética y Medio Ambiente	', '', '', NULL, NULL),
(37, 'Formación, Registro y Funcionamiento del Comité Hospitalario de Bioética del ISSSTE estatal Zacatecas	', '', '', NULL, NULL),
(38, 'Bioética y Biotecnología Bioética y Medio Ambiente	', '', '', NULL, NULL),
(39, 'Lenguaje y consentimiento informado	', '', '', NULL, NULL),
(40, 'temas actuales de bioética, comités de bioética ética y bioética ética en la salud universidad Anáhuac norte	', '', '', NULL, NULL),
(41, 'Lenguaje y consentimiento informado	', '', '', NULL, NULL),
(42, 'temas actuales de bioética, comités de bioética ética y bioética ética en la salud universidad Anáhuac norte	', '', '', NULL, NULL),
(43, '', '', '', NULL, NULL),
(44, 'MATERIA DE ÉTICA DE LA SALUD EN LA FACULTAD DE CIENCIAS DE LA SALUD EN UNIVERSIDAD ANÁHUAC SUR	', '', '', NULL, NULL),
(45, '', '', '', NULL, NULL),
(46, 'MATERIA DE ÉTICA DE LA SALUD EN LA FACULTAD DE CIENCIAS DE LA SALUD EN UNIVERSIDAD ANÁHUAC SUR	', '', '', NULL, NULL),
(47, '', '', '', NULL, NULL),
(48, '', '', '', NULL, NULL),
(49, '', '', '', NULL, NULL),
(50, '', '', '', NULL, NULL),
(51, 'Consultoría en Bioética Clínica. CBA Consultoría en Bioética Aplicada	', '', '', NULL, NULL),
(52, 'Docencia a niveles licenciatura,maestría y doctorado. Licenciatura: "Dignidad y espiritualidad". Maestría: "Fundamentos éticos de la Bioética", "Antropología filosófica", "Bioética en el final de la vida", ', '"Aspectos filosóficos de la Bioética" y "Bioética" en programas de la Fac. de Humanidades. Doctorado: Temas selectos de bioética Clínica"', 'Universidad Anáhuac Norte. Licenciatura: "Ética". Maestría: "Temas selectos de Bioética". Universidad Panamericana. Licenciatura: "Fundamentos de Bioética y Bioética Aplicada". Instituto superior de estudios para la familia.', NULL, NULL),
(53, 'Consultoría en Bioética Clínica. CBA Consultoría en Bioética Aplicada	', '', '', NULL, NULL),
(54, 'Docencia a niveles licenciatura,maestría y doctorado. Licenciatura: "Dignidad y espiritualidad". Maestría: "Fundamentos éticos de la Bioética", "Antropología filosófica", "Bioética en el final de la vida", ', '"Aspectos filosóficos de la Bioética" y "Bioética" en programas de la Fac. de Humanidades. Doctorado: Temas selectos de bioética Clínica"', 'Universidad Anáhuac Norte. Licenciatura: "Ética". Maestría: "Temas selectos de Bioética". Universidad Panamericana. Licenciatura: "Fundamentos de Bioética y Bioética Aplicada". Instituto superior de estudios para la familia.', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `perfil_id` int(11) NOT NULL,
  `id_vinculacion` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `perfil_id`, `id_vinculacion`, `status`, `password`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'SuperAdmin', 'demo@gmail.com', 1, 0, 1, '$2y$10$HN9JIq/hbye27AAOb/XX5.wcgKNSbyJRbn0XiODXy4lw/m706d9AK', '3ciD6JBUA0I10S2LL3W5vpUv3E3M682AcbGJ5GVUDjwV0iDehwdPnNIOEu21', NULL, '2017-03-10 21:29:16', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vinculacions`
--

CREATE TABLE `vinculacions` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombreCompleto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_perfil` int(11) NOT NULL,
  `id_status_formacion` int(11) NOT NULL,
  `formAcademicLIC` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_area_LIC` int(11) NOT NULL,
  `formdeMaestria` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `formEspecialidad` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `formDoctorado` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `formPostGrado` text COLLATE utf8_unicode_ci,
  `id_status_gestion_laboral` int(11) NOT NULL,
  `id_respuestas_gestion_laboral` int(11) DEFAULT NULL,
  `id_status_form_docencia` int(11) NOT NULL,
  `id_respuestas_form_docencia` int(11) DEFAULT NULL,
  `id_status_invest_bio` int(11) NOT NULL,
  `investigacion_temas` text COLLATE utf8_unicode_ci,
  `telefono` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefono2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `correo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `correo2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `palabras_claves` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `vinculacions`
--

INSERT INTO `vinculacions` (`id`, `nombreCompleto`, `id_perfil`, `id_status_formacion`, `formAcademicLIC`, `id_area_LIC`, `formdeMaestria`, `formEspecialidad`, `formDoctorado`, `formPostGrado`, `id_status_gestion_laboral`, `id_respuestas_gestion_laboral`, `id_status_form_docencia`, `id_respuestas_form_docencia`, `id_status_invest_bio`, `investigacion_temas`, `telefono`, `telefono2`, `correo`, `correo2`, `palabras_claves`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Martha Tarasco Michel', 2, 5, 'Medicina', 1, 'Investigacion clinica', 'Medicina de la comunicación humana', 'Medicina', NULL, 8, 1, 8, 2, 8, NULL, '56270211 ext 8319', '', 'mtarascos@anahuac.mx', '', 'Antropologia_filosogica, epistemologia,conducta', NULL, NULL, NULL),
(2, 'Mariel Kalkach Aparicio', 3, 6, 'Medicina', 1, 'Bioetica', 'Bioetica', '', NULL, 8, 13, 8, 14, 8, NULL, '555-5555555', '', 'mariel.kalkac@gmail.com', '', 'neuroetica,conciencia,neurobioetica', NULL, NULL, NULL),
(4, 'Oscar Javier Martínez González', 4, 6, 'Medicina', 1, 'Bioetica', 'Oftalmología pediátrica	', 'Estudios completos de doctorado en Bioética	', NULL, 8, 17, 8, 18, 8, NULL, '222-22222', '', 'ojmarti@anahuac.mx', '', 'profesor', NULL, NULL, NULL),
(5, 'Ellvira Llaca García', 3, 6, 'Medicina', 2, 'Bioética', 'Oftalmologia', 'Bioética', NULL, 10, 19, 8, 20, 8, NULL, '5544887907', '', 'elvirallaca27@gmail.com', '', 'persona, bioetica', NULL, NULL, NULL),
(7, 'Felipe de Jesús Vargas Mota', 4, 7, 'Administrador', 3, 'Maestría en ciencias de la administración / Maestría en mercadotecnia	', 'Habilidades gerenciales	', 'Doctorado en bioética	', NULL, 9, 23, 8, 24, 8, NULL, '111-11111', '', 'felipe.varga@anahuac.mx', '', 'Doctorado en bioetica', NULL, NULL, NULL),
(9, 'Martha Patricia Hernández Valdez', 3, 6, 'Psicologa', 1, 'Bioética', 'Bioética', '', NULL, 8, 27, 9, 28, 8, NULL, '55 22 61 73 94	', '', 'patricia.hernandezv@hotmail.es', 'martha.hernandez@anahuac.mx', 'psicobioetica', NULL, NULL, NULL),
(11, 'Maria Teresa Garcia', 3, 6, 'Ingeniaria', 6, 'Bioética', 'Consultoría familiar	', 'Bioética', NULL, 8, 31, 8, 32, 8, NULL, '000000000', '', 'mariateresa@gmail.com', '', 'En Cátedras de investigación en la facultad de bioetica', NULL, NULL, NULL),
(13, 'Antonio Muñoz Torres', 3, 6, 'BióLogo Modular	', 1, 'Maestría en Bioética	', 'Doctorado en Bioética', '', NULL, 8, 35, 8, 36, 8, NULL, '56 27 02 11 ext 7239', '', 'antonio.muno@anahuac.mx', '', 'bioetecnologia, ética_medioambiental', NULL, NULL, NULL),
(15, 'Samuel Weingerz Mehl', 3, 6, 'Medicina', 1, 'Bioética', 'Ginecologia y obstetricia	', 'Bioética', NULL, 8, 39, 8, 40, 8, NULL, '999-9999', '', 'bioeticaweingerxz@gmail.com', 'samuel.weingerxz@anahuac.mx', 'comites hospitalarios', NULL, NULL, NULL),
(17, 'Germán Carreto Chávez', 3, 6, 'Médico Cirujano	', 1, '', 'Ginecología y obstetricia, medicina materno fetal	', '', NULL, 8, 43, 8, 44, 8, NULL, '55-15-9521', '', 'germancerre@hotmail.com', '', 'Diagnostico prenatal, Bioética e inicio de la vida', NULL, NULL, NULL),
(19, 'Thelma Elena Peón Hernandez', 3, 6, 'Relaciones Industriales', 6, 'Maestría  En Filosofía', '', 'Estudiando el doctorado en Filosofía	', NULL, 8, 47, 8, 48, 8, NULL, '333-33333', '', 'thelma.p@anahuac.mx', '', 'Transhumanismo, enhacement', NULL, NULL, NULL),
(21, 'Ma. Elizabeth de los Rios Uriarte	', 2, 5, 'Filosofía', 5, '', '', 'Filosofía', NULL, 8, 51, 8, 52, 8, NULL, '445526535742', '', 'marieli828@hotmail.com', 'mdlrios@hotmail.com', 'Bioética Clinica,investigación', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vinculacions_ambitos`
--

CREATE TABLE `vinculacions_ambitos` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_vinculacions` int(11) NOT NULL,
  `id_ambitos` int(11) NOT NULL,
  `id_tipo` int(11) NOT NULL,
  `cheked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `vinculacions_ambitos`
--

INSERT INTO `vinculacions_ambitos` (`id`, `id_vinculacions`, `id_ambitos`, `id_tipo`, `cheked`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 11, 1, '2017-03-10 07:16:04', '2017-03-10 07:16:04'),
(2, 1, 2, 11, 0, '2017-03-10 07:16:04', '2017-03-10 07:16:04'),
(3, 1, 3, 11, 0, '2017-03-10 07:16:04', '2017-03-10 07:16:04'),
(4, 1, 4, 11, 0, '2017-03-10 07:16:04', '2017-03-10 07:16:04'),
(5, 1, 5, 11, 0, '2017-03-10 07:16:04', '2017-03-10 07:16:04'),
(6, 1, 6, 11, 0, '2017-03-10 07:16:04', '2017-03-10 07:16:04'),
(7, 1, 7, 11, 0, '2017-03-10 07:16:04', '2017-03-10 07:16:04'),
(8, 1, 8, 11, 0, '2017-03-10 07:16:04', '2017-03-10 07:16:04'),
(9, 1, 9, 11, 0, '2017-03-10 07:16:04', '2017-03-10 07:16:04'),
(10, 1, 10, 11, 0, '2017-03-10 07:16:04', '2017-03-10 07:16:04'),
(11, 1, 11, 11, 0, '2017-03-10 07:16:04', '2017-03-10 07:16:04'),
(12, 1, 12, 11, 0, '2017-03-10 07:16:04', '2017-03-10 07:16:04'),
(13, 1, 1, 12, 1, '2017-03-10 07:16:05', '2017-03-10 07:16:05'),
(14, 1, 2, 12, 0, '2017-03-10 07:16:05', '2017-03-10 07:16:05'),
(15, 1, 3, 12, 0, '2017-03-10 07:16:05', '2017-03-10 07:16:05'),
(16, 1, 4, 12, 0, '2017-03-10 07:16:05', '2017-03-10 07:16:05'),
(17, 1, 5, 12, 0, '2017-03-10 07:16:05', '2017-03-10 07:16:05'),
(18, 1, 6, 12, 0, '2017-03-10 07:16:05', '2017-03-10 07:16:05'),
(19, 1, 7, 12, 0, '2017-03-10 07:16:05', '2017-03-10 07:16:05'),
(20, 1, 8, 12, 0, '2017-03-10 07:16:05', '2017-03-10 07:16:05'),
(21, 1, 9, 12, 0, '2017-03-10 07:16:05', '2017-03-10 07:16:05'),
(22, 1, 10, 12, 0, '2017-03-10 07:16:05', '2017-03-10 07:16:05'),
(23, 1, 11, 12, 0, '2017-03-10 07:16:05', '2017-03-10 07:16:05'),
(24, 1, 12, 12, 0, '2017-03-10 07:16:05', '2017-03-10 07:16:05'),
(25, 1, 1, 13, 1, '2017-03-10 07:16:05', '2017-03-10 07:16:05'),
(26, 1, 2, 13, 0, '2017-03-10 07:16:05', '2017-03-10 07:16:05'),
(27, 1, 3, 13, 0, '2017-03-10 07:16:05', '2017-03-10 07:16:05'),
(28, 1, 4, 13, 0, '2017-03-10 07:16:05', '2017-03-10 07:16:05'),
(29, 1, 5, 13, 0, '2017-03-10 07:16:05', '2017-03-10 07:16:05'),
(30, 1, 6, 13, 0, '2017-03-10 07:16:05', '2017-03-10 07:16:05'),
(31, 1, 7, 13, 0, '2017-03-10 07:16:05', '2017-03-10 07:16:05'),
(32, 1, 8, 13, 0, '2017-03-10 07:16:05', '2017-03-10 07:16:05'),
(33, 1, 9, 13, 0, '2017-03-10 07:16:05', '2017-03-10 07:16:05'),
(34, 1, 10, 13, 0, '2017-03-10 07:16:05', '2017-03-10 07:16:05'),
(35, 1, 11, 13, 0, '2017-03-10 07:16:05', '2017-03-10 07:16:05'),
(36, 1, 12, 13, 0, '2017-03-10 07:16:05', '2017-03-10 07:16:05'),
(37, 2, 1, 11, 0, '2017-03-10 19:42:51', '2017-03-10 19:42:51'),
(38, 2, 2, 11, 0, '2017-03-10 19:42:51', '2017-03-10 19:42:51'),
(39, 2, 3, 11, 0, '2017-03-10 19:42:51', '2017-03-10 19:42:51'),
(40, 2, 4, 11, 0, '2017-03-10 19:42:51', '2017-03-10 19:42:51'),
(41, 2, 5, 11, 0, '2017-03-10 19:42:51', '2017-03-10 19:42:51'),
(42, 2, 6, 11, 0, '2017-03-10 19:42:51', '2017-03-10 19:42:51'),
(43, 2, 7, 11, 1, '2017-03-10 19:42:51', '2017-03-10 19:42:51'),
(44, 2, 8, 11, 0, '2017-03-10 19:42:51', '2017-03-10 19:42:51'),
(45, 2, 9, 11, 0, '2017-03-10 19:42:52', '2017-03-10 19:42:52'),
(46, 2, 10, 11, 0, '2017-03-10 19:42:52', '2017-03-10 19:42:52'),
(47, 2, 11, 11, 0, '2017-03-10 19:42:52', '2017-03-10 19:42:52'),
(48, 2, 12, 11, 0, '2017-03-10 19:42:52', '2017-03-10 19:42:52'),
(49, 2, 1, 12, 1, '2017-03-10 19:42:52', '2017-03-10 19:42:52'),
(50, 2, 2, 12, 0, '2017-03-10 19:42:52', '2017-03-10 19:42:52'),
(51, 2, 3, 12, 0, '2017-03-10 19:42:52', '2017-03-10 19:42:52'),
(52, 2, 4, 12, 0, '2017-03-10 19:42:52', '2017-03-10 19:42:52'),
(53, 2, 5, 12, 0, '2017-03-10 19:42:52', '2017-03-10 19:42:52'),
(54, 2, 6, 12, 0, '2017-03-10 19:42:52', '2017-03-10 19:42:52'),
(55, 2, 7, 12, 0, '2017-03-10 19:42:52', '2017-03-10 19:42:52'),
(56, 2, 8, 12, 0, '2017-03-10 19:42:52', '2017-03-10 19:42:52'),
(57, 2, 9, 12, 0, '2017-03-10 19:42:52', '2017-03-10 19:42:52'),
(58, 2, 10, 12, 0, '2017-03-10 19:42:52', '2017-03-10 19:42:52'),
(59, 2, 11, 12, 0, '2017-03-10 19:42:52', '2017-03-10 19:42:52'),
(60, 2, 12, 12, 0, '2017-03-10 19:42:52', '2017-03-10 19:42:52'),
(61, 2, 1, 13, 1, '2017-03-10 19:42:52', '2017-03-10 19:42:52'),
(62, 2, 2, 13, 0, '2017-03-10 19:42:52', '2017-03-10 19:42:52'),
(63, 2, 3, 13, 0, '2017-03-10 19:42:52', '2017-03-10 19:42:52'),
(64, 2, 4, 13, 0, '2017-03-10 19:42:52', '2017-03-10 19:42:52'),
(65, 2, 5, 13, 0, '2017-03-10 19:42:52', '2017-03-10 19:42:52'),
(66, 2, 6, 13, 0, '2017-03-10 19:42:52', '2017-03-10 19:42:52'),
(67, 2, 7, 13, 0, '2017-03-10 19:42:52', '2017-03-10 19:42:52'),
(68, 2, 8, 13, 0, '2017-03-10 19:42:52', '2017-03-10 19:42:52'),
(69, 2, 9, 13, 0, '2017-03-10 19:42:52', '2017-03-10 19:42:52'),
(70, 2, 10, 13, 0, '2017-03-10 19:42:52', '2017-03-10 19:42:52'),
(71, 2, 11, 13, 0, '2017-03-10 19:42:52', '2017-03-10 19:42:52'),
(72, 2, 12, 13, 0, '2017-03-10 19:42:52', '2017-03-10 19:42:52'),
(73, 4, 1, 11, 1, '2017-03-10 19:49:35', '2017-03-10 19:49:35'),
(74, 4, 2, 11, 0, '2017-03-10 19:49:35', '2017-03-10 19:49:35'),
(75, 4, 3, 11, 0, '2017-03-10 19:49:35', '2017-03-10 19:49:35'),
(76, 4, 4, 11, 0, '2017-03-10 19:49:35', '2017-03-10 19:49:35'),
(77, 4, 5, 11, 0, '2017-03-10 19:49:35', '2017-03-10 19:49:35'),
(78, 4, 6, 11, 0, '2017-03-10 19:49:35', '2017-03-10 19:49:35'),
(79, 4, 7, 11, 0, '2017-03-10 19:49:35', '2017-03-10 19:49:35'),
(80, 4, 8, 11, 0, '2017-03-10 19:49:35', '2017-03-10 19:49:35'),
(81, 4, 9, 11, 0, '2017-03-10 19:49:36', '2017-03-10 19:49:36'),
(82, 4, 10, 11, 0, '2017-03-10 19:49:36', '2017-03-10 19:49:36'),
(83, 4, 11, 11, 0, '2017-03-10 19:49:36', '2017-03-10 19:49:36'),
(84, 4, 12, 11, 0, '2017-03-10 19:49:36', '2017-03-10 19:49:36'),
(85, 4, 1, 12, 1, '2017-03-10 19:49:36', '2017-03-10 19:49:36'),
(86, 4, 2, 12, 0, '2017-03-10 19:49:36', '2017-03-10 19:49:36'),
(87, 4, 3, 12, 0, '2017-03-10 19:49:36', '2017-03-10 19:49:36'),
(88, 4, 4, 12, 0, '2017-03-10 19:49:36', '2017-03-10 19:49:36'),
(89, 4, 5, 12, 0, '2017-03-10 19:49:36', '2017-03-10 19:49:36'),
(90, 4, 6, 12, 0, '2017-03-10 19:49:36', '2017-03-10 19:49:36'),
(91, 4, 7, 12, 0, '2017-03-10 19:49:36', '2017-03-10 19:49:36'),
(92, 4, 8, 12, 0, '2017-03-10 19:49:36', '2017-03-10 19:49:36'),
(93, 4, 9, 12, 0, '2017-03-10 19:49:36', '2017-03-10 19:49:36'),
(94, 4, 10, 12, 0, '2017-03-10 19:49:36', '2017-03-10 19:49:36'),
(95, 4, 11, 12, 0, '2017-03-10 19:49:36', '2017-03-10 19:49:36'),
(96, 4, 12, 12, 0, '2017-03-10 19:49:36', '2017-03-10 19:49:36'),
(97, 4, 1, 13, 0, '2017-03-10 19:49:36', '2017-03-10 19:49:36'),
(98, 4, 2, 13, 0, '2017-03-10 19:49:36', '2017-03-10 19:49:36'),
(99, 4, 3, 13, 0, '2017-03-10 19:49:36', '2017-03-10 19:49:36'),
(100, 4, 4, 13, 0, '2017-03-10 19:49:36', '2017-03-10 19:49:36'),
(101, 4, 5, 13, 1, '2017-03-10 19:49:36', '2017-03-10 19:49:36'),
(102, 4, 6, 13, 0, '2017-03-10 19:49:36', '2017-03-10 19:49:36'),
(103, 4, 7, 13, 0, '2017-03-10 19:49:36', '2017-03-10 19:49:36'),
(104, 4, 8, 13, 0, '2017-03-10 19:49:36', '2017-03-10 19:49:36'),
(105, 4, 9, 13, 0, '2017-03-10 19:49:36', '2017-03-10 19:49:36'),
(106, 4, 10, 13, 0, '2017-03-10 19:49:36', '2017-03-10 19:49:36'),
(107, 4, 11, 13, 0, '2017-03-10 19:49:36', '2017-03-10 19:49:36'),
(108, 4, 12, 13, 0, '2017-03-10 19:49:36', '2017-03-10 19:49:36'),
(109, 5, 1, 11, 0, '2017-03-10 19:53:17', '2017-03-10 19:53:17'),
(110, 5, 2, 11, 0, '2017-03-10 19:53:17', '2017-03-10 19:53:17'),
(111, 5, 3, 11, 0, '2017-03-10 19:53:18', '2017-03-10 19:53:18'),
(112, 5, 4, 11, 0, '2017-03-10 19:53:18', '2017-03-10 19:53:18'),
(113, 5, 5, 11, 0, '2017-03-10 19:53:18', '2017-03-10 19:53:18'),
(114, 5, 6, 11, 0, '2017-03-10 19:53:18', '2017-03-10 19:53:18'),
(115, 5, 7, 11, 0, '2017-03-10 19:53:18', '2017-03-10 19:53:18'),
(116, 5, 8, 11, 0, '2017-03-10 19:53:18', '2017-03-10 19:53:18'),
(117, 5, 9, 11, 1, '2017-03-10 19:53:18', '2017-03-10 19:53:18'),
(118, 5, 10, 11, 0, '2017-03-10 19:53:18', '2017-03-10 19:53:18'),
(119, 5, 11, 11, 0, '2017-03-10 19:53:18', '2017-03-10 19:53:18'),
(120, 5, 12, 11, 0, '2017-03-10 19:53:18', '2017-03-10 19:53:18'),
(121, 5, 1, 12, 0, '2017-03-10 19:53:18', '2017-03-10 19:53:18'),
(122, 5, 2, 12, 1, '2017-03-10 19:53:18', '2017-03-10 19:53:18'),
(123, 5, 3, 12, 0, '2017-03-10 19:53:18', '2017-03-10 19:53:18'),
(124, 5, 4, 12, 0, '2017-03-10 19:53:18', '2017-03-10 19:53:18'),
(125, 5, 5, 12, 0, '2017-03-10 19:53:18', '2017-03-10 19:53:18'),
(126, 5, 6, 12, 0, '2017-03-10 19:53:18', '2017-03-10 19:53:18'),
(127, 5, 7, 12, 0, '2017-03-10 19:53:18', '2017-03-10 19:53:18'),
(128, 5, 8, 12, 0, '2017-03-10 19:53:18', '2017-03-10 19:53:18'),
(129, 5, 9, 12, 0, '2017-03-10 19:53:18', '2017-03-10 19:53:18'),
(130, 5, 10, 12, 0, '2017-03-10 19:53:18', '2017-03-10 19:53:18'),
(131, 5, 11, 12, 0, '2017-03-10 19:53:18', '2017-03-10 19:53:18'),
(132, 5, 12, 12, 0, '2017-03-10 19:53:18', '2017-03-10 19:53:18'),
(133, 7, 1, 11, 0, '2017-03-10 19:56:18', '2017-03-10 19:56:18'),
(134, 7, 2, 11, 0, '2017-03-10 19:56:18', '2017-03-10 19:56:18'),
(135, 7, 3, 11, 0, '2017-03-10 19:56:18', '2017-03-10 19:56:18'),
(136, 7, 4, 11, 0, '2017-03-10 19:56:18', '2017-03-10 19:56:18'),
(137, 7, 5, 11, 0, '2017-03-10 19:56:18', '2017-03-10 19:56:18'),
(138, 7, 6, 11, 0, '2017-03-10 19:56:18', '2017-03-10 19:56:18'),
(139, 7, 7, 11, 0, '2017-03-10 19:56:18', '2017-03-10 19:56:18'),
(140, 7, 8, 11, 0, '2017-03-10 19:56:18', '2017-03-10 19:56:18'),
(141, 7, 9, 11, 0, '2017-03-10 19:56:18', '2017-03-10 19:56:18'),
(142, 7, 10, 11, 1, '2017-03-10 19:56:18', '2017-03-10 19:56:18'),
(143, 7, 11, 11, 0, '2017-03-10 19:56:18', '2017-03-10 19:56:18'),
(144, 7, 12, 11, 0, '2017-03-10 19:56:18', '2017-03-10 19:56:18'),
(145, 7, 1, 12, 1, '2017-03-10 19:56:18', '2017-03-10 19:56:18'),
(146, 7, 2, 12, 0, '2017-03-10 19:56:18', '2017-03-10 19:56:18'),
(147, 7, 3, 12, 0, '2017-03-10 19:56:18', '2017-03-10 19:56:18'),
(148, 7, 4, 12, 0, '2017-03-10 19:56:18', '2017-03-10 19:56:18'),
(149, 7, 5, 12, 0, '2017-03-10 19:56:18', '2017-03-10 19:56:18'),
(150, 7, 6, 12, 0, '2017-03-10 19:56:18', '2017-03-10 19:56:18'),
(151, 7, 7, 12, 0, '2017-03-10 19:56:18', '2017-03-10 19:56:18'),
(152, 7, 8, 12, 0, '2017-03-10 19:56:18', '2017-03-10 19:56:18'),
(153, 7, 9, 12, 0, '2017-03-10 19:56:18', '2017-03-10 19:56:18'),
(154, 7, 10, 12, 0, '2017-03-10 19:56:18', '2017-03-10 19:56:18'),
(155, 7, 11, 12, 0, '2017-03-10 19:56:18', '2017-03-10 19:56:18'),
(156, 7, 12, 12, 0, '2017-03-10 19:56:18', '2017-03-10 19:56:18'),
(157, 7, 1, 13, 1, '2017-03-10 19:56:18', '2017-03-10 19:56:18'),
(158, 7, 2, 13, 0, '2017-03-10 19:56:18', '2017-03-10 19:56:18'),
(159, 7, 3, 13, 0, '2017-03-10 19:56:19', '2017-03-10 19:56:19'),
(160, 7, 4, 13, 0, '2017-03-10 19:56:19', '2017-03-10 19:56:19'),
(161, 7, 5, 13, 0, '2017-03-10 19:56:19', '2017-03-10 19:56:19'),
(162, 7, 6, 13, 0, '2017-03-10 19:56:19', '2017-03-10 19:56:19'),
(163, 7, 7, 13, 0, '2017-03-10 19:56:19', '2017-03-10 19:56:19'),
(164, 7, 8, 13, 0, '2017-03-10 19:56:19', '2017-03-10 19:56:19'),
(165, 7, 9, 13, 0, '2017-03-10 19:56:19', '2017-03-10 19:56:19'),
(166, 7, 10, 13, 0, '2017-03-10 19:56:19', '2017-03-10 19:56:19'),
(167, 7, 11, 13, 0, '2017-03-10 19:56:19', '2017-03-10 19:56:19'),
(168, 7, 12, 13, 0, '2017-03-10 19:56:19', '2017-03-10 19:56:19'),
(169, 11, 1, 13, 1, '2017-03-10 20:04:54', '2017-03-10 20:04:54'),
(170, 11, 2, 13, 0, '2017-03-10 20:04:54', '2017-03-10 20:04:54'),
(171, 11, 3, 13, 0, '2017-03-10 20:04:54', '2017-03-10 20:04:54'),
(172, 11, 4, 13, 0, '2017-03-10 20:04:54', '2017-03-10 20:04:54'),
(173, 11, 5, 13, 0, '2017-03-10 20:04:54', '2017-03-10 20:04:54'),
(174, 11, 6, 13, 0, '2017-03-10 20:04:54', '2017-03-10 20:04:54'),
(175, 11, 7, 13, 0, '2017-03-10 20:04:54', '2017-03-10 20:04:54'),
(176, 11, 8, 13, 0, '2017-03-10 20:04:54', '2017-03-10 20:04:54'),
(177, 11, 9, 13, 0, '2017-03-10 20:04:54', '2017-03-10 20:04:54'),
(178, 11, 10, 13, 0, '2017-03-10 20:04:54', '2017-03-10 20:04:54'),
(179, 11, 11, 13, 0, '2017-03-10 20:04:54', '2017-03-10 20:04:54'),
(180, 11, 12, 13, 0, '2017-03-10 20:04:54', '2017-03-10 20:04:54'),
(181, 13, 1, 11, 0, '2017-03-10 20:08:31', '2017-03-10 20:08:31'),
(182, 13, 2, 11, 0, '2017-03-10 20:08:31', '2017-03-10 20:08:31'),
(183, 13, 3, 11, 0, '2017-03-10 20:08:31', '2017-03-10 20:08:31'),
(184, 13, 4, 11, 0, '2017-03-10 20:08:31', '2017-03-10 20:08:31'),
(185, 13, 5, 11, 1, '2017-03-10 20:08:31', '2017-03-10 20:08:31'),
(186, 13, 6, 11, 0, '2017-03-10 20:08:31', '2017-03-10 20:08:31'),
(187, 13, 7, 11, 0, '2017-03-10 20:08:31', '2017-03-10 20:08:31'),
(188, 13, 8, 11, 0, '2017-03-10 20:08:31', '2017-03-10 20:08:31'),
(189, 13, 9, 11, 0, '2017-03-10 20:08:31', '2017-03-10 20:08:31'),
(190, 13, 10, 11, 0, '2017-03-10 20:08:32', '2017-03-10 20:08:32'),
(191, 13, 11, 11, 0, '2017-03-10 20:08:32', '2017-03-10 20:08:32'),
(192, 13, 12, 11, 0, '2017-03-10 20:08:32', '2017-03-10 20:08:32'),
(193, 13, 1, 12, 1, '2017-03-10 20:08:32', '2017-03-10 20:08:32'),
(194, 13, 2, 12, 0, '2017-03-10 20:08:32', '2017-03-10 20:08:32'),
(195, 13, 3, 12, 0, '2017-03-10 20:08:32', '2017-03-10 20:08:32'),
(196, 13, 4, 12, 0, '2017-03-10 20:08:32', '2017-03-10 20:08:32'),
(197, 13, 5, 12, 0, '2017-03-10 20:08:32', '2017-03-10 20:08:32'),
(198, 13, 6, 12, 0, '2017-03-10 20:08:32', '2017-03-10 20:08:32'),
(199, 13, 7, 12, 0, '2017-03-10 20:08:32', '2017-03-10 20:08:32'),
(200, 13, 8, 12, 0, '2017-03-10 20:08:32', '2017-03-10 20:08:32'),
(201, 13, 9, 12, 0, '2017-03-10 20:08:32', '2017-03-10 20:08:32'),
(202, 13, 10, 12, 0, '2017-03-10 20:08:32', '2017-03-10 20:08:32'),
(203, 13, 11, 12, 0, '2017-03-10 20:08:32', '2017-03-10 20:08:32'),
(204, 13, 12, 12, 0, '2017-03-10 20:08:32', '2017-03-10 20:08:32'),
(205, 13, 1, 13, 0, '2017-03-10 20:08:32', '2017-03-10 20:08:32'),
(206, 13, 2, 13, 0, '2017-03-10 20:08:32', '2017-03-10 20:08:32'),
(207, 13, 3, 13, 0, '2017-03-10 20:08:32', '2017-03-10 20:08:32'),
(208, 13, 4, 13, 0, '2017-03-10 20:08:32', '2017-03-10 20:08:32'),
(209, 13, 5, 13, 0, '2017-03-10 20:08:32', '2017-03-10 20:08:32'),
(210, 13, 6, 13, 0, '2017-03-10 20:08:32', '2017-03-10 20:08:32'),
(211, 13, 7, 13, 0, '2017-03-10 20:08:32', '2017-03-10 20:08:32'),
(212, 13, 8, 13, 0, '2017-03-10 20:08:32', '2017-03-10 20:08:32'),
(213, 13, 9, 13, 0, '2017-03-10 20:08:32', '2017-03-10 20:08:32'),
(214, 13, 10, 13, 1, '2017-03-10 20:08:32', '2017-03-10 20:08:32'),
(215, 13, 11, 13, 0, '2017-03-10 20:08:32', '2017-03-10 20:08:32'),
(216, 13, 12, 13, 0, '2017-03-10 20:08:32', '2017-03-10 20:08:32'),
(217, 15, 1, 11, 0, '2017-03-10 20:12:17', '2017-03-10 20:12:17'),
(218, 15, 2, 11, 0, '2017-03-10 20:12:17', '2017-03-10 20:12:17'),
(219, 15, 3, 11, 0, '2017-03-10 20:12:17', '2017-03-10 20:12:17'),
(220, 15, 4, 11, 1, '2017-03-10 20:12:17', '2017-03-10 20:12:17'),
(221, 15, 5, 11, 0, '2017-03-10 20:12:17', '2017-03-10 20:12:17'),
(222, 15, 6, 11, 0, '2017-03-10 20:12:17', '2017-03-10 20:12:17'),
(223, 15, 7, 11, 0, '2017-03-10 20:12:17', '2017-03-10 20:12:17'),
(224, 15, 8, 11, 0, '2017-03-10 20:12:17', '2017-03-10 20:12:17'),
(225, 15, 9, 11, 0, '2017-03-10 20:12:17', '2017-03-10 20:12:17'),
(226, 15, 10, 11, 0, '2017-03-10 20:12:17', '2017-03-10 20:12:17'),
(227, 15, 11, 11, 0, '2017-03-10 20:12:17', '2017-03-10 20:12:17'),
(228, 15, 12, 11, 0, '2017-03-10 20:12:17', '2017-03-10 20:12:17'),
(229, 15, 1, 12, 0, '2017-03-10 20:12:17', '2017-03-10 20:12:17'),
(230, 15, 2, 12, 0, '2017-03-10 20:12:18', '2017-03-10 20:12:18'),
(231, 15, 3, 12, 0, '2017-03-10 20:12:18', '2017-03-10 20:12:18'),
(232, 15, 4, 12, 0, '2017-03-10 20:12:18', '2017-03-10 20:12:18'),
(233, 15, 5, 12, 1, '2017-03-10 20:12:18', '2017-03-10 20:12:18'),
(234, 15, 6, 12, 0, '2017-03-10 20:12:18', '2017-03-10 20:12:18'),
(235, 15, 7, 12, 0, '2017-03-10 20:12:18', '2017-03-10 20:12:18'),
(236, 15, 8, 12, 0, '2017-03-10 20:12:18', '2017-03-10 20:12:18'),
(237, 15, 9, 12, 0, '2017-03-10 20:12:18', '2017-03-10 20:12:18'),
(238, 15, 10, 12, 0, '2017-03-10 20:12:18', '2017-03-10 20:12:18'),
(239, 15, 11, 12, 0, '2017-03-10 20:12:18', '2017-03-10 20:12:18'),
(240, 15, 12, 12, 0, '2017-03-10 20:12:18', '2017-03-10 20:12:18'),
(241, 15, 1, 13, 0, '2017-03-10 20:12:18', '2017-03-10 20:12:18'),
(242, 15, 2, 13, 0, '2017-03-10 20:12:18', '2017-03-10 20:12:18'),
(243, 15, 3, 13, 0, '2017-03-10 20:12:18', '2017-03-10 20:12:18'),
(244, 15, 4, 13, 1, '2017-03-10 20:12:18', '2017-03-10 20:12:18'),
(245, 15, 5, 13, 0, '2017-03-10 20:12:18', '2017-03-10 20:12:18'),
(246, 15, 6, 13, 0, '2017-03-10 20:12:18', '2017-03-10 20:12:18'),
(247, 15, 7, 13, 0, '2017-03-10 20:12:18', '2017-03-10 20:12:18'),
(248, 15, 8, 13, 0, '2017-03-10 20:12:18', '2017-03-10 20:12:18'),
(249, 15, 9, 13, 0, '2017-03-10 20:12:18', '2017-03-10 20:12:18'),
(250, 15, 10, 13, 0, '2017-03-10 20:12:18', '2017-03-10 20:12:18'),
(251, 15, 11, 13, 0, '2017-03-10 20:12:18', '2017-03-10 20:12:18'),
(252, 15, 12, 13, 0, '2017-03-10 20:12:18', '2017-03-10 20:12:18'),
(253, 17, 1, 12, 1, '2017-03-10 20:14:36', '2017-03-10 20:14:36'),
(254, 17, 2, 12, 0, '2017-03-10 20:14:36', '2017-03-10 20:14:36'),
(255, 17, 3, 12, 0, '2017-03-10 20:14:36', '2017-03-10 20:14:36'),
(256, 17, 4, 12, 0, '2017-03-10 20:14:36', '2017-03-10 20:14:36'),
(257, 17, 5, 12, 0, '2017-03-10 20:14:36', '2017-03-10 20:14:36'),
(258, 17, 6, 12, 0, '2017-03-10 20:14:36', '2017-03-10 20:14:36'),
(259, 17, 7, 12, 0, '2017-03-10 20:14:36', '2017-03-10 20:14:36'),
(260, 17, 8, 12, 0, '2017-03-10 20:14:36', '2017-03-10 20:14:36'),
(261, 17, 9, 12, 0, '2017-03-10 20:14:36', '2017-03-10 20:14:36'),
(262, 17, 10, 12, 0, '2017-03-10 20:14:36', '2017-03-10 20:14:36'),
(263, 17, 11, 12, 0, '2017-03-10 20:14:36', '2017-03-10 20:14:36'),
(264, 17, 12, 12, 0, '2017-03-10 20:14:36', '2017-03-10 20:14:36'),
(265, 17, 1, 13, 0, '2017-03-10 20:14:36', '2017-03-10 20:14:36'),
(266, 17, 2, 13, 1, '2017-03-10 20:14:36', '2017-03-10 20:14:36'),
(267, 17, 3, 13, 0, '2017-03-10 20:14:36', '2017-03-10 20:14:36'),
(268, 17, 4, 13, 0, '2017-03-10 20:14:36', '2017-03-10 20:14:36'),
(269, 17, 5, 13, 0, '2017-03-10 20:14:36', '2017-03-10 20:14:36'),
(270, 17, 6, 13, 0, '2017-03-10 20:14:36', '2017-03-10 20:14:36'),
(271, 17, 7, 13, 0, '2017-03-10 20:14:36', '2017-03-10 20:14:36'),
(272, 17, 8, 13, 0, '2017-03-10 20:14:36', '2017-03-10 20:14:36'),
(273, 17, 9, 13, 0, '2017-03-10 20:14:36', '2017-03-10 20:14:36'),
(274, 17, 10, 13, 0, '2017-03-10 20:14:36', '2017-03-10 20:14:36'),
(275, 17, 11, 13, 0, '2017-03-10 20:14:36', '2017-03-10 20:14:36'),
(276, 17, 12, 13, 0, '2017-03-10 20:14:36', '2017-03-10 20:14:36'),
(277, 17, 1, 13, 0, '2017-03-10 20:14:36', '2017-03-10 20:14:36'),
(278, 17, 2, 13, 0, '2017-03-10 20:14:36', '2017-03-10 20:14:36'),
(279, 17, 3, 13, 0, '2017-03-10 20:14:36', '2017-03-10 20:14:36'),
(280, 17, 4, 13, 0, '2017-03-10 20:14:36', '2017-03-10 20:14:36'),
(281, 17, 5, 13, 1, '2017-03-10 20:14:36', '2017-03-10 20:14:36'),
(282, 17, 6, 13, 0, '2017-03-10 20:14:36', '2017-03-10 20:14:36'),
(283, 17, 7, 13, 0, '2017-03-10 20:14:36', '2017-03-10 20:14:36'),
(284, 17, 8, 13, 0, '2017-03-10 20:14:36', '2017-03-10 20:14:36'),
(285, 17, 9, 13, 0, '2017-03-10 20:14:37', '2017-03-10 20:14:37'),
(286, 17, 10, 13, 0, '2017-03-10 20:14:37', '2017-03-10 20:14:37'),
(287, 17, 11, 13, 0, '2017-03-10 20:14:37', '2017-03-10 20:14:37'),
(288, 17, 12, 13, 0, '2017-03-10 20:14:37', '2017-03-10 20:14:37'),
(289, 17, 1, 13, 0, '2017-03-10 20:14:37', '2017-03-10 20:14:37'),
(290, 17, 2, 13, 0, '2017-03-10 20:14:37', '2017-03-10 20:14:37'),
(291, 17, 3, 13, 0, '2017-03-10 20:14:37', '2017-03-10 20:14:37'),
(292, 17, 4, 13, 0, '2017-03-10 20:14:37', '2017-03-10 20:14:37'),
(293, 17, 5, 13, 0, '2017-03-10 20:14:37', '2017-03-10 20:14:37'),
(294, 17, 6, 13, 0, '2017-03-10 20:14:37', '2017-03-10 20:14:37'),
(295, 17, 7, 13, 1, '2017-03-10 20:14:37', '2017-03-10 20:14:37'),
(296, 17, 8, 13, 0, '2017-03-10 20:14:37', '2017-03-10 20:14:37'),
(297, 17, 9, 13, 0, '2017-03-10 20:14:37', '2017-03-10 20:14:37'),
(298, 17, 10, 13, 0, '2017-03-10 20:14:37', '2017-03-10 20:14:37'),
(299, 17, 11, 13, 0, '2017-03-10 20:14:37', '2017-03-10 20:14:37'),
(300, 17, 12, 13, 0, '2017-03-10 20:14:37', '2017-03-10 20:14:37'),
(301, 19, 1, 12, 1, '2017-03-10 21:16:42', '2017-03-10 21:16:42'),
(302, 19, 2, 12, 0, '2017-03-10 21:16:42', '2017-03-10 21:16:42'),
(303, 19, 3, 12, 0, '2017-03-10 21:16:42', '2017-03-10 21:16:42'),
(304, 19, 4, 12, 0, '2017-03-10 21:16:42', '2017-03-10 21:16:42'),
(305, 19, 5, 12, 0, '2017-03-10 21:16:42', '2017-03-10 21:16:42'),
(306, 19, 6, 12, 0, '2017-03-10 21:16:42', '2017-03-10 21:16:42'),
(307, 19, 7, 12, 0, '2017-03-10 21:16:42', '2017-03-10 21:16:42'),
(308, 19, 8, 12, 0, '2017-03-10 21:16:42', '2017-03-10 21:16:42'),
(309, 19, 9, 12, 0, '2017-03-10 21:16:42', '2017-03-10 21:16:42'),
(310, 19, 10, 12, 0, '2017-03-10 21:16:42', '2017-03-10 21:16:42'),
(311, 19, 11, 12, 0, '2017-03-10 21:16:42', '2017-03-10 21:16:42'),
(312, 19, 12, 12, 0, '2017-03-10 21:16:42', '2017-03-10 21:16:42'),
(313, 19, 1, 13, 0, '2017-03-10 21:16:43', '2017-03-10 21:16:43'),
(314, 19, 2, 13, 0, '2017-03-10 21:16:43', '2017-03-10 21:16:43'),
(315, 19, 3, 13, 0, '2017-03-10 21:16:43', '2017-03-10 21:16:43'),
(316, 19, 4, 13, 0, '2017-03-10 21:16:43', '2017-03-10 21:16:43'),
(317, 19, 5, 13, 0, '2017-03-10 21:16:43', '2017-03-10 21:16:43'),
(318, 19, 6, 13, 0, '2017-03-10 21:16:43', '2017-03-10 21:16:43'),
(319, 19, 7, 13, 1, '2017-03-10 21:16:43', '2017-03-10 21:16:43'),
(320, 19, 8, 13, 0, '2017-03-10 21:16:43', '2017-03-10 21:16:43'),
(321, 19, 9, 13, 0, '2017-03-10 21:16:43', '2017-03-10 21:16:43'),
(322, 19, 10, 13, 0, '2017-03-10 21:16:43', '2017-03-10 21:16:43'),
(323, 19, 11, 13, 0, '2017-03-10 21:16:43', '2017-03-10 21:16:43'),
(324, 19, 12, 13, 0, '2017-03-10 21:16:43', '2017-03-10 21:16:43'),
(325, 21, 1, 11, 0, '2017-03-10 21:20:31', '2017-03-10 21:20:31'),
(326, 21, 2, 11, 0, '2017-03-10 21:20:31', '2017-03-10 21:20:31'),
(327, 21, 3, 11, 0, '2017-03-10 21:20:31', '2017-03-10 21:20:31'),
(328, 21, 4, 11, 0, '2017-03-10 21:20:31', '2017-03-10 21:20:31'),
(329, 21, 5, 11, 1, '2017-03-10 21:20:31', '2017-03-10 21:20:31'),
(330, 21, 6, 11, 0, '2017-03-10 21:20:31', '2017-03-10 21:20:31'),
(331, 21, 7, 11, 0, '2017-03-10 21:20:31', '2017-03-10 21:20:31'),
(332, 21, 8, 11, 0, '2017-03-10 21:20:31', '2017-03-10 21:20:31'),
(333, 21, 9, 11, 0, '2017-03-10 21:20:31', '2017-03-10 21:20:31'),
(334, 21, 10, 11, 0, '2017-03-10 21:20:31', '2017-03-10 21:20:31'),
(335, 21, 11, 11, 0, '2017-03-10 21:20:31', '2017-03-10 21:20:31'),
(336, 21, 12, 11, 0, '2017-03-10 21:20:31', '2017-03-10 21:20:31'),
(337, 21, 1, 12, 1, '2017-03-10 21:20:31', '2017-03-10 21:20:31'),
(338, 21, 2, 12, 0, '2017-03-10 21:20:31', '2017-03-10 21:20:31'),
(339, 21, 3, 12, 0, '2017-03-10 21:20:31', '2017-03-10 21:20:31'),
(340, 21, 4, 12, 0, '2017-03-10 21:20:31', '2017-03-10 21:20:31'),
(341, 21, 5, 12, 0, '2017-03-10 21:20:32', '2017-03-10 21:20:32'),
(342, 21, 6, 12, 0, '2017-03-10 21:20:32', '2017-03-10 21:20:32'),
(343, 21, 7, 12, 0, '2017-03-10 21:20:32', '2017-03-10 21:20:32'),
(344, 21, 8, 12, 0, '2017-03-10 21:20:32', '2017-03-10 21:20:32'),
(345, 21, 9, 12, 0, '2017-03-10 21:20:32', '2017-03-10 21:20:32'),
(346, 21, 10, 12, 0, '2017-03-10 21:20:32', '2017-03-10 21:20:32'),
(347, 21, 11, 12, 0, '2017-03-10 21:20:32', '2017-03-10 21:20:32'),
(348, 21, 12, 12, 0, '2017-03-10 21:20:32', '2017-03-10 21:20:32'),
(349, 21, 1, 13, 0, '2017-03-10 21:20:32', '2017-03-10 21:20:32'),
(350, 21, 2, 13, 0, '2017-03-10 21:20:32', '2017-03-10 21:20:32'),
(351, 21, 3, 13, 0, '2017-03-10 21:20:32', '2017-03-10 21:20:32'),
(352, 21, 4, 13, 0, '2017-03-10 21:20:32', '2017-03-10 21:20:32'),
(353, 21, 5, 13, 1, '2017-03-10 21:20:32', '2017-03-10 21:20:32'),
(354, 21, 6, 13, 0, '2017-03-10 21:20:32', '2017-03-10 21:20:32'),
(355, 21, 7, 13, 0, '2017-03-10 21:20:32', '2017-03-10 21:20:32'),
(356, 21, 8, 13, 0, '2017-03-10 21:20:32', '2017-03-10 21:20:32'),
(357, 21, 9, 13, 0, '2017-03-10 21:20:32', '2017-03-10 21:20:32'),
(358, 21, 10, 13, 0, '2017-03-10 21:20:32', '2017-03-10 21:20:32'),
(359, 21, 11, 13, 0, '2017-03-10 21:20:32', '2017-03-10 21:20:32'),
(360, 21, 12, 13, 0, '2017-03-10 21:20:32', '2017-03-10 21:20:32');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `ambitos`
--
ALTER TABLE `ambitos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `areaslic`
--
ALTER TABLE `areaslic`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indices de la tabla `referentials`
--
ALTER TABLE `referentials`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `referentials_descripcion_unique` (`descripcion`);

--
-- Indices de la tabla `respuestas`
--
ALTER TABLE `respuestas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indices de la tabla `vinculacions`
--
ALTER TABLE `vinculacions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `vinculacions_correo_unique` (`correo`);

--
-- Indices de la tabla `vinculacions_ambitos`
--
ALTER TABLE `vinculacions_ambitos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `ambitos`
--
ALTER TABLE `ambitos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT de la tabla `areaslic`
--
ALTER TABLE `areaslic`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `referentials`
--
ALTER TABLE `referentials`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT de la tabla `respuestas`
--
ALTER TABLE `respuestas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `vinculacions`
--
ALTER TABLE `vinculacions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT de la tabla `vinculacions_ambitos`
--
ALTER TABLE `vinculacions_ambitos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=361;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
